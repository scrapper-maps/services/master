`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`keyword`, {
        keyword_name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        fetched : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
        create_at : {
            type      : Sequelize.DATE,
            allowNull : false,
            defaultValue: Sequelize.fn('NOW')
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false,
            defaultValue: 1
        },
    }, {
        schema: 'transactional',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};