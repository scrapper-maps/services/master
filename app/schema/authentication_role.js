`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`authentication_role`, {
        appid : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        rolename : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        roledesc : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false,
            defaultValue: 1
        },
    }, {
        schema: 'authentication',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};