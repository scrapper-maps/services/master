`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`instances`, {
        keyword_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        instance_name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        instance_address : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        instance_phone : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        create_at : {
            type      : Sequelize.DATE,
            allowNull : false,
            defaultValue: Sequelize.fn('NOW')
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false,
            defaultValue: 1
        },
    }, {
        schema: 'transactional',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};