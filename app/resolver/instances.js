`use strict`
const { Validator: v } = require('node-input-validator')
const { hashids, validate } = require(`${__basedir}/app/helper/helper`)

module.exports = {
    Query: { instances: () => ({}) },
    InstancesQuery: {
        get: async(__, req, ctx) => {
            let validator = new v(req, { 
                order_by: 'in:instance_name,newest_first',
                page: 'required|numeric',
                page_size: 'required|numeric|in:5,10,20'
            })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            req.order_by = req.order_by == 'newest_first' ? 'create_at' : req.order_by

            let search = req.search ? {
                [Op.or]: [
                    { instance_name: { [Op.iLike]: `%${req.search}%` } },
                    { instance_address: { [Op.iLike]: `%${req.search}%` } },
                    { instance_phone: { [Op.iLike]: `%${req.search}%` } },
                ]
            } : {}

            let counts = await db.instances.count({ where: { ...search, statusid: 1 }, raw: true })

            let instances = await db.instances.findAll({
                attributes: ['id', 'instance_name', 'instance_address', 'instance_phone', 'create_at'],
                where: { ...search, statusid: 1 },
                offset: (req.page * req.page_size), limit: req.page_size,
                ... req.order_by && req.order_direction ? {
                    order: [[req.order_by, req.order_direction]]
                } : {
                    order: [['create_at', 'desc']]
                },
                raw: true
            })

            instances = hashids.encodeArray(instances, 'id')

            return { data: instances, totalCount: counts, page: req.page }
        },
        find: async (__, req, ctx) => {
            req.keyword_id = hashids.decode(req.keyword_id)

            let keyword = await db.keyword.findOne({
                attributes: ['id', 'keyword_name'],
                where: { id: req.keyword_id, statusid: 1 },
                raw: true
            })

            await validate(keyword, 'user-input')

            let instances = await db.instances.findAll({
                attributes: ['id', 'instance_name', 'instance_address', 'instance_phone', 'create_at'],
                where: { keyword_id: req.keyword_id, statusid: 1 },
                raw: true
            })

            keyword.id = hashids.encode(keyword.id)
            keyword.instances = hashids.encodeArray(instances, 'id')

            return keyword
        }
    },
}