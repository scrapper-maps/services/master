`use strict`
const { Validator: v } = require('node-input-validator')
const { hashids, validate } = require(`${__basedir}/app/helper/helper`)
const xlsx = require('xlsx');
const { createWriteStream } = require("fs");

module.exports = {
    Query: { keyword: () => ({}) },
    Mutation: { keyword: () => ({}) },
    KeywordQuery: {
        get: async () => {
            let keyword = await db.keyword.findAll({
                attributes: ['id', 'keyword_name', 'create_at', 'fetched' ],
                where: { statusid: 1 },
                order: [['create_at', 'DESC']],
                raw: true
            })
            keyword = hashids.encodeArray(keyword, 'id')
            return keyword
        }
    },
    KeywordMutation: {
        create: async (__, req) => {
            let validator = new v(req, { keyword_name: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            const keyword_name = req.keyword_name.toLowerCase()
            let validateName = await db.keyword.findOne({
                attributes: ['keyword_name'],
                where: { 
                    [Op.and]: [
                        sequelize.where(
                            sequelize.fn('lower', sequelize.col('keyword_name')), 
                            sequelize.fn('lower', keyword_name)
                        ),
                        { statusid: 1 } 
                    ]
                },
                raw: true
            })
            await validate(!validateName, 'user-input')
            
            /** transaction start */
            let transaction = await sequelize.transaction()
            
            let saveKeyword
            try {
                saveKeyword = await db.keyword.create({ keyword_name: req.keyword_name, fetched: 0, statusid: 1 }, { transaction })
                await validate(saveKeyword)
                await transaction.commit()
            } catch(error) {
                console.log(error)
                await transaction.rollback()
            }

            return {id: hashids.encode(saveKeyword.id)}
        },
        delete: async (__, req, ctx) => {
            let validator = new v(req, { id: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            req.id = hashids.decode(req.id)
            
            let find = await db.keyword.findOne({ attributes: ['id'], where: { id: req.id, statusid: 1  }, raw: true })
            await validate(find, 'user-input')

            /** transaction start */
            let transaction = await sequelize.transaction()
            try {
                await db.keyword.update({ statusid: 0 }, { where: { id: find.id, statusid: 1 }, transaction })
                await db.instances.update({ statusid: 0 }, { where: { keyword_id: find.id, statusid: 1 }, transaction })

                await transaction.commit()
            } catch(error) {
                console.log(error)
                await transaction.rollback()
                await validate(false)
            }

            return { status: 1 }
        },
        saveImport: async (_, req, ctx) => {
            let validator = new v(req, { 
                file: 'required|array',
            })
            await validate(await validator.check(), 'user-input')

            const { filename, createReadStream } = await req.file[0]
            const path = `${__basedir}/protected/import/${filename}`
            await new Promise((res) => createReadStream().pipe(createWriteStream(path)).on("close", res))

            let workbook = xlsx.readFile(path)
            let json = xlsx.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]])
            
            let keywords = []
            for(let row of json) {
                if(row.keyword_name && keywords.indexOf(row.keyword_name) === -1) {
                    keywords.push(row.keyword_name)
                }
            }

            let validateKeywords = await db.keyword.findAll({
                attributes: ['keyword_name'],
                where: { 
                    [Op.and]: [
                        sequelize.where(
                            sequelize.fn('lower', sequelize.col('keyword_name')), 
                            { [Op.in]: keywords.map((val) => val.toLowerCase()) }
                        ),
                        { statusid: 1 } 
                    ]
                },
                raw: true
            })

            for (let i in keywords) {
                if(validateKeywords.find((phone) => phone == keywords[i])) {
                    keywords.splice(i+1, 1)
                }
            }

            keywords = keywords.map(val => ({ 
                keyword_name: val, 
                fetched: 0, 
                statusid: 1
            }))

            await db.keyword.bulkCreate(keywords)

            return { status: 1 }
        }
    }
}