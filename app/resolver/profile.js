`use strict`
const bcrypt = require('bcryptjs')
const { Validator: v } = require('node-input-validator')
const { validate, files, random } = require(`${__basedir}/app/helper/helper`)
const axios = require('axios')
const FormData = require('form-data')

module.exports = {
    Mutation: { profile: () => ({}) },
    ProfileMutation: {
        savePersonalData: async(__, req, ctx) => {
            let validator = new v(req, { 
                name: 'required',
                address: 'required',
            })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            let find = await db.authentication_user.findOne({
                attributes: [ [sequelize.col('spd.id'), 'id'], [sequelize.col('spd.photo'), 'photo'] ],
                include: [{ 
                    model: db.s_personal_data, as: 'spd', attributes: [], where: {
                        appid: ctx.req.auth.app.id, 
                        statusid: 1
                    } 
                }],
                where: { appid: ctx.req.auth.app.id, username: ctx.req.auth.username, statusid: 1 },
                raw: true
            })
            await validate(find, 'user-input')
            
            let transaction = await sequelize.transaction()
            try {
                await db.s_personal_data.update({ statusid: 0 }, { where: { id: find.id }, transaction })
                await db.s_personal_data.create({
                    appid: ctx.req.auth.app.id,
                    username: ctx.req.auth.username,
                    name: req.name,
                    address: req.address,
                    photo: find.photo,
                    create_by: ctx.req.auth.username,
                    statusid: 1
                }, { transaction })

                await transaction.commit()
            } catch(error) {
                console.log(error)
                await transaction.rollback()
                await validate(false)
            }

            return { status: 1 }
        },
        savePassword: async (__, req, ctx) => {
            let validator = new v(req, { 
                old_password: 'required',
                password: 'required|same:repassword',
            })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            const old_password = await bcrypt.hash(req.old_password, env.app.secret)

            let find = await db.authentication_user.findOne({
                attributes: [ 'id' ],
                include: [{ 
                    model: db.s_personal_data, as: 'spd', attributes: [], where: {
                        appid: ctx.req.auth.app.id, 
                        statusid: 1
                    } 
                }],
                where: { 
                    appid: ctx.req.auth.app.id, 
                    username: ctx.req.auth.username, 
                    password: old_password,
                    statusid: 1 
                },
                raw: true
            })
            await validate(find, 'user-input')

            let transaction = await sequelize.transaction()
            try {
                const password = await bcrypt.hash(req.password, env.app.secret)

                await db.authentication_user.update({ statusid: 0 }, { where: { id: find.id }, transaction })
                await db.authentication_user.create({
                    appid: ctx.req.auth.app.id, 
                    username: ctx.req.auth.username,
                    password: password,
                    create_by: ctx.req.auth.username,
                    statusid: 1
                }, { transaction })

                await transaction.commit()
            } catch(error) {
                console.log(error)
                await transaction.rollback()
                await validate(false)
            }

            return { status: 1 }
        },
        savePicture: async (_, req, ctx) => {
            let validator = new v(req, { 
                picture: 'required|array',
            })
            await validate(await validator.check(), 'user-input')

            const { filename, createReadStream } = await req.picture[0]
            
            const picture = createReadStream({options: { flags: 'r', encoding: null}})
            let fileValidator = new v({picture}, { 
                picture: 'required|mime:jpg,png',
            })
            await validate(await fileValidator.check(), 'user-input')

            let find = await db.authentication_user.findOne({
                attributes: [ 
                    [sequelize.col('spd.id'), 'id'], 
                    [sequelize.col('spd.name'), 'name'],
                    [sequelize.col('spd.address'), 'address'],
                ],
                include: [{ 
                    model: db.s_personal_data, as: 'spd', attributes: [], where: {
                        appid: ctx.req.auth.app.id, 
                        statusid: 1
                    } 
                }],
                where: { appid: ctx.req.auth.app.id, username: ctx.req.auth.username, statusid: 1 },
                raw: true
            })
            await validate(find, 'user-input')

            const splittedFilename = filename.split('.')
            const convertedFilename = `${random(50)}.${splittedFilename[splittedFilename.length-1]}`
            const file = files.graphqlConvertStream(convertedFilename, picture)
            
            const form = new FormData()
            form.append('picture', file)
            form.append('filename', convertedFilename )
            axios.post(`/file/save-display-picture`, form, {headers: form.getHeaders()}).then(async response => {
                await validate(response.data.status)

                let transaction = await sequelize.transaction()
                try {
                    await db.s_personal_data.update({ statusid: 0 }, { where: { id: find.id }, transaction })
                    await db.s_personal_data.create({
                        appid: ctx.req.auth.app.id,
                        username: ctx.req.auth.username,
                        name: find.name,
                        address: find.address,
                        photo: convertedFilename,
                        create_by: ctx.req.auth.username,
                        statusid: 1
                    }, { transaction })

                    await transaction.commit()
                } catch(error) {
                    console.log(error)
                    await transaction.rollback()
                    await validate(false)
                }

                return { status: 1 }
            }).catch(e => {
                console.log(e.message)
                validate(false)
            })
        },
    }
}