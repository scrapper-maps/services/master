const { gql } = require('apollo-server-express')

const main = gql`
    scalar JSON
    scalar JSONObject
    scalar Date
    scalar DateTime
    type AccountQuery {
        get: [JSONObject]
        roles: [JSONObject]
        view(id: String!): JSONObject
    }
    type AccountMutation {
        create(username: String!, name: String!, address: String!, password: String!, repassword: String!, roles: [String]!): JSONObject
        savePersonalData(username: String!, name: String!, address: String!): JSONObject
        savePassword(username: String!, password: String!, repassword: String!): JSONObject
        saveRoles(username: String!, roles: [String]!): JSONObject
        savePicture(id: String!, picture: [Upload!]!): JSONObject
        delete(id: String!): JSONObject
    }
    type ProfileMutation {
        savePicture(picture: [Upload!]!): JSONObject
        savePersonalData(name: String!, address: String!): JSONObject
        savePassword(old_password: String!, password: String!, repassword: String!): JSONObject
    }
    type KeywordQuery {
        get: [JSONObject]
    }
    type KeywordMutation {
        create(keyword_name: String!): JSONObject
        delete(id: String!): JSONObject
        saveImport(file: [Upload!]!): JSONObject
    }
    type InstancesQuery {
        get(order_by: String!, order_direction: String!, page: Int!, page_size: Int!, search: String!): JSONObject
        find(keyword_id: String!): JSONObject
    }
    type Query {
        userinfo: JSONObject
        account: AccountQuery
        keyword: KeywordQuery
        instances: InstancesQuery
    }
    type Mutation {
        authenticate(username: String!, password: String!): JSONObject
        account: AccountMutation
        keyword: KeywordMutation
        profile: ProfileMutation
    }
`

module.exports = [main]