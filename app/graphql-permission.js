const { rule, shield, and, or } = require('graphql-shield')
const authenticated = rule({ cache: 'contextual' })(
    async (parent, args, {req}, info) => {
        return req.auth ? true : false
    },
)
const administrator = rule({ cache: 'contextual' })(
    async (parent, args, {req}, info) => {
        return req.auth.roles.includes('administrator')
    },
)
module.exports = shield({
    Query: {
        account: and(authenticated, administrator),
        keyword: and(authenticated),
        instances: and(authenticated),
    },
    Mutation: {
        account: and(authenticated, administrator),
        keyword: and(authenticated),
        profile: and(authenticated),
    }
}, {
    fallbackError: true,
    debug: true,
})