`use strict`
const qs = require('querystring')
const { Validator: v } = require('node-input-validator')
const { validate, hashids } = require(`${__basedir}/app/helper/helper`)
const sequelize = require('sequelize')

const authorize = async ({req}) => {
    let authorizeApp = await db.application.findOne({ attributes: ['id', 'appname', 'secret'], where: { secret: env.app.secret }, raw: true })
    await validate(authorizeApp, 'authentication')

    
    req.headers.authorization = req.query.token ? qs.stringify(req.query) : req.headers.authorization
    let validator = new v(req.headers, { authorization: 'required' })
    let matched = await validator.check()
    if(matched) {
        const authorization = qs.parse(req.headers.authorization)
        let session = await db.authentication_user_login.findOne({ 
            where: { 
                appid: authorizeApp.id, 
                token: authorization.token, 
                create_at: { [sequelize.Op.gt]: new Date(Date.now() - (1 * 24 * 60 * 60 * 1000)) }, 
                statusid: 1 
            }
        })
        
        if(session) {
            session.create_at = new Date()
            session = await session.reload()
            
            let roles = await db.authentication_user_role.findAll({ attributes: ['rolename'], where: { appid: authorizeApp.id, username: session.username, statusid: 1 }, raw: true })
            roles = roles.map(({ rolename }) => rolename)

            req.auth = { app: authorizeApp, username: session.username, roles: roles }
        } else {
            req.auth = false
        }
    } else {
        req.auth = false
    }
    return {req}
}

module.exports = authorize