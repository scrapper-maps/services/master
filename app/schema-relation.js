'use strict'
module.exports = () => {
    /** Menu & Label */
    db.s_label_menu.hasMany(db.s_menu, {foreignKey : 'id'})
    db.s_menu.belongsTo(db.s_label_menu, {foreignKey : 'label_id'})

    /** Keyword Instances */
    db.instances.belongsTo(db.keyword, { as: 'k', foreignKey : 'keyword_id'})
    db.keyword.hasMany(db.instances, { as: 'i', foreignKey : 'id'})

    /** User & Authentication */
    db.authentication_user.hasOne(db.s_personal_data, {as: 'spd', foreignKey : 'username', sourceKey : 'username'})
    db.s_personal_data.belongsTo(db.authentication_user, {as: 'au', foreignKey : 'username'})

    /** Roles */
    db.authentication_role.hasMany(db.authentication_user_role, { as: 'aur', foreignKey : 'rolename'})
    db.authentication_user_role.belongsTo(db.authentication_role, { as: 'ar', foreignKey : 'rolename', targetKey: 'rolename'})
}
