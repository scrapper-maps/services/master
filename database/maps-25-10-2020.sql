--
-- PostgreSQL database dump
--

-- Dumped from database version 10.11 (Ubuntu 10.11-1.pgdg19.04+1)
-- Dumped by pg_dump version 10.11 (Ubuntu 10.11-1.pgdg19.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: authentication; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA authentication;


ALTER SCHEMA authentication OWNER TO postgres;

--
-- Name: masterdata; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA masterdata;


ALTER SCHEMA masterdata OWNER TO postgres;

--
-- Name: transactional; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA transactional;


ALTER SCHEMA transactional OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: application; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.application (
    id integer NOT NULL,
    appname character varying(50) NOT NULL,
    secret character varying(100) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE authentication.application OWNER TO postgres;

--
-- Name: application_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.application_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.application_id_seq OWNER TO postgres;

--
-- Name: application_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.application_id_seq OWNED BY authentication.application.id;


--
-- Name: authentication_role; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_role (
    id integer NOT NULL,
    rolename character varying(50) NOT NULL,
    roledesc character varying(50) NOT NULL,
    statusid smallint NOT NULL,
    appid integer
);


ALTER TABLE authentication.authentication_role OWNER TO postgres;

--
-- Name: authentication_role_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.authentication_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.authentication_role_id_seq OWNER TO postgres;

--
-- Name: authentication_role_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.authentication_role_id_seq OWNED BY authentication.authentication_role.id;


--
-- Name: authentication_user; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_user (
    id integer NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(200) NOT NULL,
    create_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    create_by character varying(50) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL,
    appid integer
);


ALTER TABLE authentication.authentication_user OWNER TO postgres;

--
-- Name: authentication_user_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.authentication_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.authentication_user_id_seq OWNER TO postgres;

--
-- Name: authentication_user_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.authentication_user_id_seq OWNED BY authentication.authentication_user.id;


--
-- Name: authentication_user_login; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_user_login (
    id integer NOT NULL,
    token character varying(200) NOT NULL,
    username character varying(50) NOT NULL,
    create_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL,
    appid integer
);


ALTER TABLE authentication.authentication_user_login OWNER TO postgres;

--
-- Name: authentication_user_login_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.authentication_user_login_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.authentication_user_login_id_seq OWNER TO postgres;

--
-- Name: authentication_user_login_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.authentication_user_login_id_seq OWNED BY authentication.authentication_user_login.id;


--
-- Name: authentication_user_role; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_user_role (
    id integer NOT NULL,
    username character varying(50) NOT NULL,
    rolename character varying(50) NOT NULL,
    create_by character varying(100),
    statusid smallint DEFAULT 1 NOT NULL,
    create_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    appid integer
);


ALTER TABLE authentication.authentication_user_role OWNER TO postgres;

--
-- Name: authentication_user_role_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.authentication_user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.authentication_user_role_id_seq OWNER TO postgres;

--
-- Name: authentication_user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.authentication_user_role_id_seq OWNED BY authentication.authentication_user_role.id;


--
-- Name: s_label_menu; Type: TABLE; Schema: masterdata; Owner: postgres
--

CREATE TABLE masterdata.s_label_menu (
    id integer NOT NULL,
    label character varying(50) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE masterdata.s_label_menu OWNER TO postgres;

--
-- Name: s_label_menu_id_seq; Type: SEQUENCE; Schema: masterdata; Owner: postgres
--

CREATE SEQUENCE masterdata.s_label_menu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE masterdata.s_label_menu_id_seq OWNER TO postgres;

--
-- Name: s_label_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: masterdata; Owner: postgres
--

ALTER SEQUENCE masterdata.s_label_menu_id_seq OWNED BY masterdata.s_label_menu.id;


--
-- Name: s_menu; Type: TABLE; Schema: masterdata; Owner: postgres
--

CREATE TABLE masterdata.s_menu (
    id integer NOT NULL,
    menu character varying(25) NOT NULL,
    label_id integer NOT NULL,
    parent_id integer,
    url character varying(50),
    icons character varying(50),
    statusid smallint DEFAULT 1 NOT NULL,
    sequence integer NOT NULL,
    rolename text[]
);


ALTER TABLE masterdata.s_menu OWNER TO postgres;

--
-- Name: s_menu_id_seq; Type: SEQUENCE; Schema: masterdata; Owner: postgres
--

CREATE SEQUENCE masterdata.s_menu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE masterdata.s_menu_id_seq OWNER TO postgres;

--
-- Name: s_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: masterdata; Owner: postgres
--

ALTER SEQUENCE masterdata.s_menu_id_seq OWNED BY masterdata.s_menu.id;


--
-- Name: s_personal_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_personal_data (
    id integer NOT NULL,
    username character varying(50),
    name character varying(200),
    address character varying(200),
    create_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    create_by character varying(50),
    statusid smallint,
    photo text,
    appid integer
);


ALTER TABLE public.s_personal_data OWNER TO postgres;

--
-- Name: s_personal_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_personal_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_personal_data_id_seq OWNER TO postgres;

--
-- Name: s_personal_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_personal_data_id_seq OWNED BY public.s_personal_data.id;


--
-- Name: instances; Type: TABLE; Schema: transactional; Owner: postgres
--

CREATE TABLE transactional.instances (
    id integer NOT NULL,
    keyword_id integer NOT NULL,
    instance_name character varying(200) NOT NULL,
    instance_address character varying(255) NOT NULL,
    instance_phone character varying(50) NOT NULL,
    create_at timestamp(0) with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE transactional.instances OWNER TO postgres;

--
-- Name: instances_id_seq; Type: SEQUENCE; Schema: transactional; Owner: postgres
--

CREATE SEQUENCE transactional.instances_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transactional.instances_id_seq OWNER TO postgres;

--
-- Name: instances_id_seq; Type: SEQUENCE OWNED BY; Schema: transactional; Owner: postgres
--

ALTER SEQUENCE transactional.instances_id_seq OWNED BY transactional.instances.id;


--
-- Name: keyword; Type: TABLE; Schema: transactional; Owner: postgres
--

CREATE TABLE transactional.keyword (
    id integer NOT NULL,
    keyword_name character varying(255) NOT NULL,
    create_at timestamp(0) with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL,
    fetched smallint DEFAULT 0 NOT NULL
);


ALTER TABLE transactional.keyword OWNER TO postgres;

--
-- Name: keyword_id_seq; Type: SEQUENCE; Schema: transactional; Owner: postgres
--

CREATE SEQUENCE transactional.keyword_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transactional.keyword_id_seq OWNER TO postgres;

--
-- Name: keyword_id_seq; Type: SEQUENCE OWNED BY; Schema: transactional; Owner: postgres
--

ALTER SEQUENCE transactional.keyword_id_seq OWNED BY transactional.keyword.id;


--
-- Name: application id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.application ALTER COLUMN id SET DEFAULT nextval('authentication.application_id_seq'::regclass);


--
-- Name: authentication_role id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_role ALTER COLUMN id SET DEFAULT nextval('authentication.authentication_role_id_seq'::regclass);


--
-- Name: authentication_user id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user ALTER COLUMN id SET DEFAULT nextval('authentication.authentication_user_id_seq'::regclass);


--
-- Name: authentication_user_login id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_login ALTER COLUMN id SET DEFAULT nextval('authentication.authentication_user_login_id_seq'::regclass);


--
-- Name: authentication_user_role id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_role ALTER COLUMN id SET DEFAULT nextval('authentication.authentication_user_role_id_seq'::regclass);


--
-- Name: s_label_menu id; Type: DEFAULT; Schema: masterdata; Owner: postgres
--

ALTER TABLE ONLY masterdata.s_label_menu ALTER COLUMN id SET DEFAULT nextval('masterdata.s_label_menu_id_seq'::regclass);


--
-- Name: s_menu id; Type: DEFAULT; Schema: masterdata; Owner: postgres
--

ALTER TABLE ONLY masterdata.s_menu ALTER COLUMN id SET DEFAULT nextval('masterdata.s_menu_id_seq'::regclass);


--
-- Name: s_personal_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_personal_data ALTER COLUMN id SET DEFAULT nextval('public.s_personal_data_id_seq'::regclass);


--
-- Name: instances id; Type: DEFAULT; Schema: transactional; Owner: postgres
--

ALTER TABLE ONLY transactional.instances ALTER COLUMN id SET DEFAULT nextval('transactional.instances_id_seq'::regclass);


--
-- Name: keyword id; Type: DEFAULT; Schema: transactional; Owner: postgres
--

ALTER TABLE ONLY transactional.keyword ALTER COLUMN id SET DEFAULT nextval('transactional.keyword_id_seq'::regclass);


--
-- Data for Name: application; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

INSERT INTO authentication.application (id, appname, secret, statusid) VALUES (1, 'Scrapper Maps', '$2a$04$rGre3c9A2EH3urmSX.gKlewvhXiWL9.2qgz9/c1CjtFvFoZ.i0.om', 1);


--
-- Data for Name: authentication_role; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

INSERT INTO authentication.authentication_role (id, rolename, roledesc, statusid, appid) VALUES (2, 'administrator', 'Super Administrator', 1, 1);
INSERT INTO authentication.authentication_role (id, rolename, roledesc, statusid, appid) VALUES (1, 'general_user', 'General User', 1, 1);


--
-- Data for Name: authentication_user; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

INSERT INTO authentication.authentication_user (id, username, password, create_at, create_by, statusid, appid) VALUES (4, 'administrator', '$2a$04$rGre3c9A2EH3urmSX.gKle/JsB0AexPEFPDiIbr25GPPC6yc1Op0.', '2020-10-25 14:03:45.371256+07', 'administrator', 1, 1);
INSERT INTO authentication.authentication_user (id, username, password, create_at, create_by, statusid, appid) VALUES (5, 'general_user', '$2a$04$rGre3c9A2EH3urmSX.gKlew3mBT5NfhQGz.XgR9IFnMpRB/NMTepi', '2020-10-25 14:20:06.916176+07', 'administrator', 1, 1);


--
-- Data for Name: authentication_user_login; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (20, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDM2MDg2Nzh9.WYkYhLrRHfWN8uhkeQqeka1hbs06WsjxepEy9kQEpPQ', 'administrator', '2020-10-25 13:51:18.260042+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (21, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDM2MDg4Mzh9.p8Yi_vcRpQPCl9NLgZgo4KbNZG7qRnKNAvOMAvlme4I', 'administrator', '2020-10-25 13:53:58.622404+07', 1, 1);


--
-- Data for Name: authentication_user_role; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

INSERT INTO authentication.authentication_user_role (id, username, rolename, create_by, statusid, create_at, appid) VALUES (1, 'administrator', 'administrator', 'administrator', 1, '2020-08-14 18:51:34+07', 1);
INSERT INTO authentication.authentication_user_role (id, username, rolename, create_by, statusid, create_at, appid) VALUES (4, 'general_user', 'general_user', 'administrator', 1, '2020-10-25 14:19:53.123238+07', 1);


--
-- Data for Name: s_label_menu; Type: TABLE DATA; Schema: masterdata; Owner: postgres
--

INSERT INTO masterdata.s_label_menu (id, label, statusid) VALUES (1, 'Export Data', 1);
INSERT INTO masterdata.s_label_menu (id, label, statusid) VALUES (2, 'Super Administrator', 1);
INSERT INTO masterdata.s_label_menu (id, label, statusid) VALUES (3, 'Settings', 1);


--
-- Data for Name: s_menu; Type: TABLE DATA; Schema: masterdata; Owner: postgres
--

INSERT INTO masterdata.s_menu (id, menu, label_id, parent_id, url, icons, statusid, sequence, rolename) VALUES (1, 'Data Accumulative', 1, NULL, '/admin/accumulative', 'Equalizer', 1, 1, '{administrator,general_user}');
INSERT INTO masterdata.s_menu (id, menu, label_id, parent_id, url, icons, statusid, sequence, rolename) VALUES (2, 'Data Keywords', 1, NULL, '/admin/keywords', 'ArchiveOutlined', 1, 2, '{administrator,general_user}');
INSERT INTO masterdata.s_menu (id, menu, label_id, parent_id, url, icons, statusid, sequence, rolename) VALUES (8, 'Account Management', 2, NULL, '/admin/account', 'GroupOutlined', 1, 7, '{administrator}');
INSERT INTO masterdata.s_menu (id, menu, label_id, parent_id, url, icons, statusid, sequence, rolename) VALUES (9, 'Setting Profile', 3, NULL, '/admin/profile', 'GroupOutlined', 1, 7, '{administrator,general_user}');


--
-- Data for Name: s_personal_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.s_personal_data (id, username, name, address, create_at, create_by, statusid, photo, appid) VALUES (28, 'administrator', 'Administrator', 'Dusun Jatianom, Ds. Karangjati', '2020-10-25 07:19:08.972312', 'administrator', 1, '', 1);
INSERT INTO public.s_personal_data (id, username, name, address, create_at, create_by, statusid, photo, appid) VALUES (30, 'general_user', 'Default General User', 'Pandaan, Pasuruan, Jawa Timur', '2020-10-25 07:19:45.426672', 'administrator', 1, '', 1);


--
-- Data for Name: instances; Type: TABLE DATA; Schema: transactional; Owner: postgres
--

--
-- Name: application_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.application_id_seq', 1, true);


--
-- Name: authentication_role_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.authentication_role_id_seq', 2, true);


--
-- Name: authentication_user_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.authentication_user_id_seq', 5, true);


--
-- Name: authentication_user_login_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.authentication_user_login_id_seq', 21, true);


--
-- Name: authentication_user_role_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.authentication_user_role_id_seq', 4, true);


--
-- Name: s_label_menu_id_seq; Type: SEQUENCE SET; Schema: masterdata; Owner: postgres
--

SELECT pg_catalog.setval('masterdata.s_label_menu_id_seq', 4, true);


--
-- Name: s_menu_id_seq; Type: SEQUENCE SET; Schema: masterdata; Owner: postgres
--

SELECT pg_catalog.setval('masterdata.s_menu_id_seq', 9, true);


--
-- Name: s_personal_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_personal_data_id_seq', 30, true);


--
-- Name: instances_id_seq; Type: SEQUENCE SET; Schema: transactional; Owner: postgres
--

SELECT pg_catalog.setval('transactional.instances_id_seq', 1, true);


--
-- Name: keyword_id_seq; Type: SEQUENCE SET; Schema: transactional; Owner: postgres
--

SELECT pg_catalog.setval('transactional.keyword_id_seq', 1, true);


--
-- Name: application application_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.application
    ADD CONSTRAINT application_pk PRIMARY KEY (id);


--
-- Name: authentication_role authentication_role_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_role
    ADD CONSTRAINT authentication_role_pk PRIMARY KEY (id);


--
-- Name: authentication_user_login authentication_user_login_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_login
    ADD CONSTRAINT authentication_user_login_pk PRIMARY KEY (id);


--
-- Name: authentication_user authentication_user_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user
    ADD CONSTRAINT authentication_user_pk PRIMARY KEY (id);


--
-- Name: authentication_user_role authentication_user_role_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_role
    ADD CONSTRAINT authentication_user_role_pk PRIMARY KEY (id);


--
-- Name: s_label_menu s_label_menu_pk; Type: CONSTRAINT; Schema: masterdata; Owner: postgres
--

ALTER TABLE ONLY masterdata.s_label_menu
    ADD CONSTRAINT s_label_menu_pk PRIMARY KEY (id);


--
-- Name: s_menu s_menu_pk; Type: CONSTRAINT; Schema: masterdata; Owner: postgres
--

ALTER TABLE ONLY masterdata.s_menu
    ADD CONSTRAINT s_menu_pk PRIMARY KEY (id);


--
-- Name: s_personal_data s_personal_data_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_personal_data
    ADD CONSTRAINT s_personal_data_pk PRIMARY KEY (id);


--
-- Name: instances instances_pk; Type: CONSTRAINT; Schema: transactional; Owner: postgres
--

ALTER TABLE ONLY transactional.instances
    ADD CONSTRAINT instances_pk PRIMARY KEY (id);


--
-- Name: keyword keyword_pk; Type: CONSTRAINT; Schema: transactional; Owner: postgres
--

ALTER TABLE ONLY transactional.keyword
    ADD CONSTRAINT keyword_pk PRIMARY KEY (id);


--
-- Name: instances_instance_phone_idx; Type: INDEX; Schema: transactional; Owner: postgres
--

CREATE INDEX instances_instance_phone_idx ON transactional.instances USING btree (instance_phone, statusid);


--
-- Name: instances_keyword_id_idx; Type: INDEX; Schema: transactional; Owner: postgres
--

CREATE INDEX instances_keyword_id_idx ON transactional.instances USING btree (keyword_id, statusid);


--
-- Name: keyword_keyword_idx; Type: INDEX; Schema: transactional; Owner: postgres
--

CREATE INDEX keyword_keyword_idx ON transactional.keyword USING btree (keyword_name, statusid);


--
-- PostgreSQL database dump complete
--

