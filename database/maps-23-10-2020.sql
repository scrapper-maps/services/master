--
-- PostgreSQL database dump
--

-- Dumped from database version 10.11 (Ubuntu 10.11-1.pgdg19.04+1)
-- Dumped by pg_dump version 10.11 (Ubuntu 10.11-1.pgdg19.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: authentication; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA authentication;


ALTER SCHEMA authentication OWNER TO postgres;

--
-- Name: masterdata; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA masterdata;


ALTER SCHEMA masterdata OWNER TO postgres;

--
-- Name: transactional; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA transactional;


ALTER SCHEMA transactional OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: application; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.application (
    id integer NOT NULL,
    appname character varying(50) NOT NULL,
    secret character varying(100) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE authentication.application OWNER TO postgres;

--
-- Name: application_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.application_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.application_id_seq OWNER TO postgres;

--
-- Name: application_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.application_id_seq OWNED BY authentication.application.id;


--
-- Name: authentication_role; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_role (
    id integer NOT NULL,
    rolename character varying(50) NOT NULL,
    roledesc character varying(50) NOT NULL,
    statusid smallint NOT NULL,
    appid integer
);


ALTER TABLE authentication.authentication_role OWNER TO postgres;

--
-- Name: authentication_role_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.authentication_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.authentication_role_id_seq OWNER TO postgres;

--
-- Name: authentication_role_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.authentication_role_id_seq OWNED BY authentication.authentication_role.id;


--
-- Name: authentication_user; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_user (
    id integer NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(200) NOT NULL,
    create_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    create_by character varying(50) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL,
    appid integer
);


ALTER TABLE authentication.authentication_user OWNER TO postgres;

--
-- Name: authentication_user_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.authentication_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.authentication_user_id_seq OWNER TO postgres;

--
-- Name: authentication_user_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.authentication_user_id_seq OWNED BY authentication.authentication_user.id;


--
-- Name: authentication_user_login; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_user_login (
    id integer NOT NULL,
    token character varying(200) NOT NULL,
    username character varying(50) NOT NULL,
    create_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL,
    appid integer
);


ALTER TABLE authentication.authentication_user_login OWNER TO postgres;

--
-- Name: authentication_user_login_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.authentication_user_login_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.authentication_user_login_id_seq OWNER TO postgres;

--
-- Name: authentication_user_login_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.authentication_user_login_id_seq OWNED BY authentication.authentication_user_login.id;


--
-- Name: authentication_user_role; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_user_role (
    id integer NOT NULL,
    username character varying(50) NOT NULL,
    rolename character varying(50) NOT NULL,
    create_by character varying(100),
    statusid smallint DEFAULT 1 NOT NULL,
    create_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    appid integer
);


ALTER TABLE authentication.authentication_user_role OWNER TO postgres;

--
-- Name: authentication_user_role_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.authentication_user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.authentication_user_role_id_seq OWNER TO postgres;

--
-- Name: authentication_user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.authentication_user_role_id_seq OWNED BY authentication.authentication_user_role.id;


--
-- Name: s_label_menu; Type: TABLE; Schema: masterdata; Owner: postgres
--

CREATE TABLE masterdata.s_label_menu (
    id integer NOT NULL,
    label character varying(50) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE masterdata.s_label_menu OWNER TO postgres;

--
-- Name: s_label_menu_id_seq; Type: SEQUENCE; Schema: masterdata; Owner: postgres
--

CREATE SEQUENCE masterdata.s_label_menu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE masterdata.s_label_menu_id_seq OWNER TO postgres;

--
-- Name: s_label_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: masterdata; Owner: postgres
--

ALTER SEQUENCE masterdata.s_label_menu_id_seq OWNED BY masterdata.s_label_menu.id;


--
-- Name: s_menu; Type: TABLE; Schema: masterdata; Owner: postgres
--

CREATE TABLE masterdata.s_menu (
    id integer NOT NULL,
    menu character varying(25) NOT NULL,
    label_id integer NOT NULL,
    parent_id integer,
    url character varying(50),
    icons character varying(50),
    statusid smallint DEFAULT 1 NOT NULL,
    sequence integer NOT NULL,
    rolename text[]
);


ALTER TABLE masterdata.s_menu OWNER TO postgres;

--
-- Name: s_menu_id_seq; Type: SEQUENCE; Schema: masterdata; Owner: postgres
--

CREATE SEQUENCE masterdata.s_menu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE masterdata.s_menu_id_seq OWNER TO postgres;

--
-- Name: s_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: masterdata; Owner: postgres
--

ALTER SEQUENCE masterdata.s_menu_id_seq OWNED BY masterdata.s_menu.id;


--
-- Name: s_personal_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_personal_data (
    id integer NOT NULL,
    username character varying(50),
    name character varying(200),
    address character varying(200),
    create_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    create_by character varying(50),
    statusid smallint,
    photo text,
    appid integer
);


ALTER TABLE public.s_personal_data OWNER TO postgres;

--
-- Name: s_personal_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_personal_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_personal_data_id_seq OWNER TO postgres;

--
-- Name: s_personal_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_personal_data_id_seq OWNED BY public.s_personal_data.id;


--
-- Name: instances; Type: TABLE; Schema: transactional; Owner: postgres
--

CREATE TABLE transactional.instances (
    id integer NOT NULL,
    keyword_id integer NOT NULL,
    instance_name character varying(200) NOT NULL,
    instance_address character varying(255) NOT NULL,
    instance_phone character varying(50) NOT NULL,
    create_at timestamp(0) with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE transactional.instances OWNER TO postgres;

--
-- Name: instances_id_seq; Type: SEQUENCE; Schema: transactional; Owner: postgres
--

CREATE SEQUENCE transactional.instances_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transactional.instances_id_seq OWNER TO postgres;

--
-- Name: instances_id_seq; Type: SEQUENCE OWNED BY; Schema: transactional; Owner: postgres
--

ALTER SEQUENCE transactional.instances_id_seq OWNED BY transactional.instances.id;


--
-- Name: keyword; Type: TABLE; Schema: transactional; Owner: postgres
--

CREATE TABLE transactional.keyword (
    id integer NOT NULL,
    keyword_name character varying(255) NOT NULL,
    create_at timestamp(0) with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL,
    fetched smallint DEFAULT 0 NOT NULL
);


ALTER TABLE transactional.keyword OWNER TO postgres;

--
-- Name: keyword_id_seq; Type: SEQUENCE; Schema: transactional; Owner: postgres
--

CREATE SEQUENCE transactional.keyword_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transactional.keyword_id_seq OWNER TO postgres;

--
-- Name: keyword_id_seq; Type: SEQUENCE OWNED BY; Schema: transactional; Owner: postgres
--

ALTER SEQUENCE transactional.keyword_id_seq OWNED BY transactional.keyword.id;


--
-- Name: application id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.application ALTER COLUMN id SET DEFAULT nextval('authentication.application_id_seq'::regclass);


--
-- Name: authentication_role id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_role ALTER COLUMN id SET DEFAULT nextval('authentication.authentication_role_id_seq'::regclass);


--
-- Name: authentication_user id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user ALTER COLUMN id SET DEFAULT nextval('authentication.authentication_user_id_seq'::regclass);


--
-- Name: authentication_user_login id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_login ALTER COLUMN id SET DEFAULT nextval('authentication.authentication_user_login_id_seq'::regclass);


--
-- Name: authentication_user_role id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_role ALTER COLUMN id SET DEFAULT nextval('authentication.authentication_user_role_id_seq'::regclass);


--
-- Name: s_label_menu id; Type: DEFAULT; Schema: masterdata; Owner: postgres
--

ALTER TABLE ONLY masterdata.s_label_menu ALTER COLUMN id SET DEFAULT nextval('masterdata.s_label_menu_id_seq'::regclass);


--
-- Name: s_menu id; Type: DEFAULT; Schema: masterdata; Owner: postgres
--

ALTER TABLE ONLY masterdata.s_menu ALTER COLUMN id SET DEFAULT nextval('masterdata.s_menu_id_seq'::regclass);


--
-- Name: s_personal_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_personal_data ALTER COLUMN id SET DEFAULT nextval('public.s_personal_data_id_seq'::regclass);


--
-- Name: instances id; Type: DEFAULT; Schema: transactional; Owner: postgres
--

ALTER TABLE ONLY transactional.instances ALTER COLUMN id SET DEFAULT nextval('transactional.instances_id_seq'::regclass);


--
-- Name: keyword id; Type: DEFAULT; Schema: transactional; Owner: postgres
--

ALTER TABLE ONLY transactional.keyword ALTER COLUMN id SET DEFAULT nextval('transactional.keyword_id_seq'::regclass);


--
-- Data for Name: application; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

INSERT INTO authentication.application (id, appname, secret, statusid) VALUES (1, 'Scrapper Maps', '$2a$04$rGre3c9A2EH3urmSX.gKlewvhXiWL9.2qgz9/c1CjtFvFoZ.i0.om', 1);


--
-- Data for Name: authentication_role; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

INSERT INTO authentication.authentication_role (id, rolename, roledesc, statusid, appid) VALUES (1, 'surveyor', 'Surveyor', 1, 1);
INSERT INTO authentication.authentication_role (id, rolename, roledesc, statusid, appid) VALUES (2, 'administrator', 'Super Administrator', 1, 1);


--
-- Data for Name: authentication_user; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

INSERT INTO authentication.authentication_user (id, username, password, create_at, create_by, statusid, appid) VALUES (1, 'administrator', '$2a$04$rGre3c9A2EH3urmSX.gKlefCVEDA3/SegZJcgkceHPSmgXvOG.Hzm', '2020-08-24 11:28:24+07', 'administrator', 1, 1);
INSERT INTO authentication.authentication_user (id, username, password, create_at, create_by, statusid, appid) VALUES (2, 'surveyor', '$2a$04$rGre3c9A2EH3urmSX.gKleCuDV5I8Jw2MzP8lh9RBgrfyf0Xp.FwC', '2020-08-24 11:29:03+07', 'administrator', 1, 1);


--
-- Data for Name: authentication_user_login; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (1, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDI3Njk2NzR9.XIftd0_rn5HhwFovjfXMpoWW_i4JfzWecCTeGGx_GPU', 'administrator', '2020-10-15 20:47:54.326014+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (2, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDI3NzQ1MDR9.a1PHK2qDnbZ6K3QQOSzroG9LHsl_UFudAVNhYqnXrO4', 'administrator', '2020-10-15 22:08:24.654603+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDI3NzQ4MTZ9.QfYY2024cNO6_X_9l76Zqf1B10tyIlCa7hmRw1iZavI', 'administrator', '2020-10-15 22:13:36.022449+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDI3ODE4MTh9.OstRJgaGBqOCth6ajlB1L_cGX5us0djEB9uO32VlcS4', 'administrator', '2020-10-16 00:10:18.736187+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (5, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDI3OTI3Mjd9.FrtgALuZDrqBzzSmrnl1r9hdAcoHcSH6ojyULuXr2hI', 'administrator', '2020-10-16 03:12:07.695905+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (6, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDI3OTI4NTl9.wVp70W-dAWGw-HB6wsHbPZDkxPg9diM4VCryJVLKSiE', 'administrator', '2020-10-16 03:14:19.674225+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (7, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDI3OTMxNTF9._zEecOBqaZIET4ri8vA-uKoWUJPiJosktj7uj-BSuG4', 'administrator', '2020-10-16 03:19:11.50915+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (8, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDI3OTMyNTB9.o-K0bRIr_B1iKTMnVMUhsIg3A6uY4U3DlDAhDqJ3V40', 'administrator', '2020-10-16 03:20:50.28262+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (9, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDMwMzk4ODJ9.4DvmmzO1yaq3qq3LURnCFyfCQAzqaM3ScIo3SnLZ_Q4', 'administrator', '2020-10-18 23:51:22.796676+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (10, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDMwMzk5MDF9.IDBbkUkHkBY81B2RRrBu6mx495fbhbaPGyeGpfYhyw4', 'administrator', '2020-10-18 23:51:41.448284+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (11, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDMwNDAwOTh9.GOVJj9AxPSEgZgsVMOCXmGX9ND8j_eyncZLF8fTiPus', 'administrator', '2020-10-18 23:54:58.216211+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (12, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDMwNDAxNjR9.dx4wSDZL-nMpP-5UnAEKe6jmg_11I9A3A5ttSUFydFo', 'administrator', '2020-10-18 23:56:04.346856+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (13, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDMxNzQwMTB9.6g-P-L2oiOsinZlYAh_SqSpBvFh3oAWs5TgBdW5-xRU', 'administrator', '2020-10-20 13:06:50.45077+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (14, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDMyMDY1MDl9.QaskqKITIkhzHdiIFww24s7O8XL1_oxG1rwZDFSgTFQ', 'administrator', '2020-10-20 22:08:29.432598+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (15, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDMyMDg0NzF9.TQhkqo43m__Ud7jsl3PYBhMp203rkNr_fmVhgQGD7lk', 'administrator', '2020-10-20 22:41:11.876137+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (16, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDMyNjEwOTR9.IwyCgLKd7EhYp3KMIV7uvhaxdEVW8inKY8YuM02jv88', 'administrator', '2020-10-21 13:18:14.494093+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (17, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDMzMzU5NjB9.KkpzhOLxSn7DXmBhiWnoJy0E8U9fpJTcBEPrD9GuXY0', 'administrator', '2020-10-22 10:06:00.42396+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (18, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDMzNDc1MTR9.usrOsx9ofTpUf2cffK77HFUlP_D5ZL6GNmp8BnZiQW8', 'administrator', '2020-10-22 13:18:34.174592+07', 1, 1);
INSERT INTO authentication.authentication_user_login (id, token, username, create_at, statusid, appid) VALUES (19, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJpYXQiOjE2MDM0NDcyNjl9.ZUt9m1qNW-TtYuGPgsbI0NRo2SXCJd0qTBOaXszEIy4', 'administrator', '2020-10-23 17:01:09.947215+07', 1, 1);


--
-- Data for Name: authentication_user_role; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

INSERT INTO authentication.authentication_user_role (id, username, rolename, create_by, statusid, create_at, appid) VALUES (1, 'administrator', 'administrator', 'administrator', 1, '2020-08-14 18:51:34+07', 1);
INSERT INTO authentication.authentication_user_role (id, username, rolename, create_by, statusid, create_at, appid) VALUES (2, 'surveyor', 'surveyor', 'administrator', 1, '2020-08-24 11:28:49+07', 1);
INSERT INTO authentication.authentication_user_role (id, username, rolename, create_by, statusid, create_at, appid) VALUES (3, 'surveyor', 'administrator', 'administrator', 1, '2020-08-24 11:28:49+07', 1);


--
-- Data for Name: s_label_menu; Type: TABLE DATA; Schema: masterdata; Owner: postgres
--

INSERT INTO masterdata.s_label_menu (id, label, statusid) VALUES (1, 'Export Data', 1);
INSERT INTO masterdata.s_label_menu (id, label, statusid) VALUES (2, 'Super Administrator', 1);
INSERT INTO masterdata.s_label_menu (id, label, statusid) VALUES (3, 'Settings', 1);


--
-- Data for Name: s_menu; Type: TABLE DATA; Schema: masterdata; Owner: postgres
--

INSERT INTO masterdata.s_menu (id, menu, label_id, parent_id, url, icons, statusid, sequence, rolename) VALUES (1, 'Data Accumulative', 1, NULL, '/admin/accumulative', 'Equalizer', 1, 1, '{administrator,surveyor}');
INSERT INTO masterdata.s_menu (id, menu, label_id, parent_id, url, icons, statusid, sequence, rolename) VALUES (2, 'Data Keywords', 1, NULL, '/admin/keywords', 'ArchiveOutlined', 1, 2, '{administrator}');
INSERT INTO masterdata.s_menu (id, menu, label_id, parent_id, url, icons, statusid, sequence, rolename) VALUES (8, 'Account Management', 2, NULL, '/admin/account', 'GroupOutlined', 1, 7, '{administrator}');
INSERT INTO masterdata.s_menu (id, menu, label_id, parent_id, url, icons, statusid, sequence, rolename) VALUES (9, 'Setting Profile', 3, NULL, '/admin/profile', 'GroupOutlined', 1, 7, '{administrator}');


--
-- Data for Name: s_personal_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.s_personal_data (id, username, name, address, create_at, create_by, statusid, photo, appid) VALUES (1, 'surveyor', 'Annafia Surveyors', 'Pandaan, Pasuruan, Jawa Timurs', '2020-08-23 16:13:31', 'administrator', 1, 'annafia.super.jpg', 1);
INSERT INTO public.s_personal_data (id, username, name, address, create_at, create_by, statusid, photo, appid) VALUES (10, 'administrator', 'Annafia Administator', 'Dusun Jatianom, Ds. Karangjati', '2020-09-28 10:05:42', 'administrator', 0, 'dmgBxsKX9rB3sGglfIIhVCO06nXHl1RQuCmgfqOvTdBoPkbibV.jpeg', 1);
INSERT INTO public.s_personal_data (id, username, name, address, create_at, create_by, statusid, photo, appid) VALUES (11, 'administrator', 'Annafia Administator', 'Dusun Jatianom, Ds. Karangjati', '2020-10-22 16:18:32.624578', 'administrator', 0, 'kcpTts5dgXahUcUQEgiaV3lAWxn7Tlhpfh8bA0ACQ09cyIwZFI.jpg', 1);
INSERT INTO public.s_personal_data (id, username, name, address, create_at, create_by, statusid, photo, appid) VALUES (12, 'administrator', 'Annafia Administator', 'Dusun Jatianom, Ds. Karangjati', '2020-10-22 16:20:44.892286', 'administrator', 1, 'PpaRUHDTNGtJPwoNZXGRhVB7WP62zL2RmvhJpRJ0zfli9rv2ue.png', 1);


--
-- Data for Name: instances; Type: TABLE DATA; Schema: transactional; Owner: postgres
--

INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (916, 32, 'Greens Production (Vendor Konveksi Baju Bandung)', 'Jl. Perak No.14', '0857-7005-112', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (917, 32, 'Grosir Hijab Bandung', 'Jl. Pinus, Cilengkrang 2, Desa palasari, Pasirbiru, Kota Bandung', '0856-5912-068', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (918, 32, 'Hijabbandung', 'Jl. Teleponia II No.mor 18', '0812-2226-13', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (919, 32, 'Grosir Baju Hijab Bandung', 'Komplek Griya Bandung Indah Blok H7 no 22, RT 003 RW 016', '0821-2164-356', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (920, 32, 'Zoya Grosir (Gudang Area Jawa Barat)', 'Jl. Pelajar Pejuang 45 No.25', '(022) 733380', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (921, 32, 'Ryani Hijab Bandung', 'Facial NU skin Teras Jasmine Beauty House, Jl. Cikajang Raya II No.3', '0812-2100-771', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (922, 32, 'Zafira Hijab Bandung', 'Jl. Rancabolang No.74', '0813-2225-255', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (923, 32, 'Jilbab instan bandung', 'Jl. Taman Klaten', '0856-5964-554', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (924, 32, 'Hijab Bandung, Zarra Hijab', 'Jl. Pasir Jaya Raya No.F4', '0821-2634-664', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (925, 32, 'Macca Hijab Bandung', 'Ruko Metro Indah Mall Blok C No.6, Jalan Soekarno Hatta No.590', '0878-2400-100', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (926, 32, 'Rie hijab Bandung', 'komp.cilengkrang 2, Jl. Manglayang 8', '0821-1534-350', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (927, 32, 'Rumah Hijab', 'Jl Anggrek 3 Blok C2, Perumahan Taman Bunga No.36', '0812-2080-50', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (928, 32, 'Distributor Hijab Alila Bandung 1', 'Jalan karawitan no. 119 turangga lengkong RT 03, RW.03', '0899-1265-84', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (929, 32, 'Shaliha Hijab Bandung', '', '0856-2122-55', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (930, 32, 'Hijab Bandung Outfit olshop', '', '0896-2617-827', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (931, 32, 'Kareemah Hijab Bandung', '', '0895-3463-7169', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (932, 32, 'HijabChic', 'The Saparua, Jl. Ambon No.15', '0858-1306-546', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (933, 32, 'Elmeira Hijab', 'Jl. Progo No.3', '0877-8618-625', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (934, 32, 'Shofiyyah Hijabi | Grosir Jilbab Murah | Reseller And Dropshiper | Jilbab Murah | HIjab', 'Jl. Saluyu Indah XIV', '0811-2173-98', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (935, 32, 'Mylady Hijab', 'Jl. Palasari No.12', '0813-1261-539', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (936, 32, 'Universitas Muhammadiyah Bandung', 'Jl. Soekarno-Hatta No.752', '(022) 6374599', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (937, 32, 'Hijab Mandjha Bandung', 'Jl. Pelajar Pejuang 45 No.60', '0811-8999-87', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (938, 32, 'Azkia hijab Bandung', '', '0858-4675-724', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (939, 32, 'Arfah Shop Supplier HiJab Bandung', 'Jl. Kubang Selatan V No.126', '0813-9468-400', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (940, 32, 'Hijab Murah Bandung', 'Perumnas, Jl. Sadang Serang Jl. Intan Raya No.20', '0896-5642-151', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (941, 32, 'The WAI , Grosir Hijab Bandung', 'Komplek Sukamenak Indah Q.95A, sukamenak, margahayu', '0822-1422-997', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (942, 32, 'Raffa hijab bandung', 'Jl. Lemah Hegar', '0813-9465-936', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (943, 32, 'Gladys fashion hijab bandung', 'Jl. Kawaluyaan Indah XVI No.21', '0821-2022-222', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (944, 32, 'grosir pakaian hijab - shadaw shop', 'Mustika Permai Jln.Mustika Raya no.40 Rt.07 Rw.06, Kel.Mekarjaya Kec.Rancasari Kota.Bandung, (Masuk dari komplek baturaden)', '0896-2300-102', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (945, 32, 'Agen Wanoja Hijab Bandung', 'Komplek Bumi Orange Blok C2 No 32', '0812-2452-010', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (946, 32, 'Syakila Hijab Bandung', 'Jalan Cijambe kulon 39, blkg, pt nila, gang, saripah', '0812-1483-266', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (947, 32, 'Hijab Alila BTC Bandung', 'Outlet FILA, BTC Fashion Mall Ground Floor (GF) Blok B3 No. 2 dan 3, Samping, Jl. Dr. Djunjunan No.143-149', '0895-2299-115', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (948, 32, 'Grosir Jilbab Bandung', 'Jl. Sukawarna Baru A No.16', '0817-436-88', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (949, 32, 'Konveksi Kerudung', '', '0878-2363-847', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (950, 32, 'Nun Jilbab Bandung online', 'Jl. Raya Bojongsoang Raya No.195', '0821-2740-007', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (951, 32, 'kerudung murah bandung', 'Jalan Gg. Pasantren No.rt. 01/03', '0896-5655-552', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (952, 32, 'Hijab Shinaela Bandung', 'Jl. Babakan Jati, RT.02/RW.11', '0856-2436-079', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (953, 32, 'F-Hijab', 'Jl. Cijerah Tengah No.114', '0822-0392-55', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (954, 32, 'hijab syandana bandung', 'griya caraka blog. G 18 kelurahan cisaranten endah kecamatan a bandung', '0812-2944-117', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (955, 32, 'Konveksi Kerudung Bandung', 'Jl. Kerkof No.12, RT.05/RW.20', '0813-8700-219', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (956, 32, 'Shafia Hijab Fasion', 'Jl. Gunung Batu No.62, RW.02', '(022) 2056048', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (957, 32, 'Jilbab Nun Bandung', 'Jl. Dipati Ukur No.272', '(022) 6976006', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (958, 32, 'Kerudung Rajut Bandung', 'Jl. Ibrahim Adjie Gg. Nata', '0813-9475-558', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (959, 32, 'Alisha Fancy Shop', 'Jl. Salendro Utara No.27', '(022) 730353', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (960, 32, 'Grosir kerudung bandung', 'Gg. Remaja VIII No.89', '0813-2176-171', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (961, 32, 'Hijab Nafisa Pasar Baru Bandung', 'Ps. Baru Trade Center, Jl. Otto Iskandar Dinata No.39-40', '0812-1746-303', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (962, 32, 'Emira Hijab Store', 'Bandung Indah Plaza lantai 1 no.56a Jalan Merdeka no.56 Bandung, Bandung', '0815-7340-847', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (963, 32, 'produsen kerudung bandung', 'Jl. Pamitran I No.10, RT.02/RW.09', '0857-2242-403', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (964, 32, 'Jual Kerudung Murah Bandung', 'Jl. Kec. Jl. Jati Serut No.51', '0821-2788-706', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (965, 32, 'Toko Baju Muslim | Grosir Hijab | Gamis Murah Bandung | Grosir Baju Muslim', 'Jl. Kiara Asri II No.5', '0898-7757-70', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (966, 32, 'Kerudung Bandung', 'Jl. Mig I No.3', '0812-2331-74', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (967, 32, 'SRhijab.com= Hijab Indonesia - Hijab Online Shop - Bandung', 'Komplek, Jl. Margahayu Kencana Raya Blk. E1 No.14 006/013', '0822-5222-997', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (968, 32, 'KARUNIA JAYA, printing kain, voal, kerudung, hijab, dress, bandung', 'Businees Park TKI 2 Ruko No. 9 Cigondewah', '0812-8433-739', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (969, 32, 'Gallery Hijabstory Indonesia', 'Jl. Ir. H. Juanda No.14', '(022) 2051263', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (970, 32, 'ORIANA HIJAB', '', '0822-1834-165', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (971, 32, 'Shafia hijab', 'Jl. Sukamenak No.149A', '0822-1401-852', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (972, 32, 'Lisa Hijab', 'Jalan Waringin, Pasar Andir Trade Centre, Lantai.1 Blok.P No.12', '0812-2427-729', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (973, 32, 'Galeri Hijab', 'Metro Indah Mall, Jl. Soekarno-Hatta Blok B3 No.590', '0856-2488-422', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (974, 32, 'Hijab Khawla', '', '0899-9011-21', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (975, 32, 'Istana Zuhair Gamis Hijab', 'Jl. Sukagalih II No.samping', '0858-6111-611', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (976, 32, 'Colour Hijab', 'Jl. Salendro Utara Jl. Reog No.1', '0878-2346-446', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (977, 32, 'ShaBib Hijab Syar''i Pasar Baru Square', 'Jalan Otto Iskandar Dinata No 81-89, Pasar Baru Square Lantai 1 Blok B No 29', '(022) 9320197', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (978, 32, 'Produsen Hijab Bandung', '', '0811-2229-94', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (979, 32, 'NuKatumbiri Hijab Pusat', 'Jl. Pasir Gunting', '0821-5394-109', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (980, 32, 'aulia shifa hijab', 'jl.sindang sari 3 no 07 antapani (Toko Pakaian Kelapa)', '0822-9503-544', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (981, 32, 'Produsen Kerudung Anak dan Dewasa Bandung (Nisa''s secret)', 'Kinagara Regency, Blok D, no. 11', '0878-2631-414', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (982, 32, 'Opi Kerudung', 'Jl. Pelindung Hewan No.6', '0878-2382-818', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (983, 32, 'Noore Sport Hijab', 'Jl. Cihampelas Walk', '(022) 6440176', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (984, 32, 'RaiNa Hijab Store', 'Ps. Baru Trade Center', '0821-3030-018', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (985, 32, 'Nur Hijab', 'Jl. Melong Asih No.59', '(022) 603054', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (986, 32, 'Azameera: High End Muslim Fashion', 'Jl. L. L. R.E. Martadinata No.51', '0811-205-58', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (987, 32, 'Konveksi Hijab (Ibu Nining)', 'Kp.Coblong RT04/15 No.08', '0857-9390-384', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (988, 32, 'Rev Hijab', 'Jl. Kosar', '0812-2400-252', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (989, 32, 'Tazkia Hijab Store', 'Jl. Riung Tineung IB No.24', '(022) 8752976', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (990, 32, 'Jilbab Vania Bandung', 'Jl. Nagrog No.B7', '0813-2222-670', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (991, 32, 'Baju muslim Wie Hijab Syari bandung', 'Jl. Pratista Bar. V Jl. Pratista Raya No.50', '0813-9444-158', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (992, 32, 'Fatimah Hijab', 'Jl. Bumi Asih Raya No.8', '0812-2382-897', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (993, 32, 'Kerudung Loma Bandung', 'cluster', '0812-8221-125', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (994, 32, 'Perlengkapan hijab nafira bandung', 'Komp. Permata kopo, Gang Nata I No.17', '0812-2006-733', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (995, 32, 'Dejilbab Completely Hijab', 'Jl. Terusan Buah Batu No.211', '0896-0312-176', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (996, 32, 'Noura Hijab', 'Jl. Cilaki No.45', '0823-1683-015', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (997, 32, 'trisy hijab', 'Trisy hijab, Jl. Jati Utama A4 No.13', '0898-7100-43', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (998, 32, 'Elzatta', 'Pasar Baru, Lantai 4, B2 No. 36-37, JL. Otto Iskandardinata', '(0298) 2268', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (999, 32, 'Hijab Ridick', 'Komplek Pesona Ciganitri C No.53', '0856-2100-02', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1000, 32, 'Iik Kerudung', 'Jl. Cigondewah Rahayu No.159 B', '0813-2064-098', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1001, 32, 'Bilqis Hijab Fashion', 'Jl. Nyengseret No.35', '0812-2132-123', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1002, 32, 'Family Hijab', 'Jl. Kotabaru VII No.13', '0852-2265-615', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1003, 32, 'Utami Hijab Syar''i', 'Pasar Baru Heritage, Jl. Otto Iskandar Dinata No.88', '0812-2005-468', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1004, 32, 'deMoss Wisata Berbelanja Buah Batu', 'Jl. Buah Batu No.149', '(022) 730748', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1005, 32, 'Hazna Hijab Baltos', 'Balubur Town Square, Jl. Tamansari Blok B No.09', '(022) 8446886', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1006, 32, 'Butik Kerudung', 'Jl. Cijambe', '0813-2275-161', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1007, 32, 'KERUDUNG KHADIJAH', 'gg pelita RT06 RW02', '0813-2362-448', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1008, 32, 'Wanoja Hijab', 'Jl. Guntur Sari Wetan I No.04 RT 03/11', '(022) 7351644', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1009, 32, 'Sakinah Kerudung', 'Jl. Terusan Buah Batu No.189', '0899-7962-06', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1010, 32, 'Lulu Hijab Grosir', 'Jl. Sasak Batu', '0812-8018-913', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1011, 32, 'Istiqomah Hijab (Konveksi&Onlineshop)', '', '0813-2012-002', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1012, 32, 'Khayr Hijab', 'Jl. Perintis', '0811-2171-71', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1013, 32, 'Butterfly Inner Hijab', 'Ruko Kurdi Regency, Jl. Inhoftank No.38B', '0811-2208-65', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1014, 32, 'Sakinah Kerudung', 'Jl. Jendral Ahmad Yani No.778', '0896-3459-527', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1015, 32, 'Baju Kertas & co - Jasa Print Laser Cut, Print Baju Muslim, Print Kerudung, Print Scarf, Pouch, Masker Scuba', 'Jl. Taman Holis Indah Jl. Kavindustri No.50-51', '0877-6996-696', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1016, 32, 'Ulfah Hijab', 'Jl. Bojong Buah No.15', '0882-1356-4654', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1017, 32, 'Sakinah Kerudung', 'Jl. Soekarnohatta 690A (riung)', '0898-7054-84', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1018, 32, 'Konveksi Kerudung dan Jahit Hijab', 'Lantai Dasar 1, Blok S No. 7 - Pasar Baru Trade Centre', '0822-1695-999', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1019, 32, 'MOYR HIJAB', 'Jl. Tamansari No.Balubur Town Square,Jl. Taman Sari No.3', '0819-1000-897', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1020, 32, 'RASHAWL', 'Jl. Mangga No.36b', '(022) 8588042', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1021, 32, 'Deenay', 'Jl. Kembar Timur No.38', '0822-5558-888', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1022, 32, 'Rumah Urban - Naqeeya Hijab', 'Ruko Lestari, Jl. Manglid No.6, RT.04/RW.11', '0818-187-22', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1023, 32, 'Busana Muslim', 'ITC Kb. Kalapa, Jl. Moh. Toha Blok A3 No.13-14', '(022) 9102244', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1024, 32, 'Jual Hijab Bandung', 'Bumi Pesona Asri F7 No. 11, jl. ahmad shadali', '0852-9497-235', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1025, 32, 'Lytes Hijab', 'perumahan sukamenak indah m-27', '0856-2470-211', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1026, 32, 'Keys Hijab Bandung', 'Seroja Home Residence 2 Blok.B/53', '0819-1908-333', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1027, 32, 'Aretta Hijab', 'Kota Bandung, Komplek Residence no B.9, Antapani', '0817-0285-93', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1028, 32, 'Krupi', 'Jl. Merdeka No.56', '0815-7464-452', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1029, 32, 'grosir manset baju, manset gamis, kerudung, jilbab, fashion', 'Jl. Parakan Ayu Raya No.23', '0819-0832-433', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1030, 32, 'Istana Rok & Galeri Kerudung " SAGALALAYA "', 'Jl. Buah Batu No.164', '(022) 731820', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1031, 32, 'Albani Rumah Jilbab', 'Pasar Balubur, Jl. Tamansari Blok Q No.1', '(022) 8446871', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1032, 32, 'Hijab Murah Kanaya Moms', 'Komplek Pasirjati Resident, Blok K2 No.122', '0812-2406-829', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1033, 32, 'Ami Hijab Collection', '', '0896-7119-292', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1034, 32, 'Teka Hijab', 'Jl. Neglasari', '0882-1174-409', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1035, 32, 'Shudami Hijab Store Dago', 'Jl. Ir. H. Juanda No.151', '0821-1950-523', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1036, 32, 'Aneela Hijab Fashion', 'Jl. Pasir Impun Blok B No.34', '0821-2917-909', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1037, 32, 'Zoya', 'Jl. Merdeka No.56', '(022) 421317', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1038, 32, 'Rumah Hijab Amirah', 'Jalan Griya Bandung Indah Blok A1 No.27, Buahbatu, Bojongsoang', '0812-9000-964', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1039, 32, 'Milyarda Hijab', 'Komp. Margaasih Permai, Jl. Jati Damai No.4', '0812-2227-836', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1040, 32, 'Ftwo.hijab', 'Komplek Antapani Mas No A.40', '0812-1441-046', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1041, 32, 'Hijab Perfecto', 'Jalan Otto Iskandardinata No.70, Pasar Baru Trade Center Lantai 3 Blok A2 No.21-22', '0896-5200-613', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1042, 32, 'RUMAH PRODUKSI KERUDUNG MAI HIJAB', 'CLUSTER ANDALUS, Jl. Griya Cempaka Arum No.4', '0812-2000-202', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1043, 32, 'Sheena hijab', '', '0896-2993-305', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1044, 32, 'Dubai Hijab', 'Jalan Otto Iskandar Dinata No.70, Pasar Baru Trade Center Lantai 5 Blok B2 No.18, Kebon Jeruk, Andir', '0877-0870-822', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1045, 32, 'Monel', 'Jl. Buah Batu No.87', '(022) 731071', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1046, 32, 'Istana Hijab Revaniss', 'Jl. Pungkur No.38', '0812-2390-631', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1047, 32, 'Ameena Hijab Collection', 'Gg. Sukabakti V No.9800', '0877-8618-891', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1048, 32, 'Muiza Hijab', 'Perumahan Bumiasri Padasuka Jl. Tugu Asri III C-1 Kel', '0822-7273-073', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1049, 32, 'elzatta Hijab Online', 'Jl. Cicukang Holis Komplek Industri Prapanca', '0818-0719-256', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1050, 32, 'Shafira Sulanjana', 'Jl. Sulanjana No.26', '(022) 423941', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1051, 32, 'RAIA store', '', '0811-2225-79', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1052, 32, 'Mela Hijab Style', 'Jalan Tamansari, Balubur Town Square Lantai 2 Blok J11', '0813-9518-267', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1053, 32, 'Khalitha Hijab Store', 'Jl. Ambon No.15', '0821-1761-962', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1054, 32, 'Devansha Hijab', 'Balubur Town Square, Jl. Tamansari No.35', '0817-0932-43', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1055, 32, 'avivah hijab', 'Jl. Griya Cempaka Arum No.94', '0813-5416-303', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1056, 32, 'Hijab Geulis', 'Festival Citylink, Jl. Peta', '0853-2150-790', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1057, 32, 'Hijab Cantik', 'Jl. Manisi No.10', '0856-5954-325', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1058, 32, 'Azzahra Kerudung', 'Basement 1 Blok D No.20, Pasar Baru Trade Center, Jalan Otto Iskandar Dinata No.70, Kebon Jeruk, Andir', '0819-1048-430', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1059, 32, 'Hijab Audina', 'Jl. Haji Yasin No.36', '0857-9517-040', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1060, 32, 'Hijab Princess Office', 'Jl. Terusan Rereng Wulung no.255B, Kel', '0812-1474-120', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1061, 32, 'Jilbab Sodai/ Umaimah Style', 'Perumahan Bumi Asri, Jl. Kiara Asri I No.14, Kel', '0857-9364-199', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1062, 32, 'Rea Hijab', 'Gg. Melati', '0821-1615-408', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1063, 32, 'Monjeela Hijab', 'Jl. Gegerkalong Girang No.55', '0856-2400-000', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1064, 32, 'De''Cantiqu', 'Jalan Sindang Reret No.168, Desa Cibiru Wetan Kec.Cileunyi', '(022) 783016', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1065, 32, 'hijab.khaila', '', '0877-1175-303', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1066, 32, 'Naira Alzena Fashion Hijab', 'Ps. Baru Trade Center, Jl. Otto Iskandar Dinata Blok B2 No.70', '(022) 924545', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1067, 32, 'Hijab Geulis', 'Balubur Town Square, Jl. Tamansari No.05', '0878-2331-923', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1068, 32, 'Mirra Hijab', '', '0858-6310-168', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1069, 32, 'Umama Scarf', 'Balubur Town Square Jalan Tamansari No.3 Lantai D2 L.06-07, Kelurahan Taman Sari Kecamatan Bandung Wetan', '0812-9000-131', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1070, 32, 'Hijab Jilbab Printing Voal | Kerudung Gineed Fashion', 'Komplek Griya Bandung Asri, RW.3', '0852-6288-569', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1071, 32, 'Maulidina Hijab', 'Jl. Pelindung Hewan No.9/96, RT.4/RW.7', '0899-9446-24', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1072, 32, 'Jahirah Hijab', '', '0857-4862-381', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1073, 32, 'Ansania Collection', 'Pasar Baru Trade Center Lantai 3 Blok B2 No.53-55, Jalan Otto Iskandar Dinata No.70', '(022) 424661', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1074, 32, 'Azmi Hijab Indonesia', 'Jl. Mahmud No.109', '0811-9192-29', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1075, 32, 'Sattka Daily Hijab', '', '0813-2020-135', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1076, 32, 'Intan Hijab', 'Jl. Asia Afrika No.5', '(022) 7057884', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1077, 32, 'Salwa Hijab', 'Jalan Otto Iskandardinata, Pasar Baru Trade Centre, Lantai.4, Blok.A.1 No.48', '(022) 0000000', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1078, 32, 'Rumah Jilbab Al Bayyinah', 'JL Raya Cipadung, No, . 515 / 123', '(022) 7226799', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1079, 32, 'Mecca Hijab Store', 'Ps. Kosambi, Jl. Jendral Ahmad Yani Blok A No.5', '0813-2067-953', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1080, 32, 'Graha Printing Kain', 'Jl. Batununggal Indah Ⅷ No.11', '0822-1717-471', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1081, 32, 'Gerai Hijab | Pusat Hijab Syar''i - Jilbab Syar''i - Gamis Syar''i - Khimar Antem', 'Jl. Haji Yasin No.36', '0857-2007-272', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1082, 32, 'Desa Hijab', 'Jl. Saluyu Indah II No.8', '0853-7858-403', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1083, 32, 'Hazna Hijab Merdeka', 'Jl. Merdeka No.49', '(022) 426551', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1084, 32, 'Zoya', 'Cihampelas Walk, Jl. Cihampelas', '(022) 206102', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1085, 32, 'Kimi Indonesia', 'Gallery Hijab Story, Jl. Banda No.21', '0857-2288-121', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1086, 32, 'Nadia Fashion Hijab N Food', 'Gg. Garuda', '0813-2189-616', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1087, 32, 'Galery Silvi Hijab', 'Balubur Town Square, Lt.D2 Blok. Y55, Taman Sari', '0815-7326-426', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1088, 32, 'Hijab Syari Aila', '', '0813-2218-186', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1089, 32, 'Koyu Hijab', 'Jl. K.H. Ahmad Sadili Komp. D''pillar Blok A1 No.6', '(022) 779926', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1090, 32, 'El Syari Hijab', '', '0878-0417-190', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1091, 32, 'Hijab syar''i', '', '0896-5149-407', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1092, 32, 'ZOYA BUAHBATU 2', 'Jl. Buah Batu No.249A', '(022) 7351872', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1093, 32, 'Zoya', 'Balubur Town Square, Lantai 2, Blok Y No.3, Jl. Tamansari', '(022) 8446883', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1094, 32, 'La Rizz Hijab', '', '0812-7789-814', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1095, 32, 'AL-Qamar ENZ HIJAB', '', '0821-1680-385', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1096, 32, 'Ameera Hijab Bandung', 'jl. Rancaekek KM. 24,5 (Cluster Zanjabil Town House B7 Samping Kawasan Industri Dwipapuri Abadi', '0823-2043-966', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1097, 32, 'YULIA shoes ,bag and hijab', '', '0812-2223-191', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1098, 32, 'toko jilbab bandung murah', 'Jln babakan sari 3 rt01 rw 09', '0823-8236-876', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1099, 32, 'moezz hijab store', 'Balubur Town Square, Jl. Tamansari No.1', '0819-1002-211', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1100, 32, 'Kerudung voal murah', 'Kp. Sekbrong rt10, RW.07', '0896-3938-496', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1101, 32, 'JILBAB SYIFANA Kerudung Anak Lucu', '', '0857-2224-247', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1102, 32, 'Agen Khayr Hijab (Adiba-Aqila-Shohwah)', 'Komplek Manglayang regency blok H 17 no 15-16', '0896-5554-138', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1103, 32, 'Mellastore Hijab', 'Jl. Pajagalan Gg. Pamarset No.145', '0811-2484-88', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1104, 32, 'Mahkota Hijab Baltos', 'Jl. Tamansari No.22', '0852-1977-296', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1105, 32, 'Abadi Grosir', 'Jalan Riung Sekar No.8 Cisaranten Kidul Kecamatan Gedebage Kompleks Riung Bandung No. 1', '0838-2984-000', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1106, 32, 'yulia shoes bag and hijab', 'pasar pola', '0812-2231-91', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1107, 32, 'PH Hijab', 'Jl. Sindangreret No.9', '0811-2484-48', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1108, 32, 'Meisa Hijab', 'Blok 10 No. A3 Komp. Ujung, Jl. Ujung Berung Indah Permai Ⅵ', '0823-2000-943', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1109, 32, 'RDA Hijab', 'Jalan Cicalengka Raya No. 3 RT 02 RW 06 Kel', '0811-2422-61', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1110, 32, 'Zoya', 'Jl. Rumah Sakit No.131', '0859-5645-533', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1111, 32, 'Rabbani Buah Batu', 'Jl. Buah Batu No.145', '(022) 7351376', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1112, 32, 'SPM Hijab', 'Gang pabrik kulit utara no 641 blok 196B', '0821-2640-062', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1113, 32, 'elzatta Bandung Metro Indah Mall', '', '(021) 8068121', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1114, 32, 'Ordella Hijab', '', '0857-2340-042', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1115, 32, 'Ameera Hijab Indonesia', 'BTC Fashion Mall, Jl. Dr. Djunjunan Blk. G3 No.3', '0811-2200-90', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (889, 31, 'Rumah Hijab Azzahra', 'jl anggrek 3 blok C1 no 12 Taman Bunga Cilame', '0812-2155-188', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (890, 31, 'Krupi Hijab and Accesories Kings Shopping Centre lt 1', 'Jl. Kepatihan', '0812-2111-493', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (891, 31, 'moz5 salon muslimah Dago', 'Samping Es Duren Pa Aip, No.5 F, Jl. Tubagus Ismail Raya Jl. Dago', '(022) 250209', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (892, 31, 'House of Shasmira Buahbatu', 'Jl. Buah Batu No.151-C', '(022) 731466', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (893, 31, 'Lozy Hijab', 'Jl. Ambon No.15', '0812-2030-901', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (894, 31, 'Fashion Muslim Sylla Hijab', 'Jl. Parang V No.87', '0857-9852-060', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (895, 31, 'Geeva Hijab', 'Perumahan Griya Topindo Katapang Blok D no 2', '0838-2009-745', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (896, 31, '38 Butik', '', '0856-1777-74', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (897, 31, 'Chassia', 'Bandung Indah Plaza, Jl. Merdeka No.56', '(022) 424071', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (898, 31, 'Youthscarf Indonesia', 'Head Office, Jl. Pendawa No.30 A', '0822-1605-265', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (899, 31, 'Hijab.azzalea', 'Unnamed Road', '0878-2744-425', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (900, 31, 'Nonnetedy', 'Pasar Baru Trade Center, Lantai.4, Blok.A1 no 41', '0812-2059-374', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (901, 31, 'Youthscarf Baltos', 'Offline Store, Balubur Town Square D2 Floor Y59 60 Jl Taman Sari No. P-2', '0813-9460-214', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (902, 31, 'Balubur Town Square', 'Jl. Tamansari No.P-2', '(022) 3000310', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (903, 31, 'Label Baju Murah', 'Jl. Permata Indah I No.6', '0818-0215-404', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (904, 31, 'KINAN JASMINE HIJAB', 'Jl. Rereongan Sarupi No.21 A', '0896-7892-076', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (905, 31, 'Syamsiar Hijab', '', '0815-1504-11', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (906, 31, 'Bandung Design', 'Jl. Pasir Pogor Raya', '0812-2282-228', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (907, 31, 'Hijab Modis', '', '0812-2124-31', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (908, 31, 'PT. Elizabeth Hanjaya', 'Jl. Ibu Inggit Garnasih No.12', '(022) 520112', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (909, 31, 'Pabrik Kerudung', 'Jl. Raya Cicalengka Timur No.470', '0857-8899-990', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (910, 31, 'Hijab Swimming Pool', 'Jl. Grand Hotel No.33E', '(022) 278776', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (911, 31, 'Marlien Hijab', 'Sayang', '0813-9400-401', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (912, 31, 'Samsat Outlet ITC Kebon Kelapa', 'ITC Kebon Kelapa, Jl. Moh. Toha', '0800 140181', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (913, 31, 'grosirhijabbagus.com', 'Gg. H. Bakar 1', '0895-3525-4306', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (914, 31, 'LIDI HOUSE', 'Jl. Bojong Tanjung', '0822-2555-008', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (915, 31, 'GROSIR KERUDUNG dan KONVEKSI SAMAWA HIJAB', 'RT 03 RW 05 kampung simpen desa tenjolayakecamatan cicalengka', '0823-2222-142', '2020-10-20 14:29:29+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1116, 32, 'Rumah Hijab Azzahra', 'jl anggrek 3 blok C1 no 12 Taman Bunga Cilame', '0812-2155-188', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1117, 32, 'Krupi Hijab and Accesories Kings Shopping Centre lt 1', 'Jl. Kepatihan', '0812-2111-493', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1118, 32, 'moz5 salon muslimah Dago', 'Samping Es Duren Pa Aip, No.5 F, Jl. Tubagus Ismail Raya Jl. Dago', '(022) 250209', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1119, 32, 'House of Shasmira Buahbatu', 'Jl. Buah Batu No.151-C', '(022) 731466', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1120, 32, 'Lozy Hijab', 'Jl. Ambon No.15', '0812-2030-901', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1121, 32, 'Fashion Muslim Sylla Hijab', 'Jl. Parang V No.87', '0857-9852-060', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1122, 32, 'Geeva Hijab', 'Perumahan Griya Topindo Katapang Blok D no 2', '0838-2009-745', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1123, 32, '38 Butik', '', '0856-1777-74', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1124, 32, 'Chassia', 'Bandung Indah Plaza, Jl. Merdeka No.56', '(022) 424071', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1125, 32, 'Youthscarf Indonesia', 'Head Office, Jl. Pendawa No.30 A', '0822-1605-265', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1126, 32, 'Hijab.azzalea', 'Unnamed Road', '0878-2744-425', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1127, 32, 'Nonnetedy', 'Pasar Baru Trade Center, Lantai.4, Blok.A1 no 41', '0812-2059-374', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1128, 32, 'Youthscarf Baltos', 'Offline Store, Balubur Town Square D2 Floor Y59 60 Jl Taman Sari No. P-2', '0813-9460-214', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1129, 32, 'Balubur Town Square', 'Jl. Tamansari No.P-2', '(022) 3000310', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1130, 32, 'Label Baju Murah', 'Jl. Permata Indah I No.6', '0818-0215-404', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1131, 32, 'KINAN JASMINE HIJAB', 'Jl. Rereongan Sarupi No.21 A', '0896-7892-076', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1132, 32, 'Syamsiar Hijab', '', '0815-1504-11', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1133, 32, 'Bandung Design', 'Jl. Pasir Pogor Raya', '0812-2282-228', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1134, 32, 'Hijab Modis', '', '0812-2124-31', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1135, 32, 'PT. Elizabeth Hanjaya', 'Jl. Ibu Inggit Garnasih No.12', '(022) 520112', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1136, 32, 'Pabrik Kerudung', 'Jl. Raya Cicalengka Timur No.470', '0857-8899-990', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1137, 32, 'Hijab Swimming Pool', 'Jl. Grand Hotel No.33E', '(022) 278776', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1138, 32, 'Marlien Hijab', 'Sayang', '0813-9400-401', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1139, 32, 'Samsat Outlet ITC Kebon Kelapa', 'ITC Kebon Kelapa, Jl. Moh. Toha', '0800 140181', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1140, 32, 'grosirhijabbagus.com', 'Gg. H. Bakar 1', '0895-3525-4306', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1141, 32, 'LIDI HOUSE', 'Jl. Bojong Tanjung', '0822-2555-008', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1142, 32, 'GROSIR KERUDUNG dan KONVEKSI SAMAWA HIJAB', 'RT 03 RW 05 kampung simpen desa tenjolayakecamatan cicalengka', '0823-2222-142', '2020-10-20 14:31:34+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1266, 34, 'Yamaha MM motor Bandung (Leo Motor)', 'Jl. Jatihandap no. 292 A-B', '0831-0756-498', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1267, 34, 'JASA SERVIS DAN REPARASI TENGKI MOTOR BANDUNG', 'Jl. Soekarno-Hatta Gg, Haji Hasan II No.46, RT.1/RW.7', '0838-2988-739', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1268, 34, 'Kusumah Motor', 'Jl. BKR No.122 B', '(022) 520128', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1269, 34, 'Kredit Motor Honda Termurah Di bandung', 'Sukarasa, Jl. Gegerkalong Hilir No.50A, RT.04', '0878-2571-808', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1270, 34, 'Promosi Kawasaki Bandung', 'Jl. Jend. H. Amir Machmud No.121', '0857-2300-734', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1271, 34, 'Kredit DP Murah Motor Yamaha Bandung Online', 'Jalan Ahmad Yani No. 669 Bandung Apartemen Gateway Cicadas unit LG EB-05', '0822-5594-355', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1272, 34, 'Merdeka Motor Kiara Condong', 'Jl. Terusan Kiaracondong No.47', '(022) 730566', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1273, 34, 'Arena Motor', 'Jl. Gatot Subroto No.180', '(022) 730071', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1274, 34, 'Yamaha Bandung Raya', 'Jl. Dr. Setiabudi No.157', '(022) 202100', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1275, 34, 'Kredit Cash motor honda bandung', 'Jl. A.H. Nasution No.57', '0895-8000-7127', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1143, 34, 'Djm Motor Jual Beli Motor Bekas Bandung', 'Jl. Tubagus Ismail Raya', '0813-2206-163', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1144, 34, 'Ledeng Motor Bandung', 'Jl. Dr. Setiabudi', '0878-8555-507', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1145, 34, 'Bandung Motor', 'no, Jl. Jendral Gatot Subroto No.389', '(022) 731200', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1146, 34, 'Wahana Baru Motor Bandung', 'Jl. Ciwastra No.201', '0896-3024-693', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1147, 34, 'Yamaha Yonk Jaya Motor', 'Jalan Jendral Ahmad Yani No. 350 E - F', '(022) 721677', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1148, 34, 'Sunda Motor', 'Jl. Sunda No.26', '(022) 423561', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1149, 34, 'Venus Motor Bandung', 'Jl. Lengkong Besar No.91', '(022) 423207', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1150, 34, 'Honda Astra Motor Bandung', 'Jl. Jend. Sudirman', '0812-7887-256', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1151, 34, 'Honda Naga Mas Bandung', 'Jl. Soekarno-Hatta No.529', '(022) 732129', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1152, 34, 'Nusantara Motor', 'Jl. Jendral Ahmad Yani No.332', '(022) 721500', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1153, 34, 'Yonk Jaya Motor', 'Jl. Jendral Ahmad Yani No.358', '(022) 727133', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1154, 34, 'SKA MOTOR', 'Jl. Peta No.217', '(022) 604018', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1155, 34, 'Hadi Motor Bandung', 'Jl. PH.H. Mustofa No.153', '(022) 720010', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1156, 34, 'Lombok Motor Bandung', 'Jl. Lombok No.11 S', '0822-1497-712', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1157, 34, 'Viar motor ib bandung', 'Jl. Ibu Inggit Garnasih No.130-132', '0812-2079-778', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1158, 34, 'Riung Bandung Motor', 'Jl. Raya Cipamokolan No.40', '(022) 750018', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1159, 34, 'Sewa motor bandung', 'Jl. Batununggal Molek II No.5', '0821-2659-600', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1160, 34, 'Nizaam Motor Bandung', 'Jl. Sawah Kurung IV No.17', '0853-5357-232', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1161, 34, 'Galuh Motor Bandung', 'Jl. Sukagalih No.62', '0812-1400-63', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1162, 34, 'Surya Motor', 'Jalan Ibu Inggit Garnasih (Ciateul) No.109', '0811-2223-83', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1163, 34, 'GT Motor Bandung', 'Jl. Pasirluyu Timur No.79', '0813-2188-095', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1164, 34, 'Grosir Ban Motor Bandung', 'Jl. Jati Utama No.1A', '0812-2264-372', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1165, 34, 'Alarm Motor Bandung', 'Gg. Warga, RT.02/RW.09', '0896-1347-267', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1166, 34, 'JUARA Sewa Motor Bandung', 'Jl. Babakan Tarogong No.395', '0895-3516-4080', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1167, 34, 'Abdurrahman alarm motor bandung', 'Jl. Nasional III No.258', '0838-2058-449', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1168, 34, 'Bengkel MM Motor Bandung', 'Jl. Maleber Utara Gg. Bakti VII No.42', '0896-5628-354', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1169, 34, 'Bandung Jaya Motor', 'Jl. Gatot Subroto No.51', '(022) 732287', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1170, 34, 'Mj Motor Bandung', 'Jl. Raya Tagog Cinunuk No.188', '0812-2006-333', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1171, 34, 'Cat motor bandung - karbon kevlar bandung', 'Jl. Kawaluyaan Indah VII No.21', '0813-2017-058', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1172, 34, 'Nusantara Motor', 'Jl. Jendral Ahmad Yani No.332', '(022) 721600', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1173, 34, 'PT. Astra Daihatsu Motor Bandung', 'Jl. Soekarno-Hatta No.459', '0821-1539-970', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1174, 34, 'Yamaha Bahana Motor Bandung', 'Jl. Babakan Ciparay', '(022) 9299137', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1175, 34, 'Jual Beli Motor Bandung SM', 'Belakang Saung Rengganis ibu Kuntum, Jl. Rereongan Sarupi No.73C', '0811-2090-90', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1176, 34, 'Kredit motor honda bandung', 'Jl. Terusan Pasirkoja No.66', '0813-1240-840', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1177, 34, 'Palem Motor Bandung', 'Jl. Sadang Serang', '0897-2025-24', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1178, 34, 'Modifikasi motor bandung', 'Jl. Pungkur No.120', '0878-0065-133', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1179, 34, 'HARGA JUAL MOTOR BANDUNG', 'Jl. Ujungberung Indah Raya No.Blok 15/10', '0896-1154-338', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1180, 34, 'GPS Motor Bandung', 'Jl. Cisitu Baru No.60', '0811-3333-445', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1181, 34, 'Yamaha Fortuna Motor Bandung', 'Jl. Kiara Condong No.401A', '(022) 730666', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1182, 34, 'Sona Motor Bandung', 'Jalan Terusan Saluyu Jl. Ciwastra No.344', '(022) 8731461', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1183, 34, 'Body Motor', 'Jl. Naripan No.100', '0813-1458-133', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1184, 34, 'Kredit Motor Honda Bandung Cimahi - Dealer Honda Bandung', 'Jl. Kopo Sayati Km 7 No. 84, Dealer Motor Honda Bandung, Kredit Motor Honda Bandung, Bandung', '0878-2422-267', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1185, 34, 'Cargonesia Express Bandung | Jasa Ekspedisi Cargo | Kirim Mobil | Kirim Motor', 'Jl. Marga Cinta Komplek Bahagia Permai Blok BP1 No. 10', '0811-2273-93', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1186, 34, 'SALMA MOTOR BANDUNG', 'Jl. Satria Raya No.6', '0895-2872-964', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1187, 34, 'GP Motor', 'Jl. Jenderal Gatot Subroto No.205', '0851-0610-191', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1188, 34, 'Java Motor', 'Jl. Moch. Ramdan No.47', '0878-7727-813', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1189, 34, 'HS JAYA MOTOR BANDUNG', 'Jl. Caringin No.317', '(022) 541082', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1190, 34, 'Honda Aceh Motor', 'Jl. Aceh No.31', '(022) 423996', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1191, 34, 'Distributor Motor Mini Bandung', 'Jl. Sukaraja II', '0896-3177-934', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1192, 34, 'Adjie Motor', 'Jl. Pungkur No.235', '0812-9799-99', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1193, 34, 'Niko Motor', 'Jl. Kalipah Apo No.62-64', '0815-6021-11', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1194, 34, 'Variasi 99 motor Bandung', 'Bawah masjid Al mutaqqin kontrakan Bp.H.Agus, Kp.kertasari, kel, RT.2/RW.7', '0896-9023-043', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1195, 34, 'Maju Motor', 'Jl. Pungkur No.82D', '0899-7827-42', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1196, 34, 'Bandung Motor', 'Jl. Gunung Batu', '(022) 200169', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1197, 34, 'Honda Daya Motor Soekarno Hatta Bandung (Official)', 'Jl. Soekarno-Hatta No.518', '(022) 750900', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1198, 34, 'Dealer motor suzuki bandung - menerima cash & kredit', 'Jl. Terusan Jakarta No.16', '0896-4769-666', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1199, 34, 'KAWASAKI BANDUNG MOTOR', 'Jl. Soekarno-Hatta No.590', '0813-2000-022', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1200, 34, 'PROJAYA MOTOR BANDUNG CIATEUL', 'Jl. Ibu Inggit Garnasih No.162', '0877-2272-259', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1201, 34, 'HARGA MOTOR HONDA BANDUNG - HargaMotorHondaBandung.com - Dealer Bandung', 'Jl. Terusan Pasirkoja No.66', '0821-3050-490', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (678, 28, 'Nadira Hijab', 'Jalan PG. Soedhono, Desa No.21', '0812-7106-700', '2020-10-20 14:19:43+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (679, 28, 'Nadira Hijab', 'Jl. Nogososro Jl. Ngemplak Raya No.10', '0896-6742-955', '2020-10-20 14:19:43+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (680, 28, 'Nadiraa Hijab', 'Jl. Selokan Mataram No.451', '0857-1135-483', '2020-10-20 14:19:43+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (681, 28, 'Nadira hijab jepara', 'Desa Bawu RT.32/RW.07', '0896-0605-034', '2020-10-20 14:19:43+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (682, 28, 'OL SHOP NADIRA HIJAB', '', '0813-2904-052', '2020-10-20 14:19:43+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (683, 28, 'Nobby Hijab JCM', 'Unnamed Road', '0877-8082-587', '2020-10-20 14:19:43+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (684, 28, 'Gerai Rayfa (Nadira Hijab)', 'Pondok Ungu Permai Blok G3 No 6 Kaliabang Tengah, RT.001/RW.013', '0812-9180-448', '2020-10-20 14:19:43+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (685, 28, 'Nadira Hijab', '', '0857-9320-006', '2020-10-20 14:19:43+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (686, 28, 'Arsyeela Hijab ( Jilbab Bolak-Balik Jogja)', 'Jogokariyan No.mj 3/609, Jl. Cuwiri', '0811-2543-33', '2020-10-20 14:19:43+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (687, 28, 'GNC(grosirnadiracollection)', '', '0856-4339-635', '2020-10-20 14:19:43+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (688, 28, 'Hijab Alila Jogja', 'Jl. Kapten Haryadi Gg. Kantil No.118', '0895-3928-6046', '2020-10-20 14:19:43+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1202, 34, 'Suzuki motor bandung', 'Jl. Raya Soreang', '0816-4953-532', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1203, 34, 'Cianjur Motor', 'Jl. Gatot Subroto No.178A', '(022) 731097', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1204, 34, 'Asia Motor', 'Jl. Jendral Ahmad Yani No.216', '(022) 710141', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1205, 34, 'Servis jok motor,bengkel jok motor,servis kulit jok motor,jok motor bandung', 'Jl. Raya Cijerah No.mor 35', '0813-2015-070', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1206, 34, 'Bandung Jaya Motor', 'Jl. Terusan Buah Batu No.265', '0852-7035-525', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1207, 34, 'PHIN DETAILING', 'Jl. A.H. Nasution', '0811-2232-92', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1208, 34, 'Rental Motor Bandung Motorrent', 'JL Arumsari 7 RT/RW 05/12', '0898-3401-87', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1209, 34, 'Wahana Ritel Honda Bandung', 'Jl. Abdul Rahman Saleh No.53', '(022) 602109', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1210, 34, 'Chicago Motor Bandung', 'Jl. Cigadung Raya Timur No.130', '0813-2112-367', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1211, 34, 'Cahaya Motor', 'Jl. A.H. Nasution', '(022) 721631', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1212, 34, 'Jaya Mandiri Motor Bandung Specialist Kaca Film', 'PASAR SEGAR, Jl. Taman Kopo Indah 2', '0812-9524-145', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1213, 34, 'PD. Aneka Motor Bandung', 'Jl. Naripan', '(022) 420765', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1214, 34, 'Ahass 06541 Sampurna Motor - surapati, Bandung', 'Jl. Surapati No.185', '0821-2637-973', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1215, 34, 'Kawasaki motor bandung', 'Jl. Soekarno-Hatta No.590', '0877-2120-468', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1216, 34, 'YAMAHA DETA MOCHAMMAD TOHA - RIDWAN YAMAHA BANDUNG', 'Jl. Moh. Toha No.141', '0856-9491-377', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1217, 34, 'DC Motor Riung Bandung', 'Jl. Riung Arum Raya No.51 D', '0813-9447-961', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1218, 34, 'Lapak motor clasic bandung', 'Jl. Terusan Buah Batu Jl. Gotong Royong No.108, RT.05/RW.06/RW', '0898-7001-22', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1219, 34, 'Logos Variasi Motor', 'Jl. Pungkur No.178', '0895-3345-1933', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1220, 34, 'ML custom bandung', 'Jl. Cipamokolan Kolot', '0878-2522-455', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1221, 34, 'Sahabat Motor Bandung', 'Jl. Baladewa D No.4', '0877-2244-669', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1222, 34, 'Shakila Rental Motor', 'Jl. Gagak Jl. Surapati No.9', '0812-8970-687', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1223, 34, 'DEALER MOTOR SUZUKI BANDUNG. KREDIT MOTOR SUZUKI DI BANDUNG', 'Jl. Terusan Jakarta No.16', '0821-2926-186', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1224, 34, 'Sanjaya Motor', 'Jl. Ibu Inggit Garnasih No.136', '(022) 520022', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1225, 34, 'Dwikarya Motor', 'Jl. Pungkur No.169 C', '(022) 421398', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1226, 34, 'Jevan Motor', 'Jl. Suniaraja No.130 A', '(022) 420275', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1227, 34, 'Alif Motor Bandung', 'Jl. BKR No.132', '(022) 7120474', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1228, 34, 'Bengkel TOP 1 Yong''s Motor', 'Jl. Lengkong Kecil No.44', '(022) 420758', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1229, 34, 'Bee Motor Bandung', 'Jl. Caringin No.351', '(022) 9253853', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1230, 34, 'Raka Motor', 'Jl. Terusan Buah Batu No.54', '(022) 756543', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1231, 34, 'PD Wijaya Motor', 'Jl. Jend. Sudirman No.257', '(022) 603631', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1232, 34, 'IB Motor Kawasaki', 'Jl. Ibu Inggit Garnasih', '0856-5927-869', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1233, 34, 'Bandung Sewa Motor', 'Jl. Melati II No. 30 Blok 8 Perumahan Sadang Serang Kel. Sekeloa Kec Coblong Patokan : SDN Neglasari 189', '0821-3031-941', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1234, 34, 'AMREN - sewa motor bandung - sewa mobil bandung - PT Invento', 'Jl. Cihampelas No.119', '0811-2288-00', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1235, 34, 'Minggu Jaya Motor', 'Jln Mohammad Ramdan No.86', '0819-1033-366', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1236, 34, 'TJP RENTAL MOTOR BANDUNG', 'Jl. Cirapuhan No.43 Dago', '0818-220-65', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1237, 34, '35 Motor', 'Jl. Terusan Martanegara No.5', '(022) 730202', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1238, 34, 'Mega Motor', 'Jl. Pungkur No.109', '(022) 423882', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1239, 34, 'Cendana Motor', 'Jl. Moh. Toha', '(022) 522790', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1240, 34, 'Omega Motor Lengkong Besar - Omegamobil.com', 'Jl. Lengkong Besar No.78-82', '(022) 423894', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1241, 34, 'Santosa Motor', 'Jl. Jendral Ahmad Yani No.304', '(022) 710044', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1242, 34, 'Toko dan Bengkel King Motor', 'Jl. Pagarsih No.200', '(022) 604522', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1243, 34, 'Gudang & Distributor Motor Mini Bandung @IRG MOTOR', 'Tempat penyewaan Garasi H.Ujang, Jalan Terusan Pasir Koja Babakan Irigasi Seberang, Gg. Amd 10', '0856-2446-633', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1244, 34, 'ENEOS Cahaya Motor Bandung (Eneos Cahaya Motor)', 'Jl. Purwakarta No.107', '0857-9416-284', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1245, 34, 'Naripan Motor', 'No.3-5-5A, Jl. Naripan', '(022) 420674', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1246, 34, 'CV. Jakarta Motor', 'Jl. Banceuy No.26', '(022) 422142', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1247, 34, 'Rejeki Motor', 'Jl. Lengkong Besar No.111E', '(022) 421003', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1248, 34, 'YSS Aldora Motor', 'Jl. Jakarta No.36D', '(022) 727582', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1249, 34, 'New Champion Motor Volkswagen Bandung', 'Jl. Soekarno-Hatta No.291', '0856-2251-00', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1250, 34, 'Solex Motor Sport Bandung', 'Jl. Suniaraja No.29', '(022) 420009', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1251, 34, 'Turangga Raya Motor', 'Jl. Turangga No.15', '(022) 730230', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1252, 34, 'Dr Motor', 'Jl. Peta No.251', '(022) 603655', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1253, 34, 'Kawasaki Motor Bandung Pusat', 'Samping Metro Indah Mall, Jl. Soekarno-Hatta No.590', '0857-2200-333', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1254, 34, 'Hadi Motor', 'Jl. A.H. Nasution', '(022) 721169', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1255, 34, 'Kredit Motor Honda Bandung Gelora', 'Jl. Terusan Pasirkoja No.66', '0813-2004-005', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1256, 34, 'JAYA MANDIRI MOTOR BANDUNG', 'PASAR SEGAR, Jl. Taman Kopo Indah 2 No.38', '0812-2055-185', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1257, 34, 'Makmur Motor', 'Jl. Banceuy No.25', '0851-0922-211', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1258, 34, 'Sam Motor', 'Jl. Margacinta No.216', '(022) 9383627', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1259, 34, 'Agung Motor', 'Jl. Otto Iskandar Dinata No.506', '0818-0958-882', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1260, 34, 'HARGA MOTOR HONDA BANDUNG', 'Jl.Raya Barat No.816', '0821-1504-115', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1261, 34, 'Aegon motor', 'Jl. Peta No.215', '(022) 600618', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1262, 34, 'Prambanan Motor', 'Jl. A.H. Nasution No.107', '(022) 781684', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1263, 34, 'Kredit Motor Honda Bandung - Honda Nagamas Motor Cimahi', 'Jl.Raya Barat No.816', '(022) 662636', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1264, 34, 'Sugih Jaya Motor', 'Jl. Gatot Subroto No.391', '(022) 731958', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1265, 34, 'Bintang Motor Margaasih', 'Jl. Raya Nanjung No.3', '0896-9048-918', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1276, 34, 'Remaja Motor', 'Jl. Sasak Gantung No.22', '(022) 423392', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1277, 34, 'Leo Motor - Kredit Murah Motor Yamaha Bandung', 'Jl. Jatihandap No.292', '0812-2014-285', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1278, 34, 'Yamaha Aneka Jasa Motor', 'Jl. Ibu Inggit Garnasih', '(022) 520070', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1279, 34, 'Milan Motor', 'Jl. Pungkur No.109 A', '(022) 421807', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1280, 34, 'Sinar Jaya Motor', 'Jl. Raya Kopo No.446', '0852-7374-924', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1281, 34, 'CMS CUTTING STICKER BANDUNG', 'Jl. Gunung Batu No.166', '0822-1914-559', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1282, 34, 'Bengkel TOP 1 Motor Saung Pajajaran', 'Jl. Pajajaran No.80 D', '(022) 601852', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1283, 34, 'Pusat kredit motor honda bandung', 'Jalan Raya Leuwigajah No.157, Kel Utama, Cimahi Selatan', '0878-2392-223', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1284, 34, 'Hendra Motor', 'Jl. Banceuy No.32', '(022) 423649', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1285, 34, 'Sans Rental Motor Bandung', 'Jl. Stasiun Barat', '0858-7105-995', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1286, 34, 'SYM Bandung', 'Jl. Ibrahim Adjie No.356', '0895-2447-528', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1287, 34, 'Kredit DP Termurah Motor Yamaha Bandung Cimahi', 'Jl. Moh. Toha No.141', '0859-7422-592', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1288, 34, 'SagalaAya MotoShop', 'Ruko Kurdi Regency, Ruko, Jl. Kurdi Barat I No.7B', '0857-2143-353', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1289, 34, 'Salon Mobil Nano Coating Ceramic Kaca Film Audio Mobil Bandung - Slamet Motor', 'Citra Panyileukan AB 6 No, Soekarno-Hatta St No.8', '0852-2222-223', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1290, 34, 'Aksesoris NMax ( MAX 24 )', 'Jl. A.H. Nasution No.102', '0895-0909-163', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1291, 34, 'Detailing Mobil Motor - Salon Mobil Nano Ceramic Coating Bandung', 'Jl. Cisaranten Kulon No.46', '0812-8692-344', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1292, 34, 'Kawasaki Gamma Motor', 'Jl. Taman Kopo Indah 2', '(022) 541576', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1293, 34, 'Pratama Motor', 'Jl. Soekarno-Hatta No.463', '(022) 520342', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1294, 34, 'Jaya Mandiri Motor Bandung', 'PASAR SEGAR, Jl. Taman Kopo Indah 2', '0822-1109-228', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1295, 34, 'VIAR JARI SAKTI BANDUNG', 'Jl. Raya Kopo No.102', '0821-1901-000', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1296, 34, 'Anugrah Jaya Motor', 'Jl. Pungkur No.173', '(022) 420064', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1297, 34, 'Gadai Motor Bandung', 'Jl. Jend. H. Amir Machmud No.22', '0857-2006-333', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1298, 34, 'Harga Dan Kredit Motor Yamaha Bandung : Kang Adi', 'Jl. Jend. H. Amir Machmud No.462', '0812-9968-994', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1299, 34, 'Subur Jaya Motor', 'Jl. Raya Cijerah No.5', '(022) 7030935', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1300, 34, 'Sutanto Motor', 'Jl. A.H. Nasution No.161', '(022) 710169', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1301, 34, 'Omega Motor & Bakso Malang Langgeng', 'Jl. Terusan Buah Batu No. 39 Bandung Telp. (022) 7506662 - (022)7509863, Fax : (022) 7506663', '(022) 750666', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1302, 34, 'Yamaha STAR SARIJADI', 'Jl. Sarijadi Raya No.54', '(022) 200394', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1303, 34, 'Press Body Motor', 'Jl. Pajajaran No.80 C', '0812-8499-449', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1304, 34, 'Buana Motor', 'Jl. Holis', '(022) 600188', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1305, 34, 'Kaisar', 'Jl. Abdul Rahman Saleh No.64', '(022) 601121', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1306, 34, 'JATI MOTOR', 'Jl. Kebon Jati No.26', '(022) 423544', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1307, 34, 'Mangele Stiker Premium', 'Jl. Durman No.24C', '0812-2772-279', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1308, 34, 'Rejeki Motor(RPM Motor)', 'Jl. Cihampelas No.159', '(022) 203396', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1309, 34, 'Hidrolik Cuci Mobil / Motor Bandung Servis Repair Instalation Spare Part', 'Gang Andir Kulon II', '0811-2242-21', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1310, 34, 'Showroom mobil GP Motor', 'Jl. R.A.A. Marta Negara No.44', '0818-0227-500', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1311, 34, 'Bengkel Mobil Monroe 99 Bandung', 'Jl. Parakan Saat No.4', '0812-2330-044', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1312, 34, 'Cahaya Abadi Motor', 'Jl. Ibu Inggit Garnasih No.63', '(022) 520050', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1313, 34, 'Maju Bersama Motor Bandung', 'Jalan Melong Blok Sakola Muntilan 1 no 4 RT 01/07', '0817-615-43', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1314, 34, 'AHASS 2650', 'Jl. Jend. Sudirman No.255', '(022) 9329218', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1315, 34, 'Kawasaki Adijaya Motor', 'Jl. Jamika No.106', '0896-6672-899', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1316, 34, 'Wuling motors bandung pusat', '', '0857-1109-146', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1317, 34, 'Lucky Jaya Motor', 'Jl. Terusan Kiaracondong', '(022) 750062', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1318, 34, 'aji motor', 'Jl. Dewi Sartika No.39', '0815-2292-284', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1319, 34, 'MAKMUR PRESS MOTOR', 'Jl. Caringin No.218', '0899-7088-09', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1320, 34, 'Mobil Toko Bandung', 'komplek, Jl. Singgasana Raya', '0812-2128-901', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1321, 34, 'Make up motor bandung', 'Jl. Raya Bojongsoang No.136', '0895-8001-9875', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1322, 34, 'Vectoren Rental Motor', 'Jl. Dago Asri IV No.4', '0813-1266-747', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1323, 34, 'TVS Bandung', 'Jl. Dr. Djunjunan', '(022) 7879410', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1324, 34, 'Bengkel Lingga Motor', 'Jl. A.H. Nasution No.65', '0857-2194-212', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1325, 34, 'Kawasaki Bandung Pusat / Sales Indra', 'Jl. Soekarno-Hatta No.590', '0813-2002-016', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1326, 34, 'PT Daya Adicipta Motora', 'Jl. Raya Cibeureum No.26', '(022) 605103', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1327, 34, 'Rental Motor Dago', 'Jl. Ir. H. Juanda No.309', '0812-2256-789', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1328, 34, 'SUPER01 Terima pesanan Cutting Stiker dan spesialis plat nomor mobil/motor', 'Jl. Terusan Galunggung No.1', '0898-7127-21', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1329, 34, 'BAlarea BAndung Motor', 'Jl. Ibrahim Adjie No.47', '0812-2165-642', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1330, 34, 'AJM Motor', 'Jl. Asmi No.5', '0812-1448-26', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1331, 34, 'Fredy Motor', 'Jl. Soekarno-Hatta No.297', '(022) 522202', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1332, 34, 'Shaffira Motor', 'Jl. Ibrahim Adjie No.421', '0812-2049-78', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1333, 34, 'KIA Siloam Motor', 'Jl. Soekarno-Hatta No.471', '(022) 522700', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1334, 34, 'kredit motor honda di bandung', 'Jl. Mahar Martanegara No.157- 159', '0822-1696-256', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1335, 34, 'Yamaha Motor', 'Jl. Elang', '(022) 600585', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1336, 34, 'Viona Motor', 'Jl. Cikutra No.201', '0878-2104-407', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1337, 34, 'Central abadi motor', 'Jl. Raya Cijerah No.250', '0822-9264-670', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1338, 34, 'Motor rx king 2002', '', '0821-8799-311', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1339, 34, 'Holiday Motor', 'Jl. Jendral Ahmad Yani No.699', '(022) 727293', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1340, 34, 'Yamaha Flagship Shop Bandung Official', 'Jl. Soekarno-Hatta No.474a', '0812-2275-55', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1341, 34, 'Bintang Motor', 'Jl. Raya Kopo No.550', '0857-9439-989', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1342, 34, 'Naga Mas Motor', 'Jl. Pasir Jati Timur', '(022) 9324820', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1343, 34, 'Asco Motor', 'Jl. Abubakar No.161 A', '(022) 423949', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1344, 34, 'Jaya Motor Katapang', 'Jl. Katapang No.4', '(022) 730192', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1345, 34, 'WARNAWARNI Bengkel Cat Motor', '', '0838-2126-255', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1346, 34, 'KJV Motosport Bandung', 'Jl. Buah Batu', '0878-7829-988', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1347, 34, 'Yamaha Lingkar Motor', 'Jl. Peta No.54', '(022) 523164', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1348, 34, 'I Burn Rust By Dye Restoration ( JASA CAT MOTOR )', 'Jl. Udawa No.25', '0895-6326-6657', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1349, 34, 'Yamaha Sentra Anugrah Motor - Dipatiukur', 'Jl. Dipati Ukur Kelurahan No.75', '0821-3053-055', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1350, 34, 'GlanetsKustom MotorSickles', 'Jl. Temanggung No.27', '0878-2544-491', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1351, 34, 'Venus Variasi Motor', 'Jl. Lengkong Besar No.93', '(022) 420902', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1352, 34, 'Sewa dan Buat Jas di Bandung || Jovian Rental Motor Bandung', 'Jl. Pasir Kaliki No.50', '0811-222-41', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1353, 34, 'Rian Motor', 'Jl. Sari Asih No.63', '0817-4948-34', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1354, 34, 'Dealer Nissan Bandung (Marketing)', 'No.382, No, Jl. Soekarno-Hatta', '0823-1770-516', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1355, 34, 'Merdeka Motor Toyota', 'Jl. Raya Kopo', '(022) 8544058', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1356, 34, 'Honda Bandung Center', 'Jl. Cicendo No.18', '(022) 424088', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1357, 34, 'Nusantara Sakti', 'Jl. Sukaraja No.277', '(022) 604680', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1358, 34, 'ABC Motor 2', 'Jl. Pajajaran No.16', '(022) 426557', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1359, 34, 'Kursus Mekanik Motor LPK Indonesia Produktif', 'Jl. Gegerkalong Hilir No.165', '0812-2254-907', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1360, 34, 'Dealer motor honda bandung-cimahi', 'Jl. Gunung Batu No.41', '0822-6226-500', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1361, 34, 'Pt. Wahana Artha Harsaka', 'Jl. Abdul Rahman Saleh No.68-A', '0898-4381-78', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1362, 34, 'Sejahtera Motor', 'Jl. Kiara Condong No.414', '(022) 733313', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1363, 34, 'Nusa Motor', 'Jl. Raya Cileunyi No.32', '(022) 783025', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1364, 34, 'Suzuki Motor Service Center', 'Jl. Dr. Djunjunan No.mor 9', '(022) 612022', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1365, 34, 'Rajawali Abadi Motor', 'Jl. Rajawali Barat No.194C', '0812-2032-507', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1366, 34, 'Utama Jaya Motor', 'Jl. Ibu Inggit Garnasih No.25B', '(022) 522200', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1367, 34, 'Panjunan Motor (Distributor Jas Hujan dan Helm)', 'Jl. Panjunan No.39A', '(022) 522801', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1368, 34, 'Eddi Motor', 'Jl. Ciwastra No.243', '(022) 756472', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1369, 34, 'Bengkel Dinamo Mobil D2 ,Cicaheum, Bandung', 'Jl. Jendral Ahmad Yani No.906', '0823-2114-172', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1370, 34, 'Rumah Volkswagen Bandung', 'Jalan AH Nasution No.60 RT 01/RW 03', '(022) 780066', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1371, 34, 'SANJAYA MOTOR', 'Sanjaya Motor, Jl. Ibu Inggit Garnasih No.136', '0812-2437-282', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1372, 34, 'Jaya Super Motor', 'Jl. Jendral Ahmad Yani No.293', '(022) 727130', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1373, 34, 'Honda Tunas Dwipa Matra Bandung Cimahi', 'Jl. Raya Gadobangkong No.70 40552', '0838-2065-303', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1374, 34, 'Dealer Resmi Mobil Suzuki Bandung', 'Jl. Soekarno-Hatta No.513', '0812-2279-689', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1375, 34, 'Bengkel Mobil 24 Jam Bandung', 'Dekat warteg, Jl. Dr. Djunjunan No.124A', '0852-2117-747', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1376, 34, 'Bengkel Spesialis Mobil Toyota & Daihatsu Bandung', 'Jl. Terusan Sindang Barang No.18', '0856-2424-248', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1377, 34, 'Jual beli Motor Honda', 'Jl. Jend. Sudirman No.566-568', '0856-5905-971', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1378, 34, 'Hani Motor', 'Jl. Sukaraja II No.305 A', '0812-2448-52', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1379, 34, 'GADAI BPKB MOBIL BANDUNG EXPRESS', 'Jl. Sukabumi No.20', '0822-1993-199', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1380, 34, 'Eka Motor', 'Pasar Cikapundung, Jl. ABC', '(022) 426054', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1381, 34, 'CJR', 'Jl. Soekarno-Hatta No.623', '0812-1327-06', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1382, 34, 'PETA MOTOR Cover Seat dan Sofa', '', '0815-6144-61', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1383, 34, 'Bengkel Suzuki Cipadung', 'Cibiru, Jalan Raya Ujung Berung No.198', '0800 110080', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1384, 34, 'BB Custom Painting bandung cimahi water transfer printing', '', '0857-2218-171', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1385, 34, 'Merdeka Motor Toyota', 'Raya, Jl. Raya Kopo No.561-569', '0838-2128-310', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1386, 34, 'OTO Kredit Mobil', 'Jl. Lengkong Kecil No.48-50', '(022) 2052565', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1387, 34, 'Deler Honda Motor', 'Jl. Cijagra', '0813-9484-133', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1388, 34, 'Honda Bintang Motor - Bandung', 'Jl. Raya Banjaran No.677, RT.04/RW.01', '0858-8350-509', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1389, 34, 'Dealer KIA Bandung', 'Jl. Dr. Djunjunan No.180', '(022) 200401', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1390, 34, 'TVs Ahmad Yani', 'JL. Jenderal Ahmad Yani 474, Cicadas 40124 , Bandung , West Jawa', '(022) 720718', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1391, 34, 'Toko Pusat Aki Bandung', 'Jl. Cikutra Barat No.54', '0812-1490-06', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1392, 34, 'Fisa Car Seat Cover', 'Jl. Burangrang No.12', '0812-2282-351', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1393, 34, 'Duta Motor', 'Jl. Kartika Raya No.3', '(022) 201179', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1394, 34, 'Mokasindo Mobil Bekas Bandung', 'Jl. Sinom Jl. Senen Raya No.11', '0811-4453-12', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1395, 34, 'ARCAPADA MOTO STATION', 'Jl. A.H. Nasution Kelurahan No.272 RT 04/04', '0813-2219-283', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1396, 34, 'Raja Ban Sudirman', 'Jl. Jend. Sudirman No.316', '(022) 601318', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1397, 34, 'Riva Jaya Motor', 'Jl. Raya Majalaya - Rancaekek No.4', '0857-8040-384', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1398, 34, 'Pusat Mobil Honda Bandung', 'Jl. Raya Cimindi No.88', '0857-7766-652', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1399, 34, 'Jayatama Motor', 'Jl. Terusan Sersan Bajuri', '(022) 278782', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1400, 34, 'S2C Variasi motor', 'Jl. Raya Bojongsoang Raya No.87', '0813-2140-852', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1401, 34, 'TJ Motor', 'Jl. Peta No.128b, RT.01', '0822-2147-7878', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1402, 34, 'Cv.motorindo Center ( Benelli Bandung Center )', 'Jl. Moh. Toha No.50', '0813-2092-550', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1403, 34, 'Surya Motor', 'Jl. Ibu Inggit Garnasih No.102', '(022) 520277', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1404, 34, 'Shop & Drive Cibiru Bandung', 'Jl. Raya Tagog Cinunuk No.94', '(022) 780742', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1405, 34, 'Yamaha Bandung Berlian Merdeka Belimotoryuk.com', 'Jl. Merdeka No.51A', '0822-2199-985', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1406, 34, 'Kursi Bonceng', 'Jl. Arum Sari VIII No.10', '0899-5113-67', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1407, 34, 'JBA Cabang Bandung', 'Jl. Inhoftank No.144A', '1 500 36', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1408, 34, 'Saputra Motor', 'Jalan Cicabe No. 8, Jatihandap, Mandalajati', '(022) 727427', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1409, 34, 'Planet Ban Gatot Subroto Maleer Batununggal Bandung', 'Jl.Gatot Subroto RT 001 RW 09 Kelurahan Maleer Kecamatan Batununggal', '0857-6055-605', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1410, 34, 'Krisna Motor', 'Jl. Terusan Kopo-Soreang No.388', '0813-9480-686', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1411, 34, 'Mobil bekas bandung', 'Jl kopo, gg abah usup No.62 rt 04/08', '0877-2750-261', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1412, 34, 'HYUNDAI MOBIL SOEKARNO HATTA BANDUNG', 'Telp.022-7351 2999, Jl. Soekarno-Hatta No.625', '(022) 756399', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1413, 34, 'Panca Gemilang Motor', '', '(022) 602448', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1414, 34, 'PT.JBA Bandung', 'Jl. Inhoftank No.46', '0818-0687-368', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1415, 34, 'Mobil Seken Bandung', 'Jl. Sriwijaya No.29', '0812-2018-08', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1416, 34, 'Nissan Datsun Bandung', 'Jl. Veteran No.51-55', '(022) 421214', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1417, 34, 'Planet Ban PSM Cidurian Sukapura Bandung', 'Jl. PSM Cidurian RT 009 RW 04 Kelurahan Sukapura Kecamatan Kiara Condong Bandung Kel. Sakapura', '0815-8534-697', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1418, 34, 'Nissan Bandung', 'Jl. Soekarno-Hatta No.382', '0895-6282-0517', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1419, 34, 'Samsat Outlet BTC Pasteur', 'Jl. Dr. Djunjunan No.143-149', '0800 140181', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1420, 34, 'Shop and drive ciwastra (toko aki bandung)', 'Jl. Ciwastra No.133', '0877-7196-240', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1421, 34, 'Shop And Drive Ahmad Yani Bandung ( Toko Aki )', 'Jl. Jendral Ahmad Yani No.750d', '0857-9572-278', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1422, 34, 'Asri Mobilindo (Showroom Mobil Bekas Bandung, Toyota & Daihatsu Specialist)', 'Jl. BKR No.96 A', '0817-229-39', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1423, 34, 'Honda Bandung : All Unit Mobil Honda Ready Diskon Besar', 'Jl. Soekarno-Hatta No.760', '0822-1985-221', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1424, 34, 'DEALER MOBIL TOYOTA BANDUNG', 'Mustopha No.6 Suci Bandung, Jl. PH.H. Mustofa', '(022) 8783200', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1425, 34, 'Capricorn Motor', 'Jl. Terusan Kopo-Soreang No.562', '(022) 588019', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1426, 34, 'Honda Bintang Motor Rancamanyar', '', '0853-2055-559', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1427, 34, 'Mandiri Tunas FInance Bandung 3', 'Jl. Batununggal Indah I No.249', '(022) 8730688', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1428, 34, 'MR Motor', 'Jl. Asep Berlian No.7', '(022) 710021', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1429, 34, 'Mobil Rongsok Tua Mogok Rusak Kilo Nambru Bandung Bali', 'Kiaracondong & Marlboro', '0812-2222-10', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1430, 34, 'Badan Pendapatan Daerah Provinsi Jawa Barat (Bapenda Jabar)', 'Jl. Soekarno-Hatta No.528', '(022) 756619', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1431, 34, 'Promo Honda Bandung Deal - Info Harga & Kredit Mobil Honda', 'Jl. Soekarno-Hatta No.517', '0811-2332-28', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1432, 34, 'Amarta Motor Padalarang', 'Jl. Raya Padalarang No.541', '0811-2341-36', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1433, 34, 'BAHAGIA MOTOR', 'Jl. Raya Dayeuhkolot a No.449', '(022) 520300', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1434, 34, 'Honda Bintang Motor Cihampelas', 'Jl. Raya Cihampelas', '0822-1872-734', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1435, 34, 'Planet Ban Katapang Bandung', 'Jl. Terusan Kopo-Soreang, RT.003/RW.002', '0815-1039-959', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1436, 34, 'WIJAYA MOTOR', 'WIJAYA MOTOR, Jl. Jend. Sudirman No.257', '(022) 6031202 ext. 601419', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1437, 34, 'Gadai BPKB Motor', '', '0812-8888-56', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1438, 34, 'Bengkel Motor MRTjr', 'Jl. Rumah Sakit No.110', '0878-4635-541', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1439, 34, 'Komunitas motor', 'Jl. Tubagus Ismail Raya', '0896-0440-205', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1440, 34, 'SHOP AND DRIVE BANDUNG(ANTAR AKI)', 'Jl. Dr. Setiabudi No.188', '(022) 203254', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1441, 34, 'Nsc Finance Kopo Bandung', 'Jl. Raya Kopo No.64B', '(022) 603170', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1442, 34, 'SELIS CENTER - BANDUNG', 'Jl. Pungkur No.161', '(022) 2053279', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1443, 34, 'ASTRA MOTOR', 'ASTRA MOTOR, Jl. Jend. Sudirman No.566-568', '(022) 601639', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1444, 34, 'Dealer Mobil Cimahi', '', '0812-2191-612', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1445, 34, 'Tunas Toyota Gatot Subroto', 'Jl. Gatot Subroto No.109-111', '(022) 731299', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1446, 34, 'Toyota Merdeka Motor Soreang', 'Jalan Raya Soreang No. 443 KM. 15,7', '(022) 589661', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1447, 34, 'Agen Rosalia Express Bandung', 'Jl. Peta No.214', '0815-7637-88', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1448, 34, 'Dakota Cargo', 'Jl. Pameuntasan', '(022) 5442475', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1449, 34, 'hidrolik cuci mobil /motor servis paket hidrolik baru bandung', 'Perumahan pesona bukit bintang blok a2 nomer 46 Paseh', '0853-2016-582', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1450, 34, 'Elang Motor', 'Jalan Raya Gading Tutuka Blok FF No.7, Cingcin, Soreang', '0812-2180-646', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1451, 34, 'Himeji Express Bandung', 'Komp. Kopo Plaza', '0811-3515-15', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1452, 34, 'Merdeka Motor', '', '(022) 779498', '2020-10-20 14:37:47+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1453, 35, 'Fajar Motor Pandaan', 'Jalan A. Yani Pertokoan No', '(0343) 674446', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1454, 35, 'Yan Motor Pandaan', 'Jl. Raya Kasri, RT.01/RW.03', '0856-3390-10', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1455, 35, 'Bengkel Nur Motor Pandaan', 'Jl. Raya Kasri No.450', '0896-7835-762', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1456, 35, 'Nozomi Pandaan', '', '0812-3230-947', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1457, 35, 'Bengkel Utama Motor Pandaan', 'Jl. Raya Surabaya - Malang No.40', '(0343) 63163', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1458, 35, 'TOP1 MOTOR', 'Unnamed Road', '0813-3631-251', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1459, 35, 'Gandring Motor', 'Jl. Jambu Kutorejo No.4a', '0822-4364-910', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1460, 35, 'Surya Indah Motor', 'Ruko Griya Pandaan Indah, Jl. Dr. Sutomo No.3', '(0343) 63068', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1461, 35, 'Kasri Motor', 'Jl. Dr. Sutomo No.237', '(0343) 565081', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1462, 35, 'Klampok Motor', 'Jl. DR. Soetomo No.25', '0821-4047-092', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1463, 35, 'Barokah Motor II', 'Jl. Urip Sumoharjo No.54', '0812-5243-200', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1464, 35, 'Intan Jaya Motor 4', 'Jl. Urip Sumoharjo No.4', '(0343) 775895', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1465, 35, 'Bengkel Mobil "HAN MOTOR"', 'Jl. Kali Tengah Baru No.64', '(0343) 63582', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1466, 35, 'Slamet Jaya Motor', 'JL RA. Kartini, No. 47, Jogonalan', '(0343) 774415', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1467, 35, 'Asia Surya Jaya Raya Pandaan', 'Jl. A. Yani No.43,Petungasri, Pandaan, Pasuruan, Jawa Timur 67156 Kluncing', '(0343) 63145', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1468, 35, 'Lia Motor', 'Jl. Pahlawan Sunaryo No.41', '0858-5900-800', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1469, 35, 'Intan Jaya Motor 2', 'Jl. Jaksa Agung Suprapto, RT.1/RW.15', '0851-0175-895', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1470, 35, 'UD. Utami Motor', 'Jalan Pahlawan Sunaryo No. 1 Kutorejo', '0813-5876-689', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1471, 35, 'Toko Bengkel Motor Slamet Jaya', 'Jl. Sedap Malam No.6, RT.4/RW.08', '(0343) 63066', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1472, 35, 'CAHAYA Motor', 'Jl. Juanda No.15A', '0882-1186-878', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1473, 35, 'Jaya Abadi Motor', 'Jl. Randupitu-Gunung Gangsir No.89D', '(0343) 63746', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1474, 35, 'Bintang Jaya Motor', 'Jl. Raya Pandaan - Bangil, RT.01/RW.10', '0813-3004-883', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1475, 35, 'Full Motor', 'Jl. Pahlawan Sunaryo', '(0343) 63562', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1476, 35, 'ADI DJAYA MOTOR', 'Jln. Jaksa agung soeprapto no 55 Rt. 01 Rw 15 Rajeg', '0822-6512-777', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1477, 35, 'Dealer YAMAHA NIAGARA MOTOR PANDAAN', 'Dsn Sangarejo, Jl. Palagan Trip Jl. Raya By Pass Pandaan No.8', '(0343) 63898', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1478, 35, 'Warrior Warung Asesoris', 'Jl. Pabrik Es Kasri, RT.01/RW.05', '0813-3656-971', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1479, 35, 'Nicko Motor', 'Ruko Delta Permai, Jl. Raya Kasri No.15', '(0343) 63283', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1480, 35, 'SK Motor Pandaan (Bengkel Cat dan Servis Mobil)', '', '0811-3437-47', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1481, 35, 'Barokah Motor 3', 'Jl. Sedap Malam, RT.02/RW.09', '0858-5475-999', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1482, 35, 'Ilham Motor', 'Jl. Raya Pandaan - Bangil No.52', '0851-0129-007', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1483, 35, 'Hartono Motor Group', 'Jl. Sedap Malam No.77', '0888-5855-38', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1484, 35, 'Widori Motor', 'Jl. Patimura', '0857-3391-650', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1485, 35, 'DMS (Dikin Motor Speed)', 'Jl. Patimura No.16', '0857-3335-967', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1486, 35, 'Lestari Motor', 'Jl. A. Yani No.31', '(0343) 63152', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1487, 35, 'Dyna Motor / Sukses Motor', 'JL Ahmad Yani, No. 47, Petungasri, Pandaan', '(0343) 63261', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1488, 35, 'Bengkel Mahkota Motor', 'Jl. DR. Soetomo No.408', '0822-5737-709', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1489, 35, 'Ahass Tunggal Motor 00360', 'Jl. Raya Kasri No.44', '0857-5575-101', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1490, 35, 'Prima Motor', 'Jl. Ra. Kartini No.27', '(0343) 701161', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1491, 35, 'Akbar Motor', 'Terminal, Jl. A. Yani', '0813-7775-504', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1492, 35, 'TONY Motor', 'Jl. Sedap Malam, RT.1/RW.9', '0813-3457-768', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1493, 35, 'Honda WIN Pandaan', 'Jl. Ra. Kartini No.4', '0851-0083-336', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1494, 35, 'Tranggono Velg', 'Jl. Urip Sumoharjo No.100', '(0343) 63893', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1495, 35, 'Dillah Motor', 'Jl. Urip Sumoharjo, RT.1/RW.1', '0856-5555-010', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1496, 35, 'Bengkel SR 65 MOTOR', 'Jl. Jaksa Agung Suprapto No.43', '(0343) 63137', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1497, 35, 'Brewok Motor', 'Jl. Urip Sumoharjo, RT.04/RW.02', '0817-0305-118', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1498, 35, 'Putra Slamet Motor 2', 'Jl. Randupitu-Gunung Gangsir No.36', '(0343) 63313', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1499, 35, 'Mitro Motor + Service', 'Jalan Kedungrejo RT. 03 RW. 03', '0857-3346-244', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1500, 35, 'UD. Eka Jaya Motor', 'Jl. Pahlawan Sunaryo, RT.4/RW.4', '0896-3938-843', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1501, 35, 'Berkah Motor Kios', 'Pasar Pandaan, Jl. Urip Sumoharjo', '0838-3322-924', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1502, 35, 'Sinar Jaya Motor', 'Ruko Jatiroso, Jl. Raya Prigen No.12', '0822-3355-508', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1503, 35, 'Bengkel Motor BRT', 'Jalan Gelang-Pandaan', '0896-1728-880', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1504, 35, 'Honda Ahass 09942 CV WIN', 'Jl. Ra. Kartini No.04', '0851-0080-980', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1505, 35, 'Indo Jaya Motor', 'Jalan Raya Delta Permai Ruko A35 No 11', '0813-3628-499', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1506, 35, 'Indah Motor', 'Jalan Desa No.09', '0857-0718-890', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1507, 35, 'Sigit Motor', 'Jl. Sedap Malam No.85', '0895-6318-0157', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1508, 35, 'AB Motor', 'JL Pasar Baru', '(0373) 63566', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1509, 35, 'Bengkel Ava Motor', 'Jl. Juanda No.15', '0816-1592-442', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1510, 35, 'Mandiri Jaya Speed (Roby Bengkel Motor)', 'Unnamed Road', '0855-3658-209', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1511, 35, 'Bengkel Vespa Yono', 'Jl. Patimura, RT.08/RW.04', '0858-5036-509', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1512, 35, 'Ri2s speed', 'Unnamed Road', '0857-3131-771', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1513, 35, 'Ria Motor', '', '0857-5579-343', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1514, 35, 'Andri Jaya Motor', 'Jalan Indrokilo Rt.01 Rw.06, Ngemplak, Tanjungarum, Sukorejo', '(0343) 63813', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1515, 35, 'kentungpartcb', 'Jatiroso, RT:002/RW:013', '0858-5620-042', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1516, 35, 'Daffa Jaya Motor', '', '0898-3850-07', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1517, 35, 'Paula Motor', 'Jl. Raya Surabaya - Malang, RT.01/RW.16', '0858-5273-435', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1518, 35, 'BENGKEL MOBIL Aridjaja motor', 'Jl. Raya Pandaan - Bangil No.KM3', '0812-3515-959', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1519, 35, 'braappattitude', 'Jl. Magersari', '0838-3365-009', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1520, 35, 'Molly Motor', '', '0813-3421-583', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1521, 35, 'Santoso Motor', 'Jl. Kwangen', '0815-5480-865', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1522, 35, 'Oli Sakinah', 'Jl. Pahlawan Sunaryo No.15', '0857-9194-403', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1523, 35, 'bengkel juanda motor ( Pak Uche )', 'Jl. Juanda No.18-14', '0822-3404-344', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1524, 35, 'Bengkel Effendi/Klepon', '', '0857-4999-344', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1525, 35, 'Bengkel Motor "Pakde"', 'Unnamed Road', '0812-3429-034', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1526, 35, 'Bengkel Surya Citra Pratama', 'JL. Delta permai', '(0343) 63473', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1527, 35, 'BENGKEL CAKBOGEL&KANGTOHA', '', '0896-7864-095', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1528, 35, 'Cahaya Motor', 'No., Jl. Raya Pabean No.245', '0856-4659-847', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1529, 35, 'Pahlawan Motor Pandaan', '', '0812-3366-616', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1530, 35, 'Surya Mas Jaya. CV', '', '(0343) 63955', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1531, 35, 'Kawulo speed', '', '0857-4897-210', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1532, 35, 'Nitrogen pandaan', 'Jl. A. Yani No.1', '0813-3312-570', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1533, 35, 'Bengkel Hartono motor group', '', '0857-3352-669', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1534, 35, 'MPM MOTOR JAWI', 'Jl. Raya Candiwates', '0856-4960-513', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1535, 35, 'DAIHATSU PANDAAN', 'Jl. Raya Malang - Gempol No.9', '0822-3050-424', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1536, 35, 'Planet Ban Bangil Pandaan', 'Jl. Raya Pandaan - Bangil No.122 RT. 001/ 004', '0815-8708-67', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1537, 35, 'Habibi Motor', '', '0815-5991-615', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1538, 35, 'Bengkel Las Jais', 'Jl. Pabrik Es Kasri No.41', '0857-9111-941', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1539, 35, 'Shemir garage', '', '0822-4778-391', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1540, 35, 'SUJAT MOTO', '', '0812-3427-284', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1541, 35, 'jj garage 165 (repaint,custom,modify,repair motorcycle and etc.)', 'Jl. Pahlawan Sunaryo No.31', '0822-4564-937', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1542, 35, 'Jepang Motor', 'Jl. Dusun Jatitengah Kidul RT.02 RW.03 Dusun Jatitengah Kidul Desa', '0823-0101-927', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1543, 35, 'S1P BROMO', 'Jl. Pahlawan Sunaryo', '(0343) 64152', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1544, 35, 'SINAR JAYA MOTOR', 'Jl. Raya Malang - Gempol delta permai No.B-19', '0822-4471-740', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1545, 35, 'Diler Motor Honda pandaan ( ANDIK APANZA )', 'Jl Ra Kartini no.4 jogonalan', '0856-4520-878', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1546, 35, 'Planet Ban Dr Soetomo Pandaan Pasuruan', 'Jl Dr Soetomo 32A RT 1 RW 2 Kelurahan Sumber Gedang', '0857-6055-605', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1547, 35, 'Bima Motor Part shop', 'Jl. Melati', '0857-0400-005', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1548, 35, 'Edo Jaya Variasi Motor', 'Jl. Dr. Sutomo No.337-324', '(0343) 565013', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1549, 35, 'Tempat Parkir Motor Citra', 'Belakang Citra Swalayan Lingk. Jogonalan', '0822-3355-509', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1550, 35, 'Andri Motor', 'Jl. Raya Prigen No.19', '0822-4481-210', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1551, 35, 'Aulia', 'Pasar Pandaan Baru, Blok H / 19, JL Panglima Sudirman, No. 9, Pandaan', '0819-3807-654', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1552, 35, 'Cuci Motor Kinclong Bocah Pandaan', 'Jalan Raya Malang - Pasuruan, Karang Jati, Pandaan', '0822-2776-988', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1553, 35, 'AHASS BANGIL MOTOR 07009', 'Jl. Mangga No.5C', '(0343) 643025', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1554, 35, 'Chandra Motor', 'Jl. Ps. Baru', '(0343) 63482', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1555, 35, 'x motor '' body part mobil', '', '0812-1633-423', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1556, 35, 'Tunggal Ika Jaya', 'Jl. Raya Pandaan - Bangil No.27', '(0343) 74755', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1557, 35, 'Toko Accu Perkasa', 'Jl. Raya Lebaksari No.07/09', '0823-3105-880', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1558, 35, 'HDK Cuci Mobil & Motor', 'Jl. Rambutan No.36', '0813-3362-266', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1559, 35, 'Faisal Motor Bengkel & Variasi', 'Jl. Raya Candiwates', '0813-5958-602', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1560, 35, 'toyo veleg motor', 'Jalan Dusun Sidokatut, Sidokatut Lor, rt04/rw01', '0815-5463-315', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1561, 35, 'Toko Bu Sulis', 'Jl. Juanda, RT.01/RW.03', '0858-1501-051', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1562, 35, 'Jual beli mobil bekas berkwalitas', 'Perum asabri', '0813-3637-178', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1563, 35, 'Yupiter Electric Cabang Pandaan (Bengkel Gulung Dinamo/Rewinding Elektromotor)', 'Jl Raya Pandaan Bangil Km. 3,5 Dsn Tudan Ds Kemiri Sewu, Pandaan, Kemiri Sewu, Pandaan', '(0343) 85243', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1564, 35, 'Toko Jam Tangan Skmei Pandaan Pasuruan', 'Toko TJIPUT Ruko Belakang Fajar Motor/ Depan Sardo Swalayan', '0856-4697-716', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1565, 35, 'Rental Mobil Pandaan', 'Besongol Pandaan', '0811-3774-23', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1566, 35, 'Cuci Mobil dan Motor ABC', 'Jl. Raya Pandaan - Bangil No.KM.03 No.07', '0822-3322-773', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1567, 35, 'ERVINA BAJU GROSIR', 'Jl. Raya Bareng Kampung, RT.4/RW.2', '0823-3482-721', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1568, 35, 'Cak chur motor', '', '0812-3396-238', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1569, 35, 'Cuci motor', 'Dsn. Mejasem, Jl. Pabrik Es Kasri', '0853-3529-368', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1570, 35, 'Diler NOZOMI Pandaan - Pasuruan', '', '0858-1509-183', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1571, 35, 'MITRA SEJATI E-BIKE PANDAAN', 'Jl. Dr. Sutomo', '0822-3330-300', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1572, 35, 'Rangga Motor', 'Jl. Randupitu-Gunung Gangsir No.12', '0858-0899-066', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1573, 35, 'Duta Store', 'Jl. Pahlawan Sunaryo No.6', '0822-3793-124', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1574, 35, 'HURET CUCI MOTOR DAN TAMBAL BAN, BENSIN ECERAN', 'Jl. Sedap Malam No.11', '0857-8433-367', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1575, 35, 'Mega Jaya Ban Goodyear Central Service', 'Jalan Raya By Pass RT.01 / RW.14 Sangarejo', '(0343) 63642', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1576, 35, 'Surabaya Diesel', 'Komplek Ruko Gudang Garam Jalan Raya Malang - Surabaya No.6 KM. 39 Surabaya Pandaan', '0852-8598-898', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1577, 35, 'Green Nitrogen By Pass Pandaan SPBU 54 - 67136', 'SPBU Pertamina 54 - 67136, Jalan Raya Malang - Surabaya No. 8, Karang Jati, Pandaan', '0851-0079-245', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1578, 35, 'Sahabat Motor', 'Jl. Raya Surabaya - Malang No.96', '0812-5262-874', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1579, 35, 'Warkop Sekar Pondok Biru', 'desa', '0815-5010-64', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1580, 35, 'Edo Jaya Variasi Motor', 'Jl. Panglima Sudirman No.115', '(0343) 41709', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1581, 35, 'BFI Finance Pandaan', 'Jl. Raya Malang - Gempol No.8', '(0343) 63588', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1582, 35, 'JNE Kantor Bypass Pandaan', 'Jl. Raya Bypass Pandaan No.7', '0813-3686-325', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1583, 35, 'Toko spare part&Variasi', '', '0823-3419-800', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1584, 35, 'Kawasaki Pasuruan CV Seger Jaya Motor', 'Jl. Kyai H. Ahmad Dahlan No.3A', '0821-4359-371', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1585, 35, 'Grosir Elyka Jaket Collection Pandaan', 'Pateguhan', '0852-5776-556', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1586, 35, 'Pt Adiguna Sakti Motor', 'Jalan Raya Solo-Sragen', '(0343) 63008', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1587, 35, 'PT Bank BRI Syariah KCP Malang Pandaan', 'Jl. Dr. Sutomo No.98B', '(0343) 63010', '2020-10-20 15:28:14+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1588, 36, 'MPM Motor Pasuruan - AHASS 06443', 'Jl. Panglima Sudirman No.88', '(0343) 41083', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1589, 36, 'Hafiz Motor Alarm Motor Pasuruan', 'Jl. Imam Bonjol', '0888-0381-257', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1590, 36, 'New Rajawali Motor Pasuruan', 'Jalan Raya Soekarno Hatta No.192 Kebonsari Gadingrejo', '(0343) 42357', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1591, 36, 'Yamaha Sari Motor Pasuruan', 'Jl. Dokter Wahidin Sudiro Husodo No.129', '(0343) 41688', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1592, 36, 'NOZOMI Perkasa Motor Pasuruan', 'Jl. Soekarno Hatta No.87', '(0343) 41398', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1593, 36, 'Yamaha Sentral Motor Pasuruan', 'Jl. Soekarno Hatta No.136', '(0343) 42496', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1594, 36, 'Agung Sejahtera Motor', 'Jl. Raya Surabaya - Malang Apolo KM.36 No.1a', '(0343) 85331', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1595, 36, 'Pusat Layanan Motor Bekas', 'Jl. Dokter Wahidin Sudiro Husodo No.8', '0821-3957-772', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1596, 36, 'Furqon Motor Pasuruan', 'Jl. Karangwingko', '0856-3538-70', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1597, 36, 'Karunia Motor', 'Jl. Hasanudin No.23', '(0343) 42466', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1598, 36, 'Bagio Jaya Motor Pasuruan', 'Jalan KH. Hasyim Asyari Gg.Sekarsono4 No 20, Sekargadung', '0857-4566-680', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1599, 36, 'Kawasaki Pasuruan CV Seger Jaya Motor', 'Jl. Kyai H. Ahmad Dahlan No.3A', '0821-4359-371', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1600, 36, 'Jaya Raya Motor Pasuruan', 'Seberang pas alfamart, Jl. Raya Bromo No.4', '0812-3000-875', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1601, 36, 'Delivery MPM Pasuruan', 'Jl. Panglima Sudirman No.88', '0822-5701-606', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1602, 36, 'NEW RAYA/TIMUR RAYA motor', 'Jl. Soekarno Hatta No.153A', '0853-3453-262', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1603, 36, 'Bersinar motor pasuruan', 'Jl Majapahit setelah masjid sunan Ampel ada Alfa/ahass masuk', '0822-2897-461', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1604, 36, 'Edo Jaya Variasi Motor', 'Jl. Panglima Sudirman No.115', '(0343) 41709', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1605, 36, 'Hafiz Motor Alarm Motor Pasuruan', 'Jl. Imam Bonjol', '0856-3563-79', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1606, 36, 'New Rajawali Motor Pasuruan', 'Jalan Raya Soekarno Hatta No.192 Kebonsari Gadingrejo', '0819-3704-708', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1607, 36, 'Yamaha Sari Motor Pasuruan', 'Jl. Dokter Wahidin Sudiro Husodo No.129', '0822-3147-511', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1608, 36, 'NOZOMI Perkasa Motor Pasuruan', 'Jl. Patimura No.127', '0838-3362-223', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1609, 36, 'Yamaha Sentral Motor Pasuruan', 'Jl. Untung Suropati No.11', '(0343) 564385', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1610, 36, 'Agung Sejahtera Motor', 'Jl. Dokter Wahidin Sudiro Husodo No.79', '0853-3679-888', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1611, 36, 'Dealer Motor Second', 'Jl. Veteran No.17', '0813-3339-282', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1612, 36, 'Pusat Layanan Motor Bekas', 'Jl. Panglima Sudirman No.133', '(0343) 42856', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1613, 36, 'Rajawali Sakti Motor', 'Jl. Soekarno Hatta No.90', '(0343) 42821', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1614, 36, 'Cahaya Motor', 'Jl. Irian Jaya No.18', '(0343) 42137', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1615, 36, 'Bengkel Motor', 'Jl. Soekarno Hatta', '(0343) 41144', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1616, 36, 'Sinar Jaya Motor', 'JL. Raya Soekarno Hatta, No. 82, Trajeng, Gadingrejo', '(0343) 42179', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1617, 36, 'Berlian Motor', 'Jl. Hasanudin', '(0343) 42192', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1618, 36, 'Sumber Karya Motor', 'Jalan Jawa No.1', '0823-3017-005', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1619, 36, 'Bengkel Mobil Yono Motor', 'Jalan Bugul Kidul, Blandongan, Bugulkidul', '0851-0073-324', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1620, 36, 'PT. Asiasurya Jayaraya I', 'Jl. Soekarno Hatta No.25', '0856-0454-045', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1621, 36, 'MS Racing', 'Jl. Panglima Sudirman No.34A', '(0343) 42755', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1622, 36, 'Yamaha Metro Motor', 'Jl. Kh. Wachid Hasyim No.117 A', '(0343) 41218', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1623, 36, 'Cuci Mobil Karya Usaha Motor', 'Ruko Pd. Surya Kencana, Jalan Karya Bakti', '0851-0683-400', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1624, 36, 'Exsa Motor', 'Jl. Karangwingko No.25', '0813-3174-865', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1625, 36, 'Kawan Sejati Motor', 'Jl. Untung Suropati No.18', '0813-3191-646', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1626, 36, 'ERLANGGA MOTOR', 'Jl. Erlangga No.9 a', '0856-4643-532', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1627, 36, 'SIP Metro', 'Jl. Kyai H. Wachid Hasyim', '(0343) 42178', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1628, 36, 'MKK Motor ( Bengkel )', 'Jl. Raya Pleret No.14-18', '(0343) 43217', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1629, 36, 'Rajawali Sakti Motor', 'Jl. Soekatno Hatta Raya Gading', '(0343) 41625', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1630, 36, 'Anugrah Motor 2', 'Jl. Panglima Sudirman No.58', '(0343) 42603', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1631, 36, 'DONI MOTOR Custom', 'Jl. Gatot Subroto Rt. 1 Rw. 6, Karangketug, Gadingrejo', '0856-3566-62', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1632, 36, 'mokas TIARA MOTOR', 'Jl. Gatot Subroto', '0877-3356-122', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1633, 36, 'Bengkel Resmi Viar Motorcycle', 'Jl. Kyai H. Wachid Hasyim No.171', '(0343) 42144', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1634, 36, 'Bina Usaha Servis Sepeda Motor Edi', 'Jl. Panglima Sudirman No.105', '0857-3623-082', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1635, 36, 'Honda Anugerah Motor', 'Jl. Panglima Sudirman No.58', '0857-0603-131', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1636, 36, 'Mitra Motor Center Motor (Pakai Mesin Press)', 'Jl. Hasanudin No.24', '0813-5798-515', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1637, 36, 'Iwan Motor (Servis Kampas Kopling)', 'Jl. HOS Cokroaminoto No.02, Blandongan, Bugulkidul', '0822-2933-062', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1638, 36, 'AHASS 16410 Anugerah Motor', 'Jl. Patimura No.40', '(0343) 561652', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1639, 36, 'Kawan Setia Motor', 'Jl. Raya Pleret No.3a', '0811-3301-50', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1640, 36, 'Hero Sakti Motor Gemilang. PT', 'JL. Soekarno Hatta, Trajeng, Gadingrejo, Komplek Pasar Besar Blok A/31-32', '(0343) 41563', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1641, 36, 'Suzuki Top Motor', 'Jl. Jend. A. Yani No.218', '(0343) 42858', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1642, 36, 'Darma Motor', 'JL. Panglima Sudirman, No. 30, Purutrejo, Purworejo', '(0343) 41088', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1643, 36, 'Toko Onderdil Mobil', 'Jl. Hasanudin No.6A', '(0343) 42636', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1644, 36, 'BRAMA MOTOR', 'Jl. Kyai H. Ahmad Dahlan', '(0343) 42052', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1645, 36, 'Hamman Motor Custom', 'Jl. Irian Jaya No.1', '0818-313-83', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1646, 36, 'Sumber Oli Motor', 'Jl. A. Yani No.74', '0851-0079-680', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1647, 36, 'Batman Motor', 'Jl. Desa Sambirejo RT 3 RW 3, Sambirejo, Rejoso', '0819-3718-944', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1648, 36, 'SUZUKI SMG PASURUAN', '', '0852-3062-333', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1649, 36, 'IK Motor', 'Jl. Hasanudin No.16', '0877-0331-101', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1650, 36, 'Anang Jaya Motor', 'Jl. Kyai H. Ahmad Dahlan No.101', '(0343) 776419', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1651, 36, 'Brama Motorsport', 'Jl. Raya Pleret No.5-7', '(0343) 42654', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1652, 36, 'Planet Ban Hayam Wuruk Kota Pasuruan', 'Jl. Hayam Wuruk No.1a', '0815-9406-27', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1653, 36, 'Barokah Motor', 'Jl Lingkung Lawas RT 01 RW 03 KM 6, Kedung Bako, Rejoso', '0859-5456-005', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1654, 36, 'suzuki pasuruan', 'Jl. Gajah Mada No.52', '0813-3080-575', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1655, 36, 'Bengkel Mobil Wawan Motor', '', '0813-3471-399', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1656, 36, 'Fajar Motor (helm)', 'Jl. Kyai H. Ahmad Dahlan No.2', '(0343) 561178', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1657, 36, 'Lingkung Motor', 'Jl. Ir. H. Juanda KM 6, Sambirejo, Rejoso', '(0343) 564279', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1658, 36, 'Ahass 8000 Kenari Motor', 'Perum Pondok Asri Blok A3, Jalan Raya Warungdowo, Pohjentrek', '(0343) 41004', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1659, 36, 'Astra Isuzu Pasuruan', '', '0813-5795-100', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1660, 36, 'Bengkel Mobil Aris Motor', 'Jl. Raya Sidogiri Rt. 03 Rw. 06, Warungdowo, Pohjentrek', '0812-3433-274', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1661, 36, 'Shop And Drive Pasuruan', 'Jl. Panglima Sudirman No.30', '1 500 01', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1662, 36, 'TATA Motors Pasuruan', 'Jalan Veteran Blok M-8 No. 9 Bugul Kidul Bugulkidul', '0822-3317-336', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1663, 36, 'Planet Ban Dr Wahidin Sudiro Husodo Purutrejo Pasuruan', '1, Jl. Dokter Wahidin Sudiro Husodo No.176, RW.004', '0857-6055-605', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1664, 36, 'AHASS 16216 RANGGEH JAYA MOTOR', 'jl. Raya No.90', '0858-5409-898', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1665, 36, 'MPM Motor Ranggeh', 'Komplek Pertokoan Taman Kurnia Sari No, Jl. Raya Ranggeh No.2', '0812-3293-898', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1666, 36, 'Dealer Mobil Honda Pasuruan ( Sales, & Promo Honda Mobil Termurah )', 'Jl. Dokter Wahidin Sudiro Husodo No.176', '0821-6505-262', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1667, 36, 'Tya Motor', 'Jl. Raya Warungdowo Timur Rt.01/Rw.01, Gondangrejo, Gondang Wetan', '0851-0094-11', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1668, 36, 'Ertiga Pasuruan', 'Jl. Dokter Wahidin Sudiro Husodo No.33', '0822-2535-000', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1669, 36, 'UD. Salam Motor', 'Jl. Raya Warungdowo Timur No.5', '0851-0363-635', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1670, 36, 'Dirtbike Minimoto MiniJeep dan ATV', 'Jl. Raya Kejayan No.50', '0813-3617-922', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1671, 36, 'Motor Sport Pratama (msp racing)', 'Jl. Raya Bromo No.11-12', '0851-5761-52', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1672, 36, 'MKK Motor ( Variasi )', 'Alfamart, Jl. Raya Pleret No.24-25', '0813-3671-430', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1673, 36, 'Honda Lestari Probolinggo (Cabang Outlet Pasuruan)', 'Jl. Dokter Wahidin Sudiro Husodo No.176, RT.1/RW.4', '0821-9359-935', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1674, 36, 'Bengkel Mobil Armina Motor', 'jl.raya ngempit rt04 rw01 ngempit kraton pasuruan jawa timur kode', '0856-4650-123', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1675, 36, 'CAHAYA GUMILANG MOTOR', 'Jalan Raya Tenggilis Rejo RT.02/RW.03, Tenggilis Rejo, Gondang Wetan', '0813-3670-490', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1676, 36, 'Bengkel Makin Motor', 'Jl. Raya Pantura, RT.5/RW.2', '0896-0762-671', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1677, 36, 'Cuci Motor Perfect Clean', 'Jl. Sultan Agung No.33', '0838-3516-766', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1678, 36, 'Bengkel Motor Abbas Gejugjati Lekok Pasuruan Jatim', 'krajan selatan rt/rw02/03/02', '0812-4990-958', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1679, 36, 'OKKA MOTOR', '', '(0343) 41796', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1680, 36, 'AUTO2000 PASURUAN', 'Jl. Jend. A. Yani No.226', '(0343) 41200', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1681, 36, 'AHASS BANGIL MOTOR 07009', 'Jl. Mangga No.5C', '(0343) 643025', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1682, 36, 'Jaya Motor', 'RT/RW 04/02, Jl. Kolursari', '0823-3426-717', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1683, 36, 'Planet Ban Warungdowo Pasuruan', 'Jl. Warung dowo RT. 002/ 008 No. 4 Kel. Warungdowo Kec. Pdhjentrek Kab Pasuruan', '0815-8708-57', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1684, 36, 'Yamaha Sentral Motor Bangil', 'Jl. Patimura No.299', '(0343) 74273', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1685, 36, 'Ardy Satya Modifikasi Motor', 'Jl. Kambingan Barat RT 2 RW 1, Kambingan Rejo, Grati', '0812-3546-127', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1686, 36, 'Mili Klinik Motor ABD', 'Ruko Royal Regency Jl Bader No 2 RT. 003 RW 002', '(0343) 643040', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1687, 36, 'BENGKEL ROMBO MOTOR (BRM)', 'Jl. Rambutan No.24', '0851-0454-523', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1688, 36, 'JS motor', 'Jalan Bader RT.05 RW.02 Kalianyar Bangil', '0857-3003-019', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1689, 36, 'Rizqy Motor', 'Jl. Raya Bromo No.2', '0812-3000-214', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1690, 36, 'BENGKEL ZNG MOTOR', 'utara masjid', '0822-1111-101', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1691, 36, 'New Galaxy Motor Service & Spare Parts', 'Jl. Mangga No.604', '0857-3366-984', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1692, 36, 'Sumber Baru Motor', 'Jl. Untung Surapati No.486', '0877-5452-839', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1693, 36, 'JOVY Motor', 'Jl. Jambu No.540', '0821-3961-846', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1694, 36, 'Tunggal Ika Jaya', 'Jl. Raya Pandaan - Bangil No.27', '(0343) 74755', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1695, 36, 'LA CUSTOM pasuruan', 'Gang kambeng,Desa', '0858-5056-538', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1696, 36, 'Aneka Motor', 'Jalan Pasrepan RT 5 RW 7, Pasrepan', '0813-3133-145', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1697, 36, 'Max LEO garage - press velg racing & variasi motor', 'Jl. Mangga No.03, Kel', '0822-3455-730', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1698, 36, 'Dilla Motor', 'Jl. Kolursari, RT.01/RW.02', '0819-3683-946', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1699, 36, 'Gandring Motor', 'Jl. Jambu Kutorejo No.4a', '0822-4364-910', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1700, 36, 'Brian Motor', 'Jl. Raya Beji No.27', '(0343) 65747', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1701, 36, 'Bengkel Gangsar Motor', 'Jl. Bandeng No.486', '0821-4209-777', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1702, 36, 'Barokah Jaya Motor', 'Jl. Randupitu- Gunung Gangsir Rt 4 RW 6 Gunung Gangsir Beji', '0851-0076-479', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1703, 36, 'AHASS RAMA MOTOR', 'Jl. Raya Sambisirah No.77', '0856-4937-602', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1704, 36, 'Bengkel Edi Motor', 'Jl. Raya Pantura No.14', '0815-5598-831', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1705, 36, 'Rejeki Motor', 'Jalan Kemploko No.8 Kemloko Beji Pasuruan', '(0343) 65821', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1706, 36, 'Rangga Motor', 'Jl. Randupitu-Gunung Gangsir No.12', '0858-0899-066', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1707, 36, 'Annas Motor', '', '0817-0392-179', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1708, 36, 'Barokah Motor II', 'Jl. Urip Sumoharjo No.54', '0812-5243-200', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1709, 36, 'Soponyono Motor', 'Jalan Raya Wonorejo RT1 RW2, Dusun Surabanyak, Desa Kluwut, Wonorejo', '0853-0343-019', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1710, 36, 'Slamet Jaya Motor', 'JL RA. Kartini, No. 47, Jogonalan', '(0343) 774415', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1711, 36, 'Jaya Abadi Motor', 'Jl. Randupitu-Gunung Gangsir No.89D', '(0343) 63746', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1712, 36, 'Fajar Motor Pandaan', 'Jalan A. Yani Pertokoan No', '(0343) 674446', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1713, 36, 'Ahass Barokah 2 Motor', '', '0822-5715-092', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1714, 36, 'CAHAYA Motor', 'Jl. Juanda No.15A', '0882-1186-878', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1715, 36, 'Sukorejo Motor', 'Jl. Raya Sukorejo No.21', '0823-3778-858', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1716, 36, 'Bengkel Nur Motor Pandaan', 'Jl. Raya Kasri No.450', '0896-7835-762', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1717, 36, 'Bintang Jaya Motor', 'Jl. Raya Pandaan - Bangil, RT.01/RW.10', '0813-3004-883', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1718, 36, 'UD Lumayan Jaya Motor', 'Jl. Mangga Blok H No.1, Kelurahan Kidul Dalem, Kecamatan Bangil', '0857-3061-792', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1719, 36, 'Habibi Motor', 'Jl. Randupitu-Gunung Gangsir', '0853-7676-760', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1720, 36, 'Hasbi Motor', 'Jl. Raya Malang - Pasuruan No.59', '0812-3322-229', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1721, 36, 'Bengkel Utama Motor Pandaan', 'Jl. Raya Surabaya - Malang No.40', '(0343) 63163', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1722, 36, 'Dempo Motor', 'Jalan Sukrejo Rt/Rw 1/1 Sukorejo', '0812-3342-846', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1723, 36, 'Kasri Motor', 'Jl. Dr. Sutomo No.237', '(0343) 565081', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1724, 36, 'Bengkel Mobil "HAN MOTOR"', 'Jl. Kali Tengah Baru No.64', '(0343) 63582', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1725, 36, 'JAFI motor', 'Jl. Raya Surabaya - Malang No.3', '0813-3434-287', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1726, 36, 'Rosalia Express (Pasuruan)', 'Jl. Veteran No.16', '(0343) 41614', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1727, 36, 'Motor Sentul Service', 'Jl. Apel No.9', '0838-3323-276', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1728, 36, 'Bengkel Pasang Ruji / Jari Jari Velg sepeda motor', 'Jalan Krajan RT02 RW02 Pajaran', '0858-6671-610', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1729, 36, 'Surya Indah Motor', 'Ruko Griya Pandaan Indah, Jl. Dr. Sutomo No.3', '(0343) 63068', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1730, 36, 'Mpm Motor Sukorejo', 'Jl. Raya Sukorejo No.105', '(0343) 61203', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1731, 36, 'Toko Bengkel Motor Slamet Jaya', 'Jl. Sedap Malam No.6, RT.4/RW.08', '(0343) 63066', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1732, 36, 'UD. Utami Motor', 'Jalan Pahlawan Sunaryo No. 1 Kutorejo', '0813-5876-689', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1733, 36, 'Klampok Motor', 'Jl. DR. Soetomo No.25', '0821-4047-092', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1734, 36, 'Nicko Motor', 'Ruko Delta Permai, Jl. Raya Kasri No.15', '(0343) 63283', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1735, 36, 'Planet Ban Bangil Pasuruan', 'Jl. R.A.Kartini No.1A, RT.001', '0815-8708-61', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1736, 36, 'Janti Motor', 'Jalan Dusun Janti RT.02 RW.08 Dusun Janti, Desa Pakukerto, Sukorejo', '0815-5199-87', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1737, 36, 'TOP1 MOTOR', 'Unnamed Road', '0813-3631-251', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1738, 36, 'Tri Jaya Motor', 'Jl. Raya Pasuruan No.162', '(0343) 61100', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1739, 36, 'Widjaya Motor', 'Jl. Raya Malang - Gempol No.04', '0895-1575-575', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1740, 36, 'R.Motor', 'Jl. Raya Surabaya - Malang No.81', '0813-5388-576', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1741, 36, 'LIAN MOTOR 1', 'Jl glatik timur Jl. Ps. Sukorejo', '0815-5696-970', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1742, 36, 'Bengkel Resmi Yamaha Tunggal Ika Jaya', 'JL. Dalem, No. 01, Segok', '0815-5433-325', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1743, 36, 'Yamaha Sari Motor Purwosari', 'Jalan Kedawung II 98, Kertosari, Purwosari', '(0343) 61237', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1744, 36, 'HINO PARTS SHOP SUMBER BARU MOTOR', 'Jl. Untung Suro No.486', '(0343) 7104', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1745, 36, 'Surya Barokah Motor', '', '0812-5272-049', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1746, 36, 'AHASS 01177 AHASS BAROKAH MOTOR', '', '0812-1505-053', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1747, 36, 'Lumayan Jaya Motor', 'JL.MANGGA BLOK H NO.1 PASAR BARU', '0819-3696-869', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1748, 36, 'Bengkel cahaya motor', 'Jl.Legupit Rt.03 Rw.015 dsn.Legupit Ds.Karangrejo kec.gempol kab.pasuruan Gang.depan SDN.karangrejo 01 masuk ke Selatan 100meter', '0822-4440-034', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1749, 36, 'HINO PARTS SHOP JAPANAN MOTOR', 'Jl. Raya No.64', '(0343) 85214', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1750, 36, 'Bengkel SR 65 MOTOR', 'Jl. Jaksa Agung Suprapto No.43', '(0343) 63137', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1751, 36, 'Wahyu Jaya Motor', 'Jalan Raya Japanan, RT.07/RW.22', '0851-0082-972', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1752, 36, 'toyo veleg motor', 'Jalan Dusun Sidokatut, Sidokatut Lor, rt04/rw01', '0815-5463-315', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1753, 36, 'Toko Spare Part 57 Motor', 'Jl. St. Sengon Agung No.RT01', '0823-5920-093', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1754, 36, 'Gempol Jaya Motor', 'Perumahan Gempol Citra Asri Blok R.12, Gg. Tj.', '0813-3357-357', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1755, 36, 'UD. Nova Jaya Motor', 'Jalan Arjosari Dusun Penanggungan, RT.05/RW.23', '0812-5279-493', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1756, 36, 'Sofyan Motor', 'Jl. Raya Pasuruan No.80', '(0343) 664819', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1757, 36, 'Bengkel Motor BRT', 'Jalan Gelang-Pandaan', '0896-1728-880', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1758, 36, 'BFI Finance Pasuruan', 'Jl. Panglima Sudirman No.40A', '(0343) 41600', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1759, 36, 'Dealer YAMAHA NIAGARA MOTOR PANDAAN', 'Dsn Sangarejo, Jl. Palagan Trip Jl. Raya By Pass Pandaan No.8', '(0343) 63898', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1760, 36, 'Dealer Yamaha Putra Jaya Motor', 'Jl. Raya Malang - Gempol No.120', '0821-4371-346', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1761, 36, 'PT. Asiasurya Jayaraya IV', 'Jl. Raya Purwosari No.36', '(0343) 61229', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1762, 36, 'Hisam Jaya Motor', 'Jalan Raya Surabaya Malang No. 6, Ngetal, Gempol', '0857-4963-713', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1763, 36, 'bengkel RM (Riono Motor) Teteng Selatan', '', '0857-0665-305', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1764, 36, 'Agus Servis Jok Motor/Mobil', 'Jl. Raya Pasuruan No.RW6, RT.6', '0856-0442-989', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1765, 36, 'Ahass Hikmah Motor', 'Jl. Raya Trawas No.6', '0817-0340-060', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1766, 36, 'Honda Satrya Delta', 'Jl. Gempol - Mojokerto', '(0343) 84202', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1767, 36, 'Indah Logistic', 'Jl. Veteran No.6', '0811-1212-71', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1768, 36, 'Sahabat Motor', 'Jl. Raya Surabaya - Malang No.96', '0812-5262-874', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1769, 36, 'ESL Express', 'Jl. Veteran No.10', '0852-3020-825', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1770, 36, 'BAF', 'Jl. Panglima Sudirman No.92B', '(0343) 43144', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1771, 36, 'Friz Custom', 'Jl. Makam, RT.02/RW.01', '0857-8446-320', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1772, 36, 'Yamaha Lestari Motor', 'Jl. A. Yani', '(0343) 63152', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1773, 36, 'Beji motor elektronik', 'Jl. Raya Gondanglegi No.34', '(0343) 65657', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1774, 36, 'PT. BFI Finance Indonesia, Tbk', 'Jl. Kyai H. Ahmad Dahlan', '(0343) 750320', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1775, 36, 'Bengkel Motor SRS', 'Jl. Raya Kejapanan, RT.04/RW.7', '0877-0246-440', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1776, 36, 'SCUTO PASURUAN', 'Jl. K.H. Wachid Hasyim 55 I-J', '0812-9299-496', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1777, 36, 'ESL Express - Kraton', 'JL Soekarno - Hatta No. 40 Kraton, Pasuruan, East Java', '0817-5066-11', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1778, 36, 'Intan Motor', 'Jl. Raya Nongkojajar No.77A', '0853-3025-115', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1779, 36, 'Penitipan Sepeda Motor Anda', 'Jl. Pegadaian No.87A', '0822-2910-179', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1780, 36, 'Artha jaya Motor', 'Jalan Raya Wonorejo No.37 RT.2 RW.3 Dusun Sudan, Desa Wonosari, Kecamatan Wonorejo', '0812-3445-684', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1781, 36, 'PT. Dakota Buana Semesta', 'Jl. Soekarno Hatta No.2', '(0343) 42218', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1782, 36, 'Toko Bintang Kasih', 'Jl. Slagah No.12', '(0343) 42141', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1783, 36, 'PT. HD MOTOR 99', 'Jl. Raya Gondanglegi No.6-7', '(0343) 65853', '2020-10-20 15:34:56+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1784, 37, 'Jual Beli Motor Bekas Jaktim', 'Jl. Dr. KRT Radjiman Widyodiningrat No.58, RT.13/RW.8', '0822-2563-293', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1785, 37, 'Honda Bintang Motor - Jakarta', 'Jl. Buaran Raya No.15, RT.6/RW.13', '0812-8194-539', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1786, 37, 'Suzuki Matraman Sun Motor Jakarta', 'Jl. Matraman Raya No.140, RT.1/RW.4', '(021) 858205', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1787, 37, 'Yamaha Flagship Shop Jakarta', 'Jl. Letjend Suprapto Jl. Cemp. Putih No.402, RT.9/RW.7', '(021) 421588', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1788, 37, 'Joddy Motor', 'Jl. Raya Jatiwaringin No.1A, RT.2/RW.13', '0878-8693-845', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1789, 37, 'Probike Motor', 'Jl. Panjang Arteri Klp. Dua Raya No.88C, RT.7/RW.4', '(021) 530324', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1790, 37, 'Honda Daya Motor Jakarta (Official)', 'Jalan Sultan Agung No. 65A RT.1/RW.10 Manggarai Setiabudi RT.1, RT.1/RW.10', '0878-7466-662', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1791, 37, 'Bmj Motor', 'Jl. Letjend Suprapto No.7A, RT.2/RW.2', '(021) 421677', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1792, 37, 'Astra Motor Center Jakarta', 'Jl. Dewi Sartika No.255, RT.8/RW.5', '(021) 801555', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1793, 37, 'Wahana Motor', 'Jl. Pramuka Raya,Matraman,Jaktim., RT.3/RW.2', '(021) 420424', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1794, 37, 'Clara Motor', 'Jl. Dewi Sartika No. 297 Cawang Kramatjati RT.4, RT.4/RW.5', '(021) 800003', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1795, 37, 'Jakarta Motorbikes', 'Jl. RS. Fatmawati Raya No.11, RT.1/RW.5', '(021) 7590310', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1796, 37, 'Taruna Motor (YSS)', 'Jl. Kramat Raya No.108, RT.2/RW.9', '(021) 390677', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1797, 37, 'ASHOKA MOTOR JAKARTA', 'jln, Cempaka Jaya No.1, RT.18/RW.3', '0838-7024-978', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1798, 37, 'Dealer motor honda jakarta', 'Jl. Dewi Sartika No.255, RT.8/RW.5', '0812-8484-696', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1799, 37, 'Megah Matahari', 'Jl. Tanjung Duren Raya No.99, RT.6/RW.5', '(021) 560035', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1800, 37, 'Yamaha Cahaya Motor', 'Jl. H. Naman No.19, RT.3/RW.2', '(021) 864623', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1801, 37, 'Lancar Motor', 'Jl. Letjend Suprapto No.82, RT.8/RW.3', '(021) 420795', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1802, 37, 'Yamaha Persada Motor', 'Jl. Raya Bekasi No.Km 18 No 49, RT.9/RW.11', '(021) 4683041', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1803, 37, 'Permana Motor', 'Jl. RS. Fatmawati Raya No.33, RT.2/RW.2', '(021) 750390', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1804, 37, 'Deal Garage Cab. Pondok Bambu (Motor Bekas Jakarta)', 'Jl. Pahlawan Revolusi No.2, RT.2/RW.5', '0821-1417-861', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1805, 37, 'Twins Painting (Bengkel Cat Motor Jakarta)', 'Jl. Rengas 1 No.39, RT.5/RW.2', '0812-3876-138', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1806, 37, 'Kredit Motor Honda Jakarta', 'Jl. Buaran Raya No.15, RT.6/RW.13', '0896-5721-122', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1807, 37, 'Jakarta Motorsport', 'Jl. Cideng Timur No.5, RT.7/RW.4', '0878-8480-499', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1808, 37, 'Garin Motor Jakarta TIMUR', 'Jl. Dr. Sumarno No.60, RT.011/RW.4', '0813-8411-320', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1809, 37, 'Yamaha Mustika Jatinegara', 'Jl. Jatinegara Tim. No.52, RT.13/RW.2', '0856-9779-199', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1810, 37, 'Bengkel TOP 1 Motor Suzuki Jakarta Barat', 'Jl. Panjang Arteri Klp. Dua Raya No.18, RT.6/RW.2', '(021) 532494', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1811, 37, 'Dealer Motor Honda Pelita Jaya Jakarta Pusat', 'Jl. Senen Raya No.48, RT.9/RW.1', '(021) 344695', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1812, 37, 'Honda JAKARTA MMP GROUPMITRA/ Dealer WING Motor HONDA: PREMIUM [FORZA CBR CRF PCX C125]', 'Bizpark 2, Jl. Raya Penggilingan No.2, RT.1/RW.7', '(021) 2906200', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1813, 37, 'Jakarta Motor', 'Jl. Raden Inten II No.14, RT.1/RW.5', '0813-1405-180', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1814, 37, 'Asep motor jl kelapa dua gang langgar rt 6 rw 5 no 90 kebon jeruk jakarta barat', 'Gg. Langgar No.90, RT.6/RW.5', '0899-8844-73', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1815, 37, 'PRO-MOTOR JAKARTA', '2, Jl. Raya Pulo Gebang No.58a, RT.2/RW.6', '0812-9490-290', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1816, 37, 'Prima Inti Motor. PT - Jakarta', 'JL. Kyai Caringin, No. 18A, Central Jakarta, DKI Jakarta, RT.7/RW.5', '(021) 6386838', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1817, 37, 'Dealer Motor suzuki Jakarta', 'Jl. Halim Perdana Kusuma, RT.1/RW.8', '0812-8822-625', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1818, 37, 'Sahabat Motor Jakarta Barat', 'Jl. Meruya Ilir Raya No.1A, RT.4/RW.1', '0822-6510-236', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1819, 37, 'marketing motor yamaha jakarta', 'Jl. Jatinegara Timur No.52 rt.14/02', '0813-9563-838', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1820, 37, 'Dealer Motor Suzuki Jakarta Pst', 'Petojo Selatan, Jl. Balikpapan No.30A-B, RT.1/RW.2', '(021) 6386413', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1821, 37, 'Bengkel Ahass Honda Jakarta Pusat', 'Jl. Letjen Suprapto No.16 AB, Cempaka Putih Barat, Cempaka Putih, RT.6/RW.2', '(021) 421082', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1822, 37, 'Jaja Mandiri Motor: Bengkel Mobil Terbaik Jakarta', 'Jl. Raden Inten II No.9, RT.4/RW.7', '0813-1052-235', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1823, 37, 'Jakarta Ninja Army Motor Sport', 'Jalan Gili Sampeng No. 91, RT.1/RW.4, Kebon Jeruk, RT.9/RW.3', '0812-8387-332', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1824, 37, 'Panca Motor Toko', 'Jl. Kebun Jeruk I No. 2 Maphar Taman Sari Jakarta Barat DKI Jakarta, RT.8/RW.6', '(021) 629748', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1825, 37, 'Jual Beli Mobil Motor Bekas Terbaik di Jakarta', 'Jl. Ps. Senen Dalam VI No.7, RT.13/RW.4', '0812-8112-41', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1826, 37, 'Global Jaya Motor Hasyim Ashari, no 20 Jakarta Pusat', 'Jl. KH. Hasyim Ashari No.30-6, RT.1/RW.7', '0851-0671-363', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1827, 37, 'Bengkel Mobil Repair - Aji Motor Jakarta Barat', 'Depan SMKN 13, Komplek sandang jln, Rawa Belong 2e No.64, RT.5/RW.10', '0812-1433-335', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1828, 37, 'Dealer Yamaha Jakarta - Dealer Resmi Yamaha Menjual Cash / Kredit Motor Yamaha Murah', 'CDC, Jl. RS. Fatmawati Raya No.54, RT.10/RW.3', '0857-8008-982', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1829, 37, 'Dealer Yamaha Jakarta Mega Utama - Kredit, Cash Yamaha DP Ringan', 'Jl. KH.Moh.Mansyur No.2, RT.2/RW.2', '0813-8877-669', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1830, 37, 'CAKRA MOTOR 11 OLI & AKI JAKARTA KOTA', 'Taman, Jl. Ratu Kemuning No.26, RT.9/RW.13', '0878-7770-889', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1831, 37, 'Bengkel Ahass Honda', 'Jl. Sultan Iskandar Muda No.36C, RT.10', '(021) 724882', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1832, 37, 'Ahass 7831 Sama Jaya Motor', 'Jl. Prof. DR. Satrio Blok Tiong No.4, RT.2/RW.6', '(021) 520110', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1833, 37, 'Asih Motor Jakarta', 'Jl. Taruna V No.28, RT.19/RW.3', '0878-8889-533', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1834, 37, 'HINO PARTS SHOP HERO MOTOR cab. Jakarta', 'Jl. Duren Sawit Baru Blok C5 No.12', '(021) 626664', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1835, 37, 'Dealer Motor Kawasaki', 'Jl. Abdul Muis No.24, RT.2/RW.8', '(021) 352510', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1836, 37, 'Dealer', 'Jalan Mampang Prapatan Raya No. 25, RT.04 / RW.06, Mampang Prapatan, RT.4/RW.6', '(021) 798347', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1837, 37, 'Apartemen Senayan', 'Jl. Patal Senayan No.21, RT.3/RW.7', '(021) 5799245', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1838, 37, 'DuniaMotor.com', 'Jakarta Garden City Rukan The Avenue No.8055, RT.16/RW.1', '(021) 2246607', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1839, 37, 'Dealer Motor Honda', 'Jl. Pemuda No. 720 Jati Pulo Gadung Jakarta Timur DKI Jakarta, RT.2/RW.7', '(021) 489026', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1840, 37, 'Dealer Suzuki PT Sinar Roda Kencana Mas', 'JI, Jl. Hayam Wuruk No.38 A, RT.2/RW.1', '(021) 384900', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1841, 37, 'Berkat Motor Toko', 'Pasar Palmerah Los Bks, JL. Palmerah Blok B-3/21, RT.1/RW.1', '(021) 533314', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1842, 37, 'Motospa Jakarta', 'Jl. Prapanca Raya No.12E, RT.9/RW.8', '0811-806-16', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1843, 37, 'Mitsubishi Sun Motor Jakarta', 'Jl. Letjend Suprapto M79, RT.1/RW.3', '0878-1605-722', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1844, 37, 'Nusantara Sakti Jakarta Barat', 'NUSANTARA SAKTI, Jl. Brigjen Katamso No.5, RT.8/RW.2', '(021) 568666', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1845, 37, 'CV. Sartika Motor', 'Jl. Otista 3 No.14, RT.9/RW.9', '0857-1164-586', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1846, 37, 'Agung Motor', 'Jl. Palmerah Barat No. 53 RT 003 RW 07 Kemanggisan Palmerah Jakarta Barat DKI Jakarta, RT.3/RW.7', '(021) 532891', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1847, 37, 'att electric motor jakarta', 'Jl. Komp. Green Ville, RT.6/RW.14', '0813-1053-228', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1848, 37, 'Bintang Motor', 'Jl. Daan Mogot No.Raya, RT.9/RW.3', '(021) 619868', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1849, 37, 'Onderdil Motor Murah', '10, Jl. Raya Jakarta-Bogor No.37, RT.10/RW.10', '0857-8844-110', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1850, 37, 'Hokitama Motor', 'Jl. Buaran Raya No.1, RT.4/RW.12', '0856-1119-71', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1851, 37, 'Dealer Motor Kawasaki', 'Jl. Bekasi Raya No.150B, RW.1', '(021) 856106', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1852, 37, 'Bintang Motor', 'Jl. Hidup Baru No.11, RT.1/RW.7', '(021) 8533080', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1853, 37, 'Abadi Motor', 'Jl. Panjang No.16, RT.7/RW.1', '(021) 533304', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1854, 37, 'PT Hino Motors', 'Wisma Indomobil 2, Jl. MT. Haryono Kav.9, RW.6', '(021) 856457', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1855, 37, 'Dealer resmi Yamaha motor Jakarta', 'Jl. Batu Ampar V Jl. Raya Condet', '0812-8744-010', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1856, 37, 'BINTANG MOTOR PULO GEBANG', 'Jl. Raya Pulo Gebang No.10, RT.2/RW.6', '(021) 8661339', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1857, 37, 'Pisangan Motor', 'Jalan Raya Kampung Pisangan Rt 04 Rw 11 Penggilingan Cakung, RT.4/RW.11', '0877-7736-604', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1858, 37, 'Djayalina Variasi dan Aksesoris motor', 'No.99A-B, Jl. Kebon Jeruk III No.RT.15, RW.7', '(021) 600956', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1859, 37, 'Triumph Motorcycles Jakarta Distributor and Dealership', 'Jalan Kemang Raya No.19, RT.6/RW.1, Bangka, Mampang Prapatan, RT.6/RW.1', '(021) 7179255', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1860, 37, 'Yamaha Dadi Motor', 'Jalan Sawo Raya No. 72, RT. 03 / RW. 03, Pulo Gebang, Cakung, RT.3/RW.3', '(021) 480317', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1861, 37, 'Dealer Motor Honda', 'Jl. Tarum Barat Blok N No. 12 Kav. Agraria Cipinang Melayu Makasar Jakarta Timur DKI Jakarta, RT.11/RW.5', '(021) 862914', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1862, 37, 'Hoy Motor', 'JL Cabe Rawit, No. 9, Komplek Bojong Indah, RT.9/RW.6', '0817-0907-03', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1863, 37, 'KEMBAR JAYA MOTOR', 'Jalan Mawar 1 RT 03 RW 13 Bintaro Pesanggrahan, RT.3/RW.13', '(021) 9642011', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1864, 37, 'Sumber Jaya Motor. CV', 'Jl. Batu Ceper I No.6, RT.1/RW.1', '(021) 351225', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1865, 37, 'Rezeki Motor Taman Cosmos', 'Jl. Taman Cosmos C No.38, RT.6/RW.7', '0851-0910-155', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1866, 37, 'Nirwana Motor', 'Jalan P Komarudin Rt 04 Rw 05 #66 Kampung Ujung Krawang Pulo Gebang Cakung, RT.11', '(021) 480529', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1867, 37, 'Kawasaki Motor City', 'Jl. Dewi Sartika No.40D, RT.4/RW.12', '(021) 8087884', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1868, 37, 'Dealer Motor Honda', 'Jl. Panjang No. 5 Arteri Kebon Jeruk Kebon Jeruk Kebon Jeruk Jakarta Barat DKI Jakarta, RT.12/RW.10', '(021) 5367200', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1869, 37, 'Bengkel Resmi Suzuki Motor', 'Jl. RS. Fatmawati Raya No.10, RT.1/RW.5', '(021) 7591108', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1870, 37, 'Dealer Motor Honda Palmerah', 'Jl. Palmerah Barat No.59A, RT.3/RW.7', '(021) 548412', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1871, 37, 'Trendy Motor', 'Jl. Kebon Jeruk III No.1 G, RT.4/RW.4', '0816-942-88', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1872, 37, 'Wijaya Motor', 'Jalan Raya Pondok Gede No. 18JK, RT.1/RW.8, Lubang Buaya, Cipayung, RT.1/RW.8', '(021) 841135', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1873, 37, 'Honda Ahass 1002 Wahana', 'Jalan Boulevard Raya Blok WB2, RT.9/RW.11', '(021) 452502', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1874, 37, 'Restu Motor', 'Mega Glodok Kemayoran Lantai 6, Jalan Angkasa Kav.B6, Gunung Sahari Selatan, Kemayoran, RW.10', '0857-7747-518', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1875, 37, 'Aditya Motor', 'No, Jl. Pahlawan Revolusi No.4, RT.3/RW.4', '0816-1920-13', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1876, 37, 'Dealer Motor Honda Palmerah Cakung', 'Jl. Raya Bekasi KM. 23 No. 37 RT 003/RW 01, RT.3/RW.1', '(021) 460737', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1877, 37, 'Kredit motor termurah', 'Jalan Mandor Iren No.46 1 8, RT.1/RW.9', '0858-8794-835', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1878, 37, 'Syndicate Motor Kalimalang', 'Jl. Inspeksi Saluran Kali Malang No.12, RT.12/RW.1', '0812-8313-389', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1879, 37, 'Benelli Dewi Sartika Jakarta Timur', 'Jl. Dewi Sartika No.38 C, RT.6/RW.4', '0813-8811-138', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1880, 37, 'Udin Motor', 'Jalan Kota Bambu Utara 4 Rt 10 Rw 06 No 2 Jatipulo Tomang, Jakarta Barat, RT.4/RW.6', '0816-1815-46', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1881, 37, 'Rolls Royce Motor Cars Jakarta', 'Jl. Sultan Iskandar Muda No.51, RT.8/RW.3', '(021) 725900', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1882, 37, 'VESPA PIAGGIO SINERGI JAKARTA', '1, Jl. Prof. DR. Soepomo No.38, RT.1/RW.3', '(021) 830167', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1883, 37, 'Victory Motor', 'Jl. Panjang Arteri Klp. Dua Raya, RT.2/RW.1', '0878-1740-952', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1884, 37, 'X- Motor', 'Pasar Automotif Duren Sawit, Los Aks No. 51 - 52, JL. Dermaga Baru, Duren Sawit, 13440, RT.8/RW.16', '(021) 9922650', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1885, 37, 'Ivan''s Workshop - Bengkel CBU', '6 Arteri Pondok Indah, Jl. Sultan Iskandar Muda No.27, RT.6/RW.1', '(021) 729288', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1886, 37, 'Mandala Motor', 'Jl. Pahlawan Revolusi No.18, RT.10/RW.3', '(021) 8661301', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1887, 37, 'Karunia Motor', 'No., Jl. Kebon Jeruk III No.23A, RT.4/RW.4', '(021) 639940', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1888, 37, 'Sun Motor Group', 'Jl. Radio Dalam Raya No.80, RT.15/RW.3', '(021) 856498', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1889, 37, 'Sarung Tangan Motor & Masker Buff Murah Jakarta Pusat', 'Jl. Tambak No.10, RT.3/RW.5', '0821-1449-769', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1890, 37, 'Motor Sewa', 'Jl. Musi No 3z, RT.13/RW.2', '0878-0888-383', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1891, 37, 'Subaru Motor', 'Jl Taman Sari Raya Bl A-192%2F40 Taman Sari Taman Sari Jakarta Barat DKI Jakarta, RT.9/RW.3', '(021) 659003', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1892, 37, 'SBW Motor Kawasaki Central Cipinang', 'Jalan Bekasi Timur Raya No.150 B, RT.4/RW.2, Cipinang Besar Utara, Jatinegara, RT.8/RW.13', '(021) 852092', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1893, 37, 'WAFY ACCESSORIES MOTOR', '2, Jl. Bangka II Gg. V No.24.B, RT.2/RW.2', '0857-4125-006', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1894, 37, 'Harapan Motor', 'Jl. Kebon Jeruk III No.23, RT.4/RW.4', '(021) 659076', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1895, 37, 'Onderdil motor bekas', 'RT.1/RW.5', '0812-8377-039', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1896, 37, 'PT. TVS Motor Company Indonesia', 'Gedung Wirausaha, Lt.3, Jl. HR. Rasuna Said Kav. C5, RT.3/RW.1', '(021) 3002057', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1897, 37, 'Rudy Motor', 'Jalan Hadiah Utama Raya Kavling Polri Blok C Nomor 716, Jelambar, RT.11/RW.10', '0851-0209-609', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1898, 37, 'Akbar Motor', 'Jl. Serdang Raya No.9, RT.4/RW.9', '0812-8456-660', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1899, 37, 'Garasi Motor', 'Jl. Dewi Sartika No.293B, RT.4/RW.5', '0811-8997-95', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1900, 37, 'Clara Motor II', 'Jl. Panjang Arteri Klp. Dua Raya No.5, RT.3/RW.1', '(021) 530862', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1901, 37, 'Panca Jaya Motor', '9, Jl. Jend. R.S. Soekanto No.62, RT.1/RW.1', '0818-0872-332', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1902, 37, 'Hobby Motor', 'Jl. Sultan Iskandar Muda, RT.14/RW.6', '(021) 723134', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1903, 37, 'Hobi Motor Mini', 'Komplek Yado 1, Jalan Haji Sajim No. 2, RT02/02, Radio Dalam, RT.7', '0815-9393-23', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1904, 37, 'PT. Hartono Raya Motor', 'Daan Mogot Rd No.99, RT.6/RW.5', '(021) 560520', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1905, 37, 'Baruna Motor', 'Jalan FATMAWATI No.79, RT.4/RW.4, Cilandak Barat, Cilandak, RT.1/RW.10', '(021) 750421', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1906, 37, 'Luminta Motor', 'Jalan Raya Patriot Kranji No.65 ( Samping PATRIOT SQUARE, RT.005/RW.006', '0813-1153-280', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1907, 37, 'Dealer Motor Honda Intan Motor', 'Jl. Hayam Wuruk No.69 - 70, RT.10/RW.4', '(021) 649746', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1908, 37, 'Honda Lancar Motor', 'Jl. Letjend Suprapto No.82, RT.10/RW.4', '(021) 424093', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1909, 37, 'Pelangi Motor', 'JL Basuki Rahmat, No. 18 C, Rawa Bunga, Jatinegara, RT.8/RW.10', '(021) 851058', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1910, 37, 'Duta Motor Yamaha', '1, Jl. Raya Bekasi No.28, RW.1', '(021) 4682669', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1911, 37, 'Dealer Motor Honda', 'Jl. Pos Pengebumen No. 21 B-C-D Kelapa Dua Kebon Jeruk Jakarta Barat DKI Jakarta, RT.2/RW.1', '(021) 586770', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1912, 37, 'Vieta Motor', 'Jl Raya Boulevard Timur Blok M1 No. 08 & 11, Bursa Mobil 1 Kelapa Gading, RW.13', '0812-1119-66', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1913, 37, 'Sentosa Motor sparepart mobil suzuki', 'Mall Atrium Lantai 5 B 22 - 24, Jalan Senen Raya, RW.2', '0812-2262-289', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1914, 37, 'Darma Motor', 'Gg. Mantri 4 No.111, RT.7/RW.10', '0818-895-22', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1915, 37, 'X Motor', 'Pasar Automotive Duren Sawit AKS 051-052, Jalan Duren Sawit Raya, RT.7/RW.10', '0821-1030-722', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1916, 37, 'Benelli Fatmawati', 'Jl. RS. Fatmawati Raya No.2, RT.5/RW.3', '(021) 750917', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1917, 37, 'Garuda Sakti Motor', 'Jl. Sukarjo Wiryopranoto Kel No.20A, RT.1/RW.1', '0817-7696-050', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1918, 37, 'Suzuki Puma Saharjo', 'Jl. Dr. Saharjo No.75, RT.9/RW.8', '(021) 8378512', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1919, 37, 'Pelita Motor 2', 'Timur DKI Jakarta, Jl. Jend Pol RS Sukamto No.1 A, RT.1/RW.11', '(021) 8660266', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1920, 37, 'Wahana Honda Gunung Sahari', 'Jl. Gn. Sahari No.32, RT.12/RW.4', '(021) 601207', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1921, 37, 'JERRY MOTOR SPORT', 'Jalan KRT Doktor Radjiman Wiryodiningrat #22 Rt 011 Rw 06 Kelurahan Kecamatan Cakung, RT.11/RW.6', '0813-1603-671', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1922, 37, 'Yamaha Mampang Jaya Motor', 'Jalan Mampang Prapatan Raya No.83 C, RT.01/RW.02, RT.2/RW.2', '(021) 794109', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1923, 37, 'Ton''s motor', 'sebrang pom bensin lama, Jl. Raya Condet Blk. E No.225, RT.11/RW.3', '0858-8336-181', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1924, 37, 'Mandala Motor Sport', 'Jl. H. Ung No.20, RT.3/RW.3', '0812-2464-533', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1925, 37, 'Pro Bike Motor', 'JL Panjang, No. 88-A, Duri Kepa, Kebon Jeruk, RT.5/RW.11', '(021) 530905', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1926, 37, 'Cahaya Baru Motor', 'Jl.Ciledug Raya Nomor 8 Petukangan Selatan,Pesanggrahan Jakarta Utara, Jakarta Selatan, RT.12/RW.3', '0858-9073-647', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1927, 37, 'jual sparepart aksesoris motor Jsam velg knalpot', '7, Jl. Kebon Nanas IV No.48, RT.7/RW.2', '0838-7129-764', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1928, 37, 'PT Yamaha Motor', 'Jl. Kedoya Raya No.28, RT.2/RW.2', '(021) 580178', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1929, 37, 'Danu Motor', 'Rawa Belong No.3, RW.15', '0815-1011-848', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1930, 37, 'Yamaha Sartika Motor', 'Jl. Dewi Sartika No.131, RT.5/RW.2', '(021) 809061', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1931, 37, 'Honda Dunia Motor', 'Jl. Raya Kby. Lama No.556, RT.6/RW.1', '(021) 721000', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1932, 37, 'ASTRA MOTOR KETAHUN', 'JL. WIJAYA KUSUMA DESA UNIT 1.WPP, Blok V', '0821-7571-338', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1933, 37, 'Hendra Motor', 'Jl. Batu Tulis Raya No.27, RT.3/RW.2', '(021) 345662', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1934, 37, 'Sales Mobil Honda Maju Motor,Sunter Jakarta Utara', 'Jl. Danau Sunter Utara No.81-85, RT.10/RW.14', '0852-1133-826', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1935, 37, 'Asia Motor', 'Jl. Raya Condet No.5, RT.4/RW.4', '0895-2703-445', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1936, 37, 'Babe Motor / Babe Motor Indonesia', 'Blok M Square Lt 3A , Blok C 105 - 106, RT.3/RW.1', '0813-1119-197', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1937, 37, 'Karya Indah Motor - Rudy Poa', 'Jl. Bungur Besar No.153, RW.4', '(021) 420244', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1938, 37, 'YAMAHA PANGLIMA MOTOR', 'Jl. Komarudin Lama, RT.3/RW.5', '(021) 2285967', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1939, 37, 'Lucky Motor', 'Plaza Atrium, Lantai 5 Blok F 14 Jalan Raya Senen, Jl. Pasar Senen, RW.2', '0878-8901-777', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1940, 37, 'Arjuna Motor (Dealer Mobil Bekas)', 'Jl. Raya Jatiwaringin, RT.8/RW.5', '0813-2003-201', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1941, 37, 'Kredit Motor Honda', 'Jl. Raya Pasar Minggu No.Kel, RT.9/RW.10', '0813-1855-763', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1942, 37, 'Solotigo Motor', 'Jalan Kamboja #3, Halim Perdana kusuma, Jakarta Timur, RT.9/RW.1', '0878-8688-627', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1943, 37, 'Sunda Motor', 'Jl. RS. Fatmawati Raya Pasar No.9, RT.6/RW.5', '(021) 722069', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1944, 37, 'Budi Motor Knalpot', 'Jl. Raya Kby. Lama No.6, RT.6/RW.1', '(021) 530880', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1945, 37, 'Suzuki Jaya Abadi Motor', 'Pusat Niaga Duta Mas Fatmawati Lantai Dasar No. 29, RT.1/RW.5', '(021) 7279371', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1946, 37, 'Tanabang Motor Honda', 'Jl. K.H. Mas Mansyur No.80, RT.4/RW.13', '(021) 390080', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1947, 37, 'Dealer Suzuki Sun motor', 'Jl. HOS Cokroaminoto No.57, RT.003/RW.012', '(021) 730449', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1948, 37, 'AHASS Bengkel Motor Honda Nusantara Motor', 'Jl. Kedoya Raya No.1, RT.5/RW.4', '(021) 582501', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1949, 37, 'PT. Prabu Pendawa Motor', 'Jl. Prof. DR. Satrio No.78, RT.1/RW.6', '0813-8611-888', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1950, 37, 'KONG JAYA MOTOR/KJM MOTOR', '1, Jl. Daan Mogot No.67D, RT.5/RW.1', '0813-6790-696', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1951, 37, 'Dealer Motor Honda', 'Jl. Bogor Raya Km. 19 No.17 RT 005%2FRW 10 Kramat Jati Kramat Jati Jakarta Timur DKI Jakarta, RT.10/RW.10', '(021) 809201', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1952, 37, 'CHAMPION MOTOR NISSAN', 'KREKOT JAYA MOLEK BLOK AE NO 8, RT.3/RW.7', '(021) 351852', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1953, 37, 'Deta Yamaha Pondok Bambu', 'No.8 10, Jl. Pahlawan Revolusi No.003, RT.10/RW.3', '(021) 861127', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1954, 37, 'GM Motor', 'Gg. Saabun No.40D, RT.5/RW.9', '(021) 8591663', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1955, 37, 'Putera Group', 'Konica Minolta, Jl. Gunung Sahari Raya No. 78, Gedung Konica Lt. 2, RT.4/RW.3', '(0622) 1422888', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1956, 37, 'PLATINUM MOTOR', 'Jl. Kebon Jeruk III No.75 A, RT.4/RW.6', '0851-0092-126', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1957, 37, 'Yamaha Dinamika Motor', 'Jl. Meruya Ilir Raya No.37 3 5, RT.3/RW.5', '(021) 587155', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1958, 37, 'Lancar Jaya Motor', 'Jl. Letjend Suprapto No.RT.9, RT.1/RW.7', '(021) 420024', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1959, 37, 'Dealer motor honda Jatinegara', 'Jl. Jatinegara Timur balik No.57B, RT.11/RW.3', '0853-5813-396', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1960, 37, 'Redzone Company', 'Jl. Buaran Raya No.10B, RT.6/RW.13', '0813-6911-992', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1961, 37, 'YAMAHA Pelangi Motor Dealer dan Servis Center YAMAHA MOTOR', 'RT.8/RW.12', '(021) 4585418', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1962, 37, 'Yamaha Sartika Motor Otista', 'Jl. Otista Raya No.39, RT.9/RW.10', '0858-9248-738', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1963, 37, 'Syafira MOTOR (Rumah Yg Ada Spanduk Motobatt)', 'Jl. Rambutan No.3, RT.3/RW.7', '0877-8019-806', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1964, 37, 'Yamaha Motor', 'Jl. Jatinegara Timur No.52', '0812-9617-279', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1965, 37, 'Borobudur Megah Motor', 'Jl. Dermaga Raya No.99A, RT.5/RW.9', '0856-7010-67', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1966, 37, 'Mari Mampir Motor Sunter Kemayoran', 'Jalan Sunter Kemayoran No, Jakarta Utara, RT.3/RW.7', '0897-4135-33', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1967, 37, 'ARJUNA MOTOR', 'Jl. Kosambi Dalam No.48, RT.1/RW.2', '0811-1532-31', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1968, 37, 'Dennis Motor Sport', '', '0813-1829-830', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1969, 37, 'Platinum Motor', 'Jl. Otista Raya No.31a, RT.15/RW.10', '(021) 819075', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1970, 37, 'R&J MOTOSPORT', 'Jl. Duren Tiga Selatan V No.28, RT.12/RW.1', '0821-8014-448', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1971, 37, 'Honda Daya Motor', '12, Jl. Pegambiran No.3, RT.12/RW.7', '(021) 2961765', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1972, 37, 'Bengkel Mekanika Motor - Body repair & Oven painting', 'Jl. Kapten Tendean, RT.1/RW.3', '(021) 799107', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1973, 37, 'Permata Motor', 'Jl. Raya Kby. Lama No.226, RT.2/RW.1', '0831-3205-379', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1974, 37, 'DIRGANTARA MOTOR', '1, Jl. Samanhudi No.14/3, RT.1/RW.8', '(021) 350351', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1975, 37, 'R&J Motosport Kemang', 'Jl. Bangka Raya No.3, RT.4/RW.7', '0822-7822-647', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1976, 37, 'Mekar Motor Bintaro Yamaha Service Shop', 'Jl. RC. Veteran Raya No.162, RT.1/RW.6', '0896-0190-236', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1977, 37, 'Oto Kredit Motor', 'Ruko Graha Arteri Mas, Jl. Panjang No.68, RT.19/RW.4', '(021) 5830358', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1978, 37, 'Yamaha Mustika Motor', '14, Jl. Jatinegara Timur Kelurahan No.52, RT.14/RW.2', '0812-1315-790', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1979, 37, 'PT JBA Indonesia', 'Jl. Sosial No.4, RT.4/RW.2', '(021) 5086205', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1980, 37, 'Ratu Motor Variasi', 'Ps. Mobil Kemayoran Blok A 23, Jl. Industri Raya No.80, Kel No.10, RW.10', '0811-1216-62', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1981, 37, 'PT. Sahabat Motor Abadi', 'Jl. Taman Sari Raya No.16A, RT.11/RW.3', '(021) 639803', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1982, 37, 'Bungur Jaya Motor', 'Jalan Bungur Besar Raya No.87 D-E, RT.8/RW.1', '(021) 420270', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1983, 37, 'Dealer Motor Kawasaki', 'Jl. Ciledug Raya No. 35 A-B, Kreo Selatan Larangan, RT.3/RW.3', '(021) 730958', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1984, 37, 'Ta Finance', 'Plaza 5, Jl. Margaguna Raya No.3, RT.3/RW.11', '(021) 7279303', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1985, 37, 'Rasamala motor', 'Jl. Rasamala II No.29, RT.12/RW.9', '0822-2052-049', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1986, 37, 'Anugerah Motor', 'Jl. Buncit Raya Pulo No.405, RT.7/RW.5', '(021) 797216', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1987, 37, 'Mega Finance (Mega Zip) Fatmawati', 'Jalan Rs Fatmawati No. 52 B - C, Cilandak Barat, Cilandak, RT.7/RW.3', '(021) 751434', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1988, 37, 'Juara Knalpot', 'Jl. RS. Fatmawati Raya No.16-A, RT.5/RW.3', '(021) 765862', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1989, 37, 'PT. Sumber Mas Motor', 'Jl. Raya Bogor KM.20 No.120, RT.2/RW.8', '(021) 801002', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1990, 37, 'TVS Motor Retail & GS', '', '0877-8880-819', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1991, 37, 'Miki Motor', 'Jl. Dermaga Raya No.3, RT.5/RW.5', '(021) 861538', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1992, 37, 'Indah Jaya Motor', 'Kebun Sayur No 4, Jl. Kerajinan Dalam, RT.9/RW.9', '(021) 631807', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1993, 37, 'Kurnia Motor', 'Jl. Arteri Palmerah Timur No.22, RT.3/RW.1', '(021) 530481', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1994, 37, 'Emoto Sepeda Motor Listrik', 'BLK. I4, No. 10, Ruko Kelapa Gading Permai, JL. Bulevar Kelapa Gading, Kelapa Gading Timur, 14240 Jakarta Utara, Indonesia, RT.1/RW.2', '(021) 453115', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1995, 37, 'Asia Motor', 'Jl. Raya Condet No.49, RT.1/RW.3', '0812-5445-454', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1996, 37, 'PT. Yamaha Indonesia Motor Manufacturing', 'Jl. Dr. KRT Radjiman Widyodiningrat No.KM. 23, RW.4', '(021) 461222', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1997, 37, 'HONDA SURYA ABADI MOTOR', 'Ruko Green MANSION blok B 6-7, Jl. Daan Mogot No.km 10, RT.9/RW.3', '(021) 2251259', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1998, 37, 'HINO PARTS SHOP KARUNIA MOTOR', 'Jl. Kayu Manis Timur No.40, RT.6/RW.2', '(021) 8590032', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (1999, 37, 'Handy Motor', 'Jl. Kb. Raya I No.103, RT.4/RW.7', '(021) 565260', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2000, 37, 'Servis radiator dan solder tangki motor', 'Daerah khusus ibukota, Jl. Batu Merah 1 No.RT 009/01, RT.9/RW.no: 16', '0815-1179-816', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2001, 37, 'Yamaha Galur Motor', 'Jl. Let. Jen. Suprapto-Kramat Bunder, RT.10/RW.4', '(021) 424071', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2002, 37, 'Honda - Kebon Jeruk', 'Jalan Panjang No.200, Kebon Jeruk, RT.11/RW.10', '(021) 54925808', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2003, 37, 'Chung Motor. PT', 'JL. Duri, 54-56, Jembatan V, Jakarta, RT.2/RW.4', '(021) 631078', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2004, 37, 'Dealer Honda Bareno Motor', 'No. 555 Blok B-C, Jl. RC. Veteran Raya, RT.3/RW.11', '0896-6557-968', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2005, 37, 'Karimapainting_art ( cat repaint motor dan helm )', 'Jl. Batu Sari V Jl. Raya Condet No.62, RT.9/RW.2', '0895-1070-287', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2006, 37, 'Usaha Baru Motor', 'Jl. Tanjung Duren Barat III No. 104 F Tanjung Duren Selatan Grogol Petamburan Jakarta Barat DKI Jakarta, RT.7/RW.5', '(021) 918095', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2007, 37, 'Yamaha Prima Motor', 'Gg. Madrasah Samping RM.Sederhana No.7A, RT.5/RW.1', '(021) 8088412', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2008, 37, 'Honda', '', '(021) 564488', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2009, 37, 'PT Mitsubishi Motors Krama Yudha Sales Indonesia (MMKSI)', 'Blok Pulo MAS Selatan Jalan Pulomas Selatan Kavling Blok, Jl. Pulomas Selatan No.22, RT.6/RW.11', '(021) 489610', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2010, 37, 'YAMAHA PERSADA MOTOR', 'Jl. Kayu Manis Barat No.36, RT.20/RW.3', '0878-8690-041', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2011, 37, 'YAMAHA-Niaga Motor Cengkareng', 'Jalan Lingkar Luar No. 1-2-3 RT.7/RW.14 Cengkareng Timur Cengkareng RT.1, RT.1/RW.8', '(021) 545313', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2012, 37, 'Bahagia Motor', 'Jl. Permata Meruya 2 No.E14, RT.22/RW.4', '(021) 585148', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2013, 37, 'Bintang Motor', 'Jl. Prof. Dr. Latumenten No. 12A Angke Tambora Jakarta Barat DKI Jakarta, RT.10/RW.3', '(021) 7100766', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2014, 37, 'Marno Jaya Motor', '3, Jl. Tj. Barat Raya No.148, RT.3/RW.4', '(021) 2787090', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2015, 37, 'Astra Motor Racing Team', 'Jalan gaya Motor Raya no 8 Sunter 2, Sunter, Indonesia, RW.8', '(021) 6531025', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2016, 37, 'Tani Motor ISUZU,MITSUBISI,FORD,UD NISSAN TRUCK,HINO,CHEVROLET,FORKLIFT,GENSET', '', '(021) 380571', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2017, 37, 'Yamaha Putera Ragunan', 'Jl. Taman Margasatwa Raya Jl. Raya Ragunan No.22, RT.7/RW.5', '(021) 780783', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2018, 37, 'Dealer Motor Honda', 'Jl. Cilandak KKO Raya No.2, RT.14/RW.8', '(021) 789041', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2019, 37, 'PT. CIWANGI BERLIAN MOTOR (COLT DIESEL/FUSO)', 'Jl. Sultan Iskandar Muda No.28, RT.2/RW.1', '0813-1080-873', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2020, 37, 'Usaha Baru Motor', 'Jl. Swadaya No.102, RT.6/RW.10', '0818-0600-117', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2021, 37, 'JBA Tipar', 'Jl. Tipar Cakung No.8, RT.9/RW.2', '1 500 36', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2022, 37, 'Motor Mini Shop', 'Jl. Raya Pondok Randu No.38, RT.3/RW.7', '0812-1234-281', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2023, 37, 'Kusuma Motor', 'Pasar Mobil Kemayoran Blok S No 99 G H Jalan Industri Raya, Jakarta Pusat, RT.9/RW.7', '(021) 654357', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2024, 37, 'JBA Indonesia', 'Jl. Lotte Mart No.5, RT.6/RW.11', '(021) 5890193', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2025, 37, 'SM SPORT - SYM Senjaya Motor', 'Jl. P Jayakarta Komp. No.139, RT.7/RW.7', '0811-151-99', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2026, 37, 'Planet Ban Percetakan Negara Jakarta', 'Jalan Percetakan Negara No.26, Jl. Nasional 1 No.RT.2, RT.2/RW.1', '0815-1039-970', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2027, 37, 'PT. Srikandi Diaomnd Motor', 'Jl. Raya Siliwangi No.26, RT.001/RW.004', '(021) 9518885', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2028, 37, 'Yamaha Yamasakti Motor', 'Jl.Daan mogot Km 13, Jl. Daan Mogot No.31A, RT.2/RW.1', '(021) 544853', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2029, 37, 'Viar Mitraindo Rempoa', 'Rempoa, Jl. Pahlawan, RT.1', '0812-9023-905', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2030, 37, 'Songsi Motor', 'Jalan Jeruk Raya Blok Sumanto No.55, RT.5/RW.1', '0811-141-30', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2031, 37, 'PT.KIRINDO MITRA LESTARI', 'Jl. KH. Hasyim Ashari No.8-10, RT.1/RW.4', '(021) 630943', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2032, 37, 'Planet Ban Mampang Prapatan Jakarta', 'Jl. Mampang prapatan XVI No. 4 Rt. 001/ 003 Kel, Jl. Nasional 1 No.RT.1, RT.1/RW.6', '0815-1037-423', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2033, 37, 'Adira Finance', 'Jalan KH Zainul Arifin Blok A3-A4 No. 27, Petojo Utara, Gambir, RT.8/RW.1, RT.8/RW.1', '(021) 632270', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2034, 37, 'Niaga Motor', 'Jl. Utama Raya No.4C, RT.4/RW.3', '0878-8556-220', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2035, 37, 'Adira', 'Pasar Rawamangun, Jl. Pegambiran, RT.9/RW.8', '0856-6395-80', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2036, 37, 'Antara Motor', 'Jl. Kalisari No.106, RW.2', '0822-1000-600', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2037, 37, 'PT. Triangle Motorindo', 'Jl. Danau Agung Selatan Blok O III No. 38 Sunter, RT.10/RW.11', '(021) 6583220', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2038, 37, 'Planet Ban Asem Baris Tebet Jakarta', '9, Jl. Asem Baris Raya No.114, RT.9/RW.5', '0857-6055-605', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2039, 37, 'Honda IKM Hasyim Ashari', 'Jl. KH. Hasyim Ashari No.24', '(021) 633145', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2040, 37, 'Planet Ban Pramuka Jakarta', '2, Jl. Pramuka No.20, RT.5/RW.1', '0815-1039-969', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2041, 37, 'Bintang Motor', 'Jl. Pangkalan Jati No.36', '0821-2112-112', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2042, 37, 'Asosiasi Industri Sepeda Motor Indonesia (AISI)', 'Jl. Boulevard Timur No.88, RT.5/RW.2', '(021) 856618', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2043, 37, 'Benelli', 'Ruko The Linq No.839, Jl. Trembesi No.2, RW.5', '(021) 571132', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2044, 37, 'Maxindo Jasa Kirim Motor I Ekspedisi Murah Jasa Pengiriman Motor Antar Kota Antar Pulau Via Kereta Api Door to Door Service', 'Grand Boutique Centre Complex Block D, Jl. Mangga Dua Raya No.52, RT.11/RW.5', '0818-0437-005', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2045, 37, 'Honda Maju Motor', 'Jl. Danau Sunter Utara No.81 - 85, RT.10/RW.14', '(021) 651800', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2046, 37, 'JDM Motor', 'Mangga Dua Square Lantai LG M 12 A Raya No 1 Pademangan Bara Pademangan, Jl. Gn. Sahari, RT.1/RW.13', '0852-1773-331', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2047, 37, 'Toko Motor Mini', 'Jl. Utan Jati No.2C, RT.6/RW.12', '0851-0032-033', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2048, 37, 'Arya Niaga Mitra Nusa. PT', 'JL. Lingkar Luar Barat, No. 1-3, RT.1/RW.8', '(021) 544125', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2049, 37, 'Ivan Motor', 'Mall MGK, Jl. Rendani Blok D3 No.5, RW.10', '(021) 2664741', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2050, 37, 'Honda Cengkareng', 'Jl. Lkr. Luar Barat No.12A, RT.1/RW.8', '(021) 545448', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2051, 37, 'NISSAN MOTOR INDONESIA', 'South Quarter Tower C, 15-16F, Jl. R.A.Kartini No.Kav. 8, RT.10/RW.4', '(021) 2271232', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2052, 37, 'PT Tjahja Sakti Motor', 'Jalan Danau Sunter Utara No. Sunter II, RW.8', '(021) 650933', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2053, 37, 'Yamaha niaga motor duri kosambi', 'JL. Raya duri kosambi rt. 013 / 007 no.8d, RT.13/RW.7', '0878-8102-112', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2054, 37, 'Honda Ahass 1084', 'Jl. Ampera Raya No.10B, RT.2/RW.10', '(021) 780455', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2055, 37, 'Rajawali Motorcycle Rental - Sewa Motor Jakarta', 'Grand Duta Tangerang Cluster Garnet G1/76, RT.001/RW.024', '0811-9899-23', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2056, 37, 'Benelli Motor Indonesia', 'ruko the linq no.839, Jl. Trembesi', '(021) 2961457', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2057, 37, 'Yamaha Ika Motor', 'Jl. Jemb. Dua Raya No.12 B, RT.2/RW.4', '(021) 662014', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2058, 37, 'Asep Subroto', '13, Jl. Kalisari Asri No.49, RT.13/RW.3', '0896-5064-404', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2059, 37, 'Bone Motor', 'Jl. Semangka No.24, RT.3/RW.10', '0822-9909-877', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2060, 37, 'Jennete Rent Car', 'Jl. Pangeran Jayakarta No.73, RT.3/RW.6', '0888-1111-50', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2061, 37, 'PT. Honda Prospect Motor', 'Jl. Gaya Motor I No.1, RT.9/RW.9', '(021) 651040', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2062, 37, 'Prima Wahana Motor UD', 'Jl. Peta Barat No. 12G Kalideres Kalideres Jakarta Barat DKI Jakarta, RT.1/RW.18', '(021) 544211', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2063, 37, 'Berlian Motor', 'Jl. Kramat Jaya Raya No.6, RT.2/RW.7', '0878-7821-397', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2064, 37, 'Sarana Motor', 'Jl. Moh. Kahfi, RT.3/RW.4', '(021) 787144', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2065, 37, 'PT Dipo Angkasa Motor', 'Jl. Pluit Selatan Raya No.1C, RT.16/RW.8', '(021) 661111', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2066, 37, 'Mario Motor', 'Jl. Sumur Batu Raya No.18, RW.8', '(021) 422785', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2067, 37, 'Baychii Motor Shop', 'Jl. Moh. Kahfi 1, RT.3/RW.1', '0813-1111-547', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2068, 37, 'Yamaha Riguna Motor', 'Jl. Raya Cilincing No.5, RT.3/RW.7', '(021) 440812', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2069, 37, 'PT. Maxindo Sukses Express', 'Komp. Perkantoran Pasar Segar Citra Blok KBC No.18, Jl. Utan Jati, RT.7/RW.12', '0813-1855-087', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2070, 37, 'Dealer Yamaha Motor (Yamaha Gilang Mandiri)', 'Jl. Moch. Kahfi II No.40, RT.1/RW.4', '(021) 727158', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2071, 37, 'Indomobil Trada Nasional (Nissan). PT', 'JL. Pluit Raya Selatan, No. 8 A, Penjaringan, Jakarta, RT.7/RW.7', '(021) 6669711', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2072, 37, 'ADIRA Daan Mogot', 'Jl. Tanah Lot Blok LC I No 14 - 15, Kalideres, RT.8/RW.12', '0895-0370-127', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2073, 37, 'OTO.com', 'PT. Carbay Service Indonesia, Menara Citicon 10th Floor, Jl. Letjen S. Parman No.Kav. 72, RT.4/RW.3', '(021) 2930875', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2074, 37, 'Honda Jakarta Center', 'Jl. P Jayakarta Komp. No.50, RT.7/RW.7', '(021) 601110', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2075, 37, 'Viar shwroom', 'Ciledug Mas, Jl. HOS Cokroaminoto No.15, RT.003/RW.004', '0878-8715-483', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2076, 37, 'Showroom Hyundai jakarta', 'Jl. Karang Tengah Raya No.13, RT.13/RW.3', '0877-8044-806', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2077, 37, 'Ilham Helm', 'Jl. Raya Bogor, RT.4/RW.8', '0812-9858-236', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2078, 37, 'BADAN PENDAPATAN DAERAH', 'Jl. Abdul Muis No.66, RT.4/RW.3', '(021) 386558', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2079, 37, 'Benelli Depok Official', 'Cimanggis Center No. 10 - 11, Jalan Raya Bogor KM. 29, Mekarsari, Cimanggis', '(021) 2282794', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2080, 37, 'Dakota Cargo', 'Mr. Fajri/ sdr. apep, Jl Baru gg 3 rt 08/01 kel.cilincing ksc.cilincing jakarta utara tlp 021-24404418 hp 0857755589026, RT.12/RW.1', '0857-7558-902', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2081, 37, 'Raja Motor Cibubur Point', 'Jl. Alternatif Cibubur', '(021) 2281856', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2082, 37, 'ROSALIA EXPRESS Karang Tengah', 'Jl. Karyawan 3 No.5, RT.002/RW.007', '0817-383-83', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2083, 37, 'Rosalia Express Ciputat', '', '0815-6760-298', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2084, 37, 'JAKARTA MOTOR SLAWI', 'Jl. Jend Sudirman No.21', '0818-330-18', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2085, 37, 'Yamaha Kencana Cemerlang Abadi', 'Jl. Kh Hasyim Asyari No.45-bcd, RT.006/RW.002', '(021) 730883', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2086, 37, 'Yamaha AON Motor', 'Jl. Raya Hankam No.99, RT.002/RW.005', '(021) 8459793', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2087, 37, 'Basecame The NINE', 'Jl. Kapuk Darurat No.85 B, RT.3/RW.12', '0812-1999-201', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2088, 37, 'Kejaksaan Negeri Jakarta Selatan', 'Jl. Tanjung No.1, RT.1/RW.2', '0813-1056-777', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2089, 37, 'Mizuho Balimor Finance', 'GRHA 137, 7th Floor, Jl. Pangeran Jayakarta No.137, RT.7/RW.7', '(021) 639387', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2090, 37, 'Bengkel Motor & Mobil Bandar Lampung', 'Jl. Medan Merdeka Sel. No.8-9', '(021) 382248', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2091, 37, 'PT Honda Prospect Motor (HPM)', 'Kawasan Industri Mitra Karawang, Jl. Mitra Utara II', '(0267) 44077', '2020-10-20 22:08:45+07', 1);
INSERT INTO transactional.instances (id, keyword_id, instance_name, instance_address, instance_phone, create_at, statusid) VALUES (2092, 37, 'Jakarta Motor', 'Jl. Sam Ratulangi No.85', '(0431) 85286', '2020-10-20 22:08:45+07', 1);


--
-- Data for Name: keyword; Type: TABLE DATA; Schema: transactional; Owner: postgres
--

INSERT INTO transactional.keyword (id, keyword_name, create_at, statusid, fetched) VALUES (32, 'Hijab Bandung', '2020-10-20 14:31:34+07', 1, 1);
INSERT INTO transactional.keyword (id, keyword_name, create_at, statusid, fetched) VALUES (33, 'Nadira Hijab', '2020-10-20 14:36:43+07', 1, 1);
INSERT INTO transactional.keyword (id, keyword_name, create_at, statusid, fetched) VALUES (34, 'Motor bandung', '2020-10-20 14:37:47+07', 1, 1);
INSERT INTO transactional.keyword (id, keyword_name, create_at, statusid, fetched) VALUES (35, 'Motor Pandaan', '2020-10-20 15:28:14+07', 1, 1);
INSERT INTO transactional.keyword (id, keyword_name, create_at, statusid, fetched) VALUES (36, 'Motor pasuruan', '2020-10-20 15:34:56+07', 1, 1);
INSERT INTO transactional.keyword (id, keyword_name, create_at, statusid, fetched) VALUES (37, 'Motor Jakarta', '2020-10-20 22:08:45+07', 1, 1);


--
-- Name: application_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.application_id_seq', 1, true);


--
-- Name: authentication_role_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.authentication_role_id_seq', 2, true);


--
-- Name: authentication_user_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.authentication_user_id_seq', 2, true);


--
-- Name: authentication_user_login_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.authentication_user_login_id_seq', 19, true);


--
-- Name: authentication_user_role_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.authentication_user_role_id_seq', 3, true);


--
-- Name: s_label_menu_id_seq; Type: SEQUENCE SET; Schema: masterdata; Owner: postgres
--

SELECT pg_catalog.setval('masterdata.s_label_menu_id_seq', 4, true);


--
-- Name: s_menu_id_seq; Type: SEQUENCE SET; Schema: masterdata; Owner: postgres
--

SELECT pg_catalog.setval('masterdata.s_menu_id_seq', 9, true);


--
-- Name: s_personal_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_personal_data_id_seq', 12, true);


--
-- Name: instances_id_seq; Type: SEQUENCE SET; Schema: transactional; Owner: postgres
--

SELECT pg_catalog.setval('transactional.instances_id_seq', 2092, true);


--
-- Name: keyword_id_seq; Type: SEQUENCE SET; Schema: transactional; Owner: postgres
--

SELECT pg_catalog.setval('transactional.keyword_id_seq', 37, true);


--
-- Name: application application_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.application
    ADD CONSTRAINT application_pk PRIMARY KEY (id);


--
-- Name: authentication_role authentication_role_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_role
    ADD CONSTRAINT authentication_role_pk PRIMARY KEY (id);


--
-- Name: authentication_user_login authentication_user_login_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_login
    ADD CONSTRAINT authentication_user_login_pk PRIMARY KEY (id);


--
-- Name: authentication_user authentication_user_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user
    ADD CONSTRAINT authentication_user_pk PRIMARY KEY (id);


--
-- Name: authentication_user_role authentication_user_role_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_role
    ADD CONSTRAINT authentication_user_role_pk PRIMARY KEY (id);


--
-- Name: s_label_menu s_label_menu_pk; Type: CONSTRAINT; Schema: masterdata; Owner: postgres
--

ALTER TABLE ONLY masterdata.s_label_menu
    ADD CONSTRAINT s_label_menu_pk PRIMARY KEY (id);


--
-- Name: s_menu s_menu_pk; Type: CONSTRAINT; Schema: masterdata; Owner: postgres
--

ALTER TABLE ONLY masterdata.s_menu
    ADD CONSTRAINT s_menu_pk PRIMARY KEY (id);


--
-- Name: s_personal_data s_personal_data_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_personal_data
    ADD CONSTRAINT s_personal_data_pk PRIMARY KEY (id);


--
-- Name: instances instances_pk; Type: CONSTRAINT; Schema: transactional; Owner: postgres
--

ALTER TABLE ONLY transactional.instances
    ADD CONSTRAINT instances_pk PRIMARY KEY (id);


--
-- Name: keyword keyword_pk; Type: CONSTRAINT; Schema: transactional; Owner: postgres
--

ALTER TABLE ONLY transactional.keyword
    ADD CONSTRAINT keyword_pk PRIMARY KEY (id);


--
-- Name: instances_instance_phone_idx; Type: INDEX; Schema: transactional; Owner: postgres
--

CREATE INDEX instances_instance_phone_idx ON transactional.instances USING btree (instance_phone, statusid);


--
-- Name: instances_keyword_id_idx; Type: INDEX; Schema: transactional; Owner: postgres
--

CREATE INDEX instances_keyword_id_idx ON transactional.instances USING btree (keyword_id, statusid);


--
-- Name: keyword_keyword_idx; Type: INDEX; Schema: transactional; Owner: postgres
--

CREATE INDEX keyword_keyword_idx ON transactional.keyword USING btree (keyword_name, statusid);


--
-- PostgreSQL database dump complete
--

