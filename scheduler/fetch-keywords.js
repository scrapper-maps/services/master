`use strict`
const puppeteer = require('puppeteer')

let onProcess = null
const interval = 20000
module.exports = async function fetchKeywords () {
    if(onProcess !== null && onProcess - new Date() < 10 * 60 * 1000){
        console.log('Another Proccess is currently running')
        return 'Another Proccess is currently running'
    }

    console.log('Scraping is started')
    try {
        let keyword = await db.keyword.findOne({
            attributes: ['id', 'keyword_name', 'create_at', 'fetched' ],
            where: { fetched: 0, statusid: 1 },
            order: ['create_at'],
            raw: true
        })
        console.log('Keyword: ', keyword.keyword_name)
    
        if (keyword) {
            /** Lock Another Scrape */
            onProcess = new Date()
    
            /** Scrapping */
            function sleep(ms) {
                return new Promise(resolve => setTimeout(resolve, ms));
            }
    
            const url = `https://www.google.com/maps/search/${keyword.keyword_name.toLowerCase().replace(' ', '+')}`
            const browser = await puppeteer.launch(
                {args: ['--no-sandbox', '--disable-setuid-sandbox']}
            )
    
            const page = await browser.newPage();
            await page.setViewport({
                width: 1920,
                height: 1080,
                deviceScaleFactor: 1,
            });
    
            await page.goto(url);
            
            let places = []
    
            do {
                await sleep(interval)
                
                let result = {
                    instance_name: [...await page.$$eval('.section-result .section-result-title', results => results.map(result => result.textContent))],
                    instance_address: [...await page.$$eval('.section-result .section-result-location', results => results.map(result => result.textContent))],
                    instance_phone: [
                        ...await page.$$eval('.section-result .section-result-phone-number', results => results.map(result => {
                            let str = result.textContent.trim()
                            return str.substring(0, str.length - 3)
                        }))
                    ],
                }

                console.log('Result', result)
                
                for (let index in result.instance_name) {
                    if (result.instance_phone[index] && !places.find((place) => result.instance_phone[index] == place.instance_phone)) {
                        places.push({ 
                            keyword_id: keyword.id,
                            instance_name: result.instance_name[index].substring(0, 175), 
                            instance_address: result.instance_address[index].substring(0, 175),
                            instance_phone: result.instance_phone[index] 
                        })
                    }
                }
    
                const next = await page.$('#n7lv7yjyC35__section-pagination-button-next[disabled]');
    
                if (result.instance_name.length == 0 || next != null) { break }
    
                await page.click('#n7lv7yjyC35__section-pagination-button-next')
            } while(true)
    
            await browser.close()
            /** End Scrapping */
            
            let validatePhone = await db.instances.findAll({
                attributes: ['instance_phone'],
                where: { 
                    [Op.and]: [
                        sequelize.where(
                            sequelize.col('instance_phone'),
                            { [Op.in]: places.map((val) => val.instance_phone) }
                        ),
                        { statusid: 1 } 
                    ]
                },
                raw: true
            })
            
            for (let i in places) {
                if(validatePhone.find((phone) => phone == places[i].instance_phone)) {
                    places.splice(i+1, 1)
                }
            }
    
            /** transaction start */
            let transaction = await sequelize.transaction()
            
            try {
                await db.instances.bulkCreate(places, { transaction })
                await db.keyword.update(
                    { fetched: 1 },
                    { where: { id: keyword.id }, transaction },
                )
    
                await transaction.commit()
            } catch(error) {
                console.log(error)
                await transaction.rollback()
            }
            /** transaction end */
        }
    
        /** Unlock Another Scrapes */
        onProcess = null
        console.log('Scraping is finished')
        
        return 'Scraping process is successfull'
    } catch(error) {
        console.log(error)
        onProcess = null
    }
}