const CronJob = require('cron').CronJob
const fectchKeyword = require('./fetch-keywords')

const scheduler = {
    start: () => {
        const jobs = [ new CronJob('*/30 * * * * *', fectchKeyword) ]

        for (let job of jobs) { job.start() }
    }
}

module.exports = scheduler